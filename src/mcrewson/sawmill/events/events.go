// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package events

import (
	"time"
)

type LogEvent struct {
	Timestamp time.Time
	Fields    map[string]string
}

type LogEventChannel chan *LogEvent

func NewLogEvent(msg string) *LogEvent {
	event := &LogEvent{
		Timestamp: time.Now(),
		Fields:    make(map[string]string),
	}

	event.Fields["message"] = msg
	return event
}

func (event *LogEvent) AddFields(fields map[string]string) {
	for k, v := range fields {
		event.Fields[k] = v
	}
}

func (event *LogEvent) Copy() *LogEvent {
	newEvent := &LogEvent{
		Timestamp: event.Timestamp,
		Fields:    make(map[string]string),
	}
	if len(event.Fields) > 0 {
		for k, v := range event.Fields {
			newEvent.Fields[k] = v
		}
	}
	return newEvent
}

func (event *LogEvent) Merge(newEvent *LogEvent) {
	for newKey, newValue := range newEvent.Fields {
		if newKey == "message" {
			// append to existing message
			message, exists := event.Fields["message"]
			if !exists {
				message = newValue
			} else {
				message = message + "\n" + newValue
			}
			event.Fields["message"] = message
		} else {
			event.Fields[newKey] = newValue
		}
	}
}

//////////////////////////////////////////////////////////////////////////////
// THE END
