// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package events

import (
	"bytes"
	"compress/zlib"
	"io"
)

//////////////////////////////////////////////////////////////////////////////

type EventCodec interface {
	EncodeEvent(event *LogEvent) (data []byte, err error)
	DecodeEvent(data []byte) (event *LogEvent, err error)

	EncodeFrame(frame *LogFrame) (data []byte, err error)
	DecodeFrame(data []byte) (frame *LogFrame, err error)
}

//////////////////////////////////////////////////////////////////////////////

func compressData(data []byte) (compressedData []byte, err error) {
	var buffer bytes.Buffer
	compressor := zlib.NewWriter(&buffer)
	compressor.Write(data)
	compressor.Flush()
	compressor.Close()
	return buffer.Bytes(), nil
}

func decompressData(data []byte) (decompressedData []byte, err error) {
	var buffer bytes.Buffer
	decompressor, err := zlib.NewReader(bytes.NewReader(data))
	if err != nil {
		return nil, err
	}
	io.Copy(&buffer, decompressor)
	decompressor.Close()
	return buffer.Bytes(), nil
}

//////////////////////////////////////////////////////////////////////////////

/*type JsonEventCodec struct {
    compress bool
}

func NewJsonEventCodec(compress bool) *JsonEventCodec {
    codec := &JsonEventCodec{
        compress: compress,
    }
    return codec
}

func (jec *JsonEventCodec) EncodeEvent(event *LogEvent) (data []byte, err error) {
}

func (jec *JsonEventCodec) DecodeEvent(data []byte) (event *LogEvent, err error) {
}

func (jec *JsonEventCodec) EncodeEvents(events []*LogEvent) (data []byte, err error) {
}

func (jec *JsonEventCodec) DecodeEvents(data []byte) (events []*LogEvent, err error) {
}
*/
//////////////////////////////////////////////////////////////////////////////
// THE END
