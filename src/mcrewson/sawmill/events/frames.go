// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package events

import (
	"fmt"
	"time"
)

type LogFrame struct {
	Timestamp time.Time
	Events    []*LogEvent
	Size      uint32
	Count     uint32
}

func NewLogFrame(size uint32) *LogFrame {
	frame := &LogFrame{}
	frame.Timestamp = time.Now()
	frame.Size = size
	frame.Events = make([]*LogEvent, size)
	return frame
}

func (frame *LogFrame) AddEvent(event *LogEvent) error {
	if frame.Count == frame.Size {
		// this frame is full. We cannot add another event
		return fmt.Errorf("Cannot add event. LogFrame is full")
	}
	frame.Events[frame.Count] = event
	frame.Count += 1
	return nil
}

func (frame *LogFrame) Reset() {
	frame.Count = 0
	frame.Timestamp = time.Now()
}

func (frame *LogFrame) Copy() *LogFrame {
	newFrame := &LogFrame{
		Timestamp: frame.Timestamp,
		Size:      frame.Size,
		Count:     frame.Count,
	}
	newFrame.Events = make([]*LogEvent, frame.Size)
	for i := uint32(0); i < frame.Count; i++ {
		newFrame.Events[i] = frame.Events[i].Copy()
	}
	return newFrame
}

//////////////////////////////////////////////////////////////////////////////
// THE END
