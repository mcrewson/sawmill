// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package events

import (
	"bytes"
	"encoding/json"
	"time"
)

//////////////////////////////////////////////////////////////////////////////

func (event *LogEvent) EncodeJson() []byte {
	buffer := new(bytes.Buffer)
	dst := make(map[string]string)
	for k, v := range event.Fields {
		dst[k] = v
	}
	dst["@timestamp"] = event.Timestamp.UTC().Format(time.RFC3339Nano)
	jsonb, _ := json.Marshal(dst)
	buffer.Write(jsonb)
	return buffer.Bytes()
}

func DecodeJson(jsonLogEvent []byte) (event *LogEvent) {
	var dst map[string]string
	json.Unmarshal(jsonLogEvent, &dst)

	event = &LogEvent{}
	event.Timestamp, _ = time.Parse(time.RFC3339Nano, dst["@timstamp"])
	for k, v := range dst {
		if k != "@timestamp" {
			event.Fields[k] = v
		}
	}
	return
}

//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
// THE END
