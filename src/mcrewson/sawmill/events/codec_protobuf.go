// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package events

import (
	"math"
	"time"

	"code.google.com/p/goprotobuf/proto"
)

//////////////////////////////////////////////////////////////////////////////

type ProtobufEventCodec struct {
	compress bool
}

func NewProtobufEventCodec(compress bool) *ProtobufEventCodec {
	codec := &ProtobufEventCodec{
		compress: compress,
	}
	return codec
}

func eventToProtobuf(event *LogEvent) (pbLogEvent *PBLogEvent, err error) {
	pbLogEvent = &PBLogEvent{}
	timestamp := &PBTimestamp{
		EpochSeconds: proto.Int64(event.Timestamp.Unix()),
		EpochNanos:   proto.Int64(int64(event.Timestamp.Nanosecond())),
	}
	pbLogEvent.Timestamp = timestamp
	for k, v := range event.Fields {
		field := &PBLogEvent_PBLogField{Key: proto.String(k), Value: proto.String(v)}
		pbLogEvent.Fields = append(pbLogEvent.Fields, field)
	}
	return
}

func protobufToEvent(pbLogEvent *PBLogEvent) (event *LogEvent, err error) {
	event = &LogEvent{}
	event.Timestamp = time.Unix(pbLogEvent.GetTimestamp().GetEpochSeconds(), pbLogEvent.GetTimestamp().GetEpochNanos())
	event.Fields = make(map[string]string)
	for _, field := range pbLogEvent.GetFields() {
		k := field.GetKey()
		v := field.GetValue()
		event.Fields[k] = v
	}
	return
}

func (pec *ProtobufEventCodec) EncodeEvent(event *LogEvent) (data []byte, err error) {
	pbLogEvent, err := eventToProtobuf(event)
	if err == nil {
		data, err = proto.Marshal(pbLogEvent)
	}
	return
}

func (pec *ProtobufEventCodec) DecodeEvent(data []byte) (event *LogEvent, err error) {
	pbLogEvent := &PBLogEvent{}
	err = proto.Unmarshal(data, pbLogEvent)
	if err == nil {
		event, err = protobufToEvent(pbLogEvent)
	}
	return
}

///////////////////////////////////////

func (pec *ProtobufEventCodec) EncodeFrame(frame *LogFrame) (data []byte, err error) {
	pbLogFrame := &PBLogFrame{
		Version: proto.Int32(1),
		Size:    proto.Uint32(frame.Size),
		Count:   proto.Uint32(frame.Count),
	}

	timestamp := &PBTimestamp{
		EpochSeconds: proto.Int64(frame.Timestamp.Unix()),
		EpochNanos:   proto.Int64(int64(frame.Timestamp.Nanosecond())),
	}
	pbLogFrame.Timestamp = timestamp

	compression_type := PBLogFrame_NONE
	if pec.compress {
		compression_type = PBLogFrame_ZLIB
	}
	pbLogFrame.Compression = &compression_type

	pbLogEvents := &PBLogFrame_PBLogEvents{}
	for i := uint32(0); i < frame.Count; i++ {
		pbLogEvent, encodeErr := eventToProtobuf(frame.Events[i])
		if encodeErr != nil {
			return nil, encodeErr
		}
		pbLogEvents.Events = append(pbLogEvents.Events, pbLogEvent)
	}

	if pec.compress {
		eventsdata, marshalErr := proto.Marshal(pbLogEvents)
		if err != nil {
			return nil, marshalErr
		}
		eventsbytes, compressErr := compressData(eventsdata)
		if err != nil {
			return nil, compressErr
		}
		pbLogFrame.CompressedData = eventsbytes
	} else {
		pbLogFrame.Events = pbLogEvents
	}

	data, err = proto.Marshal(pbLogFrame)
	if err != nil {
		return nil, err
	}

	return
}

func (pec *ProtobufEventCodec) DecodeFrame(data []byte) (frame *LogFrame, err error) {
	pbLogFrame := &PBLogFrame{}
	err = proto.Unmarshal(data, pbLogFrame)
	if err == nil {
		frame = NewLogFrame(pbLogFrame.GetSize())
		frame.Timestamp = time.Unix(pbLogFrame.GetTimestamp().GetEpochSeconds(), pbLogFrame.GetTimestamp().GetEpochNanos())

		compression_type := pbLogFrame.GetCompression()
		var pbLogEvents *PBLogFrame_PBLogEvents
		switch {
		case compression_type == PBLogFrame_NONE:
			pbLogEvents = pbLogFrame.GetEvents()

		case compression_type == PBLogFrame_ZLIB:
			eventsdata, decompressErr := decompressData(pbLogFrame.GetCompressedData())
			if decompressErr != nil {
				return nil, decompressErr
			}
			pbLogEvents = &PBLogFrame_PBLogEvents{}
			decodeErr := proto.Unmarshal(eventsdata, pbLogEvents)
			if decodeErr != nil {
				return nil, decodeErr
			}
		}

		for _, pbLogEvent := range pbLogEvents.GetEvents() {
			event, decodeErr := protobufToEvent(pbLogEvent)
			if decodeErr != nil {
				return nil, decodeErr
			}
			frame.AddEvent(event)
		}

	}
	return
}

//////////////////////////////////////////////////////////////////////////////

// INTERNAL PROTOBUF STRUCTURES

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = math.Inf

type PBLogFrame_PBCompressionType int32

const (
	PBLogFrame_NONE PBLogFrame_PBCompressionType = 0
	PBLogFrame_ZLIB PBLogFrame_PBCompressionType = 1
)

var PBLogFrame_PBCompressionType_name = map[int32]string{
	0: "NONE",
	1: "ZLIB",
}
var PBLogFrame_PBCompressionType_value = map[string]int32{
	"NONE": 0,
	"ZLIB": 1,
}

func (x PBLogFrame_PBCompressionType) Enum() *PBLogFrame_PBCompressionType {
	p := new(PBLogFrame_PBCompressionType)
	*p = x
	return p
}
func (x PBLogFrame_PBCompressionType) String() string {
	return proto.EnumName(PBLogFrame_PBCompressionType_name, int32(x))
}
func (x *PBLogFrame_PBCompressionType) UnmarshalJSON(data []byte) error {
	value, err := proto.UnmarshalJSONEnum(PBLogFrame_PBCompressionType_value, data, "PBLogFrame_PBCompressionType")
	if err != nil {
		return err
	}
	*x = PBLogFrame_PBCompressionType(value)
	return nil
}

type PBTimestamp struct {
	EpochSeconds     *int64 `protobuf:"varint,3,req,name=epochSeconds" json:"epochSeconds,omitempty"`
	EpochNanos       *int64 `protobuf:"varint,4,opt,name=epochNanos" json:"epochNanos,omitempty"`
	XXX_unrecognized []byte `json:"-"`
}

func (m *PBTimestamp) Reset()         { *m = PBTimestamp{} }
func (m *PBTimestamp) String() string { return proto.CompactTextString(m) }
func (*PBTimestamp) ProtoMessage()    {}

func (m *PBTimestamp) GetEpochSeconds() int64 {
	if m != nil && m.EpochSeconds != nil {
		return *m.EpochSeconds
	}
	return 0
}

func (m *PBTimestamp) GetEpochNanos() int64 {
	if m != nil && m.EpochNanos != nil {
		return *m.EpochNanos
	}
	return 0
}

type PBLogEvent struct {
	Timestamp        *PBTimestamp             `protobuf:"bytes,6,req,name=timestamp" json:"timestamp,omitempty"`
	Fields           []*PBLogEvent_PBLogField `protobuf:"bytes,5,rep,name=fields" json:"fields,omitempty"`
	XXX_unrecognized []byte                   `json:"-"`
}

func (m *PBLogEvent) Reset()         { *m = PBLogEvent{} }
func (m *PBLogEvent) String() string { return proto.CompactTextString(m) }
func (*PBLogEvent) ProtoMessage()    {}

func (m *PBLogEvent) GetTimestamp() *PBTimestamp {
	if m != nil {
		return m.Timestamp
	}
	return nil
}

func (m *PBLogEvent) GetFields() []*PBLogEvent_PBLogField {
	if m != nil {
		return m.Fields
	}
	return nil
}

type PBLogEvent_PBLogField struct {
	Key              *string `protobuf:"bytes,1,req,name=key" json:"key,omitempty"`
	Value            *string `protobuf:"bytes,2,req,name=value" json:"value,omitempty"`
	XXX_unrecognized []byte  `json:"-"`
}

func (m *PBLogEvent_PBLogField) Reset()         { *m = PBLogEvent_PBLogField{} }
func (m *PBLogEvent_PBLogField) String() string { return proto.CompactTextString(m) }
func (*PBLogEvent_PBLogField) ProtoMessage()    {}

func (m *PBLogEvent_PBLogField) GetKey() string {
	if m != nil && m.Key != nil {
		return *m.Key
	}
	return ""
}

func (m *PBLogEvent_PBLogField) GetValue() string {
	if m != nil && m.Value != nil {
		return *m.Value
	}
	return ""
}

type PBLogFrame struct {
	Version          *int32                        `protobuf:"varint,1,req,name=version" json:"version,omitempty"`
	Timestamp        *PBTimestamp                  `protobuf:"bytes,2,req,name=timestamp" json:"timestamp,omitempty"`
	Size             *uint32                       `protobuf:"varint,3,req,name=size" json:"size,omitempty"`
	Count            *uint32                       `protobuf:"varint,4,req,name=count" json:"count,omitempty"`
	Compression      *PBLogFrame_PBCompressionType `protobuf:"varint,5,req,name=compression,enum=events.PBLogFrame_PBCompressionType" json:"compression,omitempty"`
	Events           *PBLogFrame_PBLogEvents       `protobuf:"bytes,6,opt,name=events" json:"events,omitempty"`
	CompressedData   []byte                        `protobuf:"bytes,7,opt,name=compressedData" json:"compressedData,omitempty"`
	XXX_unrecognized []byte                        `json:"-"`
}

func (m *PBLogFrame) Reset()         { *m = PBLogFrame{} }
func (m *PBLogFrame) String() string { return proto.CompactTextString(m) }
func (*PBLogFrame) ProtoMessage()    {}

func (m *PBLogFrame) GetVersion() int32 {
	if m != nil && m.Version != nil {
		return *m.Version
	}
	return 0
}

func (m *PBLogFrame) GetTimestamp() *PBTimestamp {
	if m != nil {
		return m.Timestamp
	}
	return nil
}

func (m *PBLogFrame) GetSize() uint32 {
	if m != nil && m.Size != nil {
		return *m.Size
	}
	return 0
}

func (m *PBLogFrame) GetCount() uint32 {
	if m != nil && m.Count != nil {
		return *m.Count
	}
	return 0
}

func (m *PBLogFrame) GetCompression() PBLogFrame_PBCompressionType {
	if m != nil && m.Compression != nil {
		return *m.Compression
	}
	return PBLogFrame_NONE
}

func (m *PBLogFrame) GetEvents() *PBLogFrame_PBLogEvents {
	if m != nil {
		return m.Events
	}
	return nil
}

func (m *PBLogFrame) GetCompressedData() []byte {
	if m != nil {
		return m.CompressedData
	}
	return nil
}

type PBLogFrame_PBLogEvents struct {
	Events           []*PBLogEvent `protobuf:"bytes,1,rep,name=events" json:"events,omitempty"`
	XXX_unrecognized []byte        `json:"-"`
}

func (m *PBLogFrame_PBLogEvents) Reset()         { *m = PBLogFrame_PBLogEvents{} }
func (m *PBLogFrame_PBLogEvents) String() string { return proto.CompactTextString(m) }
func (*PBLogFrame_PBLogEvents) ProtoMessage()    {}

func (m *PBLogFrame_PBLogEvents) GetEvents() []*PBLogEvent {
	if m != nil {
		return m.Events
	}
	return nil
}

func init() {
	proto.RegisterEnum("events.PBLogFrame_PBCompressionType", PBLogFrame_PBCompressionType_name, PBLogFrame_PBCompressionType_value)
}

//////////////////////////////////////////////////////////////////////////////
// THE END
