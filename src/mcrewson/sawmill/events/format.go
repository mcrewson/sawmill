// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package events

import (
	"fmt"
	"regexp"
	"strconv"
)

//////////////////////////////////////////////////////////////////////////////

type evfmtVerb int

const (
	evfmtVerbStatic evfmtVerb = iota
	evfmtVerbUnixTime
	evfmtVerbFormattedTime
	evfmtVerbField
	evfmtVerbUnknown
)

type evfmtPart struct {
	verb  evfmtVerb
	field []byte
}

var evfmtRegex *regexp.Regexp = regexp.MustCompile(`%\{[^}]+\}`)

type EventFormatter struct {
	parts []evfmtPart
}

func NewEventFormatter(format string) (*EventFormatter, error) {
	var fmtr = &EventFormatter{}

	add := func(verb evfmtVerb, field []byte) {
		fmtr.parts = append(fmtr.parts, evfmtPart{verb, field})
	}

	matches := evfmtRegex.FindAllStringIndex(format, -1)
	if matches == nil {
		return nil, fmt.Errorf("invalid event format: %s", format)
	}

	prev := 0
	for _, m := range matches {
		start, end := m[0], m[1]
		if start > prev {
			add(evfmtVerbStatic, []byte(format[prev:start]))
		}

		name := format[m[0]+2 : m[1]-1]
		if name == "+%s" {
			add(evfmtVerbUnixTime, nil)
		} else if name[0] == '+' {
			add(evfmtVerbFormattedTime, []byte(name[1:]))
		} else {
			add(evfmtVerbField, []byte(name))
		}
		prev = end
	}
	end := format[prev:]
	if end != "" {
		add(evfmtVerbStatic, []byte(end))
	}

	return fmtr, nil
}

func (fmtr *EventFormatter) Format(event *LogEvent) []byte {
	output := []byte{}
	for _, part := range fmtr.parts {
		var value []byte
		switch part.verb {
		case evfmtVerbStatic:
			value = part.field
		case evfmtVerbUnixTime:
			value = []byte(strconv.FormatInt(event.Timestamp.Unix(), 10))
		case evfmtVerbFormattedTime:
			value = []byte(event.Timestamp.Format(string(part.field)))
		case evfmtVerbField:
			if event.Fields == nil {
				value = part.field
			} else {
				val, ok := event.Fields[string(part.field)]
				if ok {
					value = []byte(val)
				} else {
					value = part.field
				}
			}
		}
		output = append(output, value...)
	}
	return output
}

func (fmtr *EventFormatter) FormatString(event *LogEvent) string {
	return string(fmtr.Format(event))
}

//////////////////////////////////////////////////////////////////////////////
// THE END
