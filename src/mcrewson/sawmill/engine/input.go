// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package engine

import (
	"mcrewson/sawmill/events"
	"fmt"
	"time"

	"github.com/rcrowley/go-metrics"
)

//////////////////////////////////////////////////////////////////////////////

type InputChainable interface {
	Chainable
	AddField(string, string) error
	RemoveField(string) error

	AddOutputChannel(events.LogEventChannel) error
	RemoveOutputChannel(events.LogEventChannel) error
	HasMultipleOutputs() bool
}

//////////////////////////////////////////////////////////////////////////////

type BaseInput struct {
	BaseChainable
	fields  map[string]string
	outputs []events.LogEventChannel
	meter   metrics.Meter
}

func (bic *BaseInput) InitBaseInput(args map[string]string, eng *Engine) (err error) {
	err = bic.InitBaseChainable(args, eng, INPUT)
	if err != nil {
		return err
	}

	bic.fields = make(map[string]string)
	bic.outputs = make([]events.LogEventChannel, 0)

	bic.meter = metrics.NewMeter()
	eng.RegisterMetric("inputs."+bic.chainName+".events", bic.meter)

	return
}

func (bic *BaseInput) Stop() error {
	if !bic.IsRunning() {
		return nil
	}
	bic.MarkNotRunning()
	return nil
}

func (bic *BaseInput) Cleanup() error {
	if len(bic.outputs) > 0 {
		return fmt.Errorf("cannot delete input while it is part of a chain")
	}
	if bic.IsRunning() {
		if err := bic.Stop(); err != nil {
			return err
		}
	}
	return nil
}

func (bic *BaseInput) AddField(name, value string) error {
	bic.fields[name] = value
	return nil
}

func (bic *BaseInput) RemoveField(name string) error {
	delete(bic.fields, name)
	return nil
}

func (bic *BaseInput) AddOutputChannel(channel events.LogEventChannel) error {
	bic.outputs = append(bic.outputs, channel)
	return nil
}

func (bic *BaseInput) RemoveOutputChannel(channel events.LogEventChannel) error {
	for i := range bic.outputs {
		if bic.outputs[i] == channel {
			bic.outputs = append(bic.outputs[:i], bic.outputs[i+1:]...)
			return nil
		}
	}
	return fmt.Errorf("output channel not found in this input")
}

func (bic *BaseInput) HasMultipleOutputs() bool {
	if len(bic.outputs) > 1 {
		return true
	}
	return false
}

func (bic *BaseInput) AnnotateAndSendEvent(ev *events.LogEvent) {
	ev.AddFields(bic.fields)
	bic.SendEvent(ev)
}

func (bic *BaseInput) SendEvent(ev *events.LogEvent) {
	if len(bic.outputs) == 0 {
		bic.Engine.Logger.Warningf("input(%s) is not chained to anything. Nowhere to send this event. Dropping it.\n", bic.Name())
		return
	}
	for _, output := range bic.outputs {
		timeout := time.After(time.Duration(5) * time.Second)
		select {
		case output <- ev:
		case <-timeout:
			bic.Engine.Logger.Warningf("Input(%s): dropped event. Its output channel has been blocked for too long.\n", bic.Name())
		}
	}
	bic.meter.Mark(1)
}

//////////////////////////////////////////////////////////////////////////////
// THE END
