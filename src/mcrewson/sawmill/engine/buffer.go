// vim:set ts=4 sw=4 et nowrap syntax=go:
//

package engine

import (
	"mcrewson/sawmill/events"
	"mcrewson/sawmill/util/logging"
	"container/list"
	"fmt"
	"github.com/rcrowley/go-metrics"
	"math"
	"math/rand"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"sync"
	"time"
)

//////////////////////////////////////////////////////////////////////////////

type bufferImpl interface {
	BufferEvent(*events.LogEvent) error
	Flush(bool) error
	Clear() error
	Stop()
}

type FlushFunction func(*events.LogFrame) error

//////////////////////////////////////////////////////////////////////////////

type bufferImplNone struct {
	currentFrame  *events.LogFrame
	flushFunction FlushFunction
}

func newBufferImplNone(frameSize uint32, bufferLimit uint32, flushInterval time.Duration, flushFunc FlushFunction) *bufferImplNone {
	nb := &bufferImplNone{
		flushFunction: flushFunc,
		currentFrame:  events.NewLogFrame(1),
	}
	return nb
}

func (nb *bufferImplNone) BufferEvent(event *events.LogEvent) error {
	if err := nb.currentFrame.AddEvent(event); err != nil {
		return err
	}
	if err := nb.flushFunction(nb.currentFrame); err != nil {
		return err
	}
	nb.currentFrame.Reset()
	return nil
}

func (nb *bufferImplNone) Flush(force bool) error {
	return nil
}

func (nb *bufferImplNone) Clear() error {
	return nil
}

func (nb *bufferImplNone) Stop() {
}

//////////////////////////////////////////////////////////////////////////////

type bufferImplMemory struct {
	frameSize     uint32
	bufferLimit   uint32
	flushInterval time.Duration
	flushFunction FlushFunction

	queue        *list.List
	currentFrame *events.LogFrame
	frameLock    sync.Mutex
	stopping     bool
}

func newBufferImplMemory(frameSize uint32, bufferLimit uint32, flushInterval time.Duration, flushFunc FlushFunction) *bufferImplMemory {
	mb := &bufferImplMemory{
		frameSize:     frameSize,
		bufferLimit:   bufferLimit,
		flushInterval: flushInterval,
		flushFunction: flushFunc,
		stopping:      false,
	}
	mb.queue = list.New()
	mb.currentFrame = events.NewLogFrame(mb.frameSize)
	return mb
}

func (mb *bufferImplMemory) BufferEvent(event *events.LogEvent) error {
	mb.frameLock.Lock()
	defer mb.frameLock.Unlock()

	if err := mb.currentFrame.AddEvent(event); err != nil {
		if err = mb.pushCurrentFrame(); err != nil {
			return err
		}
		mb.currentFrame = events.NewLogFrame(mb.frameSize)
		if err = mb.currentFrame.AddEvent(event); err != nil {
			// should never happen ..
			return fmt.Errorf("WTF! DID NOT EXPECT THIS! %s", err)
		}
	}
	return nil
}

func (mb *bufferImplMemory) Flush(force bool) error {
	now := time.Now()
	for e := mb.queue.Back(); e != nil; e = e.Prev() {
		if !force && mb.stopping {
			return nil
		}

		frame := e.Value.(*events.LogFrame)
		if force || now.After(frame.Timestamp.Add(mb.flushInterval)) {
			if flushErr := mb.flushFunction(frame); flushErr != nil {
				return flushErr
			}
			mb.queue.Remove(e)
		}
	}

	if force || now.After(mb.currentFrame.Timestamp.Add(mb.flushInterval)) {
		mb.frameLock.Lock()
		defer mb.frameLock.Unlock()
		if flushErr := mb.flushFunction(mb.currentFrame); flushErr != nil {
			return flushErr
		}
		mb.currentFrame.Reset()
	}
	return nil
}

func (mb *bufferImplMemory) Clear() error {
	mb.queue.Init()
	mb.currentFrame.Reset()
	return nil
}

func (mb *bufferImplMemory) Stop() {
	mb.stopping = true
}

func (mb *bufferImplMemory) pushCurrentFrame() error {
	if uint32(mb.queue.Len()) == mb.bufferLimit {
		lastFrame := mb.queue.Back()
		if flushErr := mb.flushFunction(lastFrame.Value.(*events.LogFrame)); flushErr != nil {
			return flushErr
		}
		mb.queue.Remove(lastFrame)
	}
	mb.queue.PushFront(mb.currentFrame)
	return nil
}

//////////////////////////////////////////////////////////////////////////////

type bufferImplFile struct {
	path          string
	frameSize     uint32
	bufferLimit   uint32
	flushInterval time.Duration
	flushFunction FlushFunction

	queue                *list.List
	currentFrame         *events.LogFrame
	currentFrameFilename string
	pushingFrame         bool
	frameLock            sync.Mutex
	writeLock            sync.Mutex
	codec                events.EventCodec
	depth                metrics.Counter
	stopping             bool
}

func newBufferImplFile(path string, frameSize uint32, bufferLimit uint32, flushInterval time.Duration, flushFunc FlushFunction, eng *Engine) *bufferImplFile {
	fb := &bufferImplFile{
		path:          path,
		frameSize:     frameSize,
		bufferLimit:   bufferLimit,
		flushInterval: flushInterval,
		flushFunction: flushFunc,
		codec:         events.NewProtobufEventCodec(false),
		stopping:      false,
	}

	rand.Seed(time.Now().Unix())

	fb.currentFrame = events.NewLogFrame(fb.frameSize)
	fb.currentFrameFilename = fb.generateFilename()
	fb.pushingFrame = false

	fb.depth = metrics.NewCounter()
	eng.RegisterMetric("buffer.depth", fb.depth)

	return fb
}

func (fb *bufferImplFile) BufferEvent(event *events.LogEvent) error {
	fb.frameLock.Lock()
	defer fb.frameLock.Unlock()
	if err := fb.currentFrame.AddEvent(event); err != nil {
		if err = fb.pushCurrentFrame(); err != nil {
			return err
		}
		fb.currentFrame = events.NewLogFrame(fb.frameSize)
		fb.currentFrameFilename = fb.generateFilename()
		if err = fb.currentFrame.AddEvent(event); err != nil {
			// should never happen ...
			return fmt.Errorf("WTF! DID NOT EXPECT THIS! %s", err)
		}
	}
	fb.depth.Inc(1)
	return nil
}

func (fb *bufferImplFile) Flush(force bool) error {
	now := time.Now()

	filenames, err := filepath.Glob(fb.path)
	if err != nil {
		return err
	}

	for _, filename := range filenames {
		if !force && fb.stopping {
			// break out when shutting down (flush hasn't been forced) ...
			return nil
		}

		if filename == fb.currentFrameFilename {
			continue
		}

		payload, readErr := fb.readData(filename)
		if readErr != nil {
			if os.IsNotExist(readErr) {
				continue
			}
			return fmt.Errorf("%s: %s", filename, readErr)
		}
		frame, codecErr := fb.codec.DecodeFrame(payload)
		if codecErr != nil {
			return fmt.Errorf("%s: %s", filename, codecErr)
		}

		if force || now.After(frame.Timestamp.Add(fb.flushInterval)) {
			if flushErr := fb.flushFunction(frame); flushErr != nil {
				return flushErr
			}
			if removeErr := os.Remove(filename); removeErr != nil {
				if os.IsNotExist(removeErr) {
					continue
				}
				return fmt.Errorf("%s: %s", filename, removeErr)
			}
			fb.depth.Dec(int64(frame.Count))
		}
	}

	// TODO: flush the currentFrame if it is old too!
	if fb.currentFrame.Count > 0 && (force || now.After(fb.currentFrame.Timestamp.Add(fb.flushInterval))) {

		fb.frameLock.Lock()
		frameCopy := fb.currentFrame.Copy()
		fb.currentFrame.Reset()
		fb.frameLock.Unlock()

		if flushErr := fb.flushFunction(frameCopy); flushErr != nil {
			return flushErr
		}
		fb.depth.Dec(int64(frameCopy.Count))
	}

	// Write the current frame to disk, for safety and justice!
	fb.frameLock.Lock()
	defer fb.frameLock.Unlock()
	if writeErr := fb.writeFrame(fb.currentFrameFilename, fb.currentFrame); writeErr != nil {
		return writeErr
	}

	return nil
}

func (fb *bufferImplFile) Clear() error {
	filenames, err := filepath.Glob(fb.path)
	if err != nil {
		return err
	}
	for _, filename := range filenames {
		if removeErr := os.Remove(filename); removeErr != nil {
			if os.IsNotExist(removeErr) {
				continue
			}
			return removeErr
		}
	}

	fb.currentFrame.Reset()
	return nil
}

func (fb *bufferImplFile) Stop() {
	fb.stopping = true
}

func (fb *bufferImplFile) pushCurrentFrame() error {
	if writeErr := fb.writeFrame(fb.currentFrameFilename, fb.currentFrame); writeErr != nil {
		return writeErr
	}

	go func() error {
		if fb.pushingFrame {
			return nil
		}
		fb.pushingFrame = true

		filenames, globErr := filepath.Glob(fb.path)
		if globErr != nil {
			fb.pushingFrame = false
			return globErr
		}

		for uint32(len(filenames)) > fb.bufferLimit {
			if fb.stopping {
				// break out of the loop if shutting down...
				return nil
			}

			var oldest string = ""
			var oldestidx int
			tstamp := time.Now()
			for i, name := range filenames {
				if name == fb.currentFrameFilename {
					continue
				}
				fileinfo, statErr := os.Stat(name)
				if statErr != nil {
					continue
				}
				if fileinfo.ModTime().Before(tstamp) {
					oldest = name
					oldestidx = i
					tstamp = fileinfo.ModTime()
				}
			}
			if oldest == "" {
				fb.pushingFrame = false
				return nil
			}

			payload, readErr := fb.readData(oldest)
			if readErr != nil {
				if os.IsNotExist(readErr) {
					filenames = append(filenames[:oldestidx], filenames[oldestidx+1:]...)
					continue
				}
				fb.pushingFrame = false
				return readErr
			}
			oldestFrame, codecErr := fb.codec.DecodeFrame(payload)
			if codecErr != nil {
				fb.pushingFrame = false
				return codecErr
			}

			if flushErr := fb.flushFunction(oldestFrame); flushErr != nil {
				fb.pushingFrame = false
				return flushErr
			}

			if removeErr := os.Remove(oldest); removeErr != nil {
				if os.IsNotExist(removeErr) {
					filenames = append(filenames[:oldestidx], filenames[oldestidx+1:]...)
					continue
				}
				fb.pushingFrame = false
				return removeErr
			}

			fb.depth.Dec(int64(oldestFrame.Count))

			filenames = append(filenames[:oldestidx], filenames[oldestidx+1:]...)
		}

		fb.pushingFrame = false
		return nil
	}()

	return nil
}

func (fb *bufferImplFile) generateFilename() string {
	r := rand.Int63()
	return strings.Replace(fb.path, "*", strconv.FormatInt(r, 16), -1)
}

func (fb *bufferImplFile) writeFrame(filename string, frame *events.LogFrame) error {
	// frameLock must already be held by caller...
	frameCopy := frame.Copy()
	go func() error {
		payload, encErr := fb.codec.EncodeFrame(frameCopy)
		if encErr != nil {
			return encErr
		}

		fb.writeLock.Lock()
		defer fb.writeLock.Unlock()

		fp, err := os.OpenFile(filename, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0666)
		if err != nil {
			return err
		}
		defer fp.Close()

		_, werr := fp.Write(payload)
		if werr != nil {
			return werr
		}
		return nil
	}()
	return nil
}

func (fb *bufferImplFile) readData(filename string) ([]byte, error) {
	fp, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer fp.Close()

	fileinfo, statErr := fp.Stat()
	if statErr != nil {
		return nil, err
	}
	sz := fileinfo.Size()

	payload := make([]byte, sz)

	_, rerr := fp.Read(payload)
	if rerr != nil {
		return nil, rerr
	}
	return payload, nil
}

//////////////////////////////////////////////////////////////////////////////

type BufferController struct {
	buffer       bufferImpl
	interval     time.Duration
	retryWait    float64
	maxRetryWait float64
	retryLimit   uint32
	flushOnStop  bool

	numErrors  uint32
	stopSignal chan bool
	logger     *logging.Logger
}

var default_buffer_frame_size uint32 = 128          // events
var default_buffer_limit uint32 = 512               // frames
var default_buffer_flush_interval time.Duration = 0 // seconds (0 == disabled)
var default_retry_wait float64 = 1.0                // flush intervals
var default_max_retry_wait float64 = 0.0
var default_retry_limit uint32 = 17

func NewBuffer(args map[string]string, eng *Engine, flushFunc FlushFunction) (*BufferController, error) {
	frame_size := default_buffer_frame_size
	buffer_limit := default_buffer_limit
	flush_interval := default_buffer_flush_interval
	retry_wait := default_retry_wait
	max_retry_wait := default_max_retry_wait
	retry_limit := default_retry_limit

	val, ok := args["buffertype"]
	if !ok {
		return nil, fmt.Errorf("buffertype must be specified")
	}
	buffer_type := val

	val, ok = args["bufferframesize"]
	if ok {
		i, err := strconv.ParseUint(val, 10, 32)
		if err != nil {
			return nil, fmt.Errorf("bufferframesize must be a positive integer")
		}
		frame_size = uint32(i)
	}

	val, ok = args["bufferlimit"]
	if ok {
		i, err := strconv.ParseUint(val, 10, 32)
		if err != nil {
			return nil, fmt.Errorf("bufferlimit must be a positive integer")
		}
		buffer_limit = uint32(i)
	}

	val, ok = args["flushinterval"]
	if ok {
		i, err := strconv.ParseUint(val, 10, 64)
		if err != nil {
			return nil, fmt.Errorf("flushinterval must be a positive integer")
		}
		flush_interval = time.Duration(i) * time.Second
	}

	val, ok = args["retrywait"]
	if ok {
		f, err := strconv.ParseFloat(val, 64)
		if err != nil {
			return nil, fmt.Errorf("retrywait must be a positive float")
		}
		retry_wait = f
	}

	val, ok = args["maxretrywait"]
	if ok {
		f, err := strconv.ParseFloat(val, 64)
		if err != nil {
			return nil, fmt.Errorf("maxretrywait must be a positive float")
		}
		max_retry_wait = f
	}

	val, ok = args["retrylimit"]
	if ok {
		i, err := strconv.ParseUint(val, 10, 32)
		if err != nil {
			return nil, fmt.Errorf("retrylimit must be a positive integer")
		}
		retry_limit = uint32(i)
	}

	var flush_on_stop bool
	var buffer bufferImpl
	switch {
	case buffer_type == "none":
		buffer = newBufferImplNone(frame_size, buffer_limit, flush_interval, flushFunc)

	case buffer_type == "memory":
		buffer = newBufferImplMemory(frame_size, buffer_limit, flush_interval, flushFunc)

		val, ok = args["flushonstop"]
		if ok {
			bval, berr := strconv.ParseBool(val)
			if berr != nil {
				return nil, fmt.Errorf("flushonstop must be a boolean value")
			}
			flush_on_stop = bval
		} else {
			flush_on_stop = true
		}

	case buffer_type == "file":
		val, ok = args["bufferpath"]
		if !ok {
			return nil, fmt.Errorf("bufferpath must be specified")
		}
		buffer_path := val
		buffer = newBufferImplFile(buffer_path, frame_size, buffer_limit, flush_interval, flushFunc, eng)

		val, ok = args["flushonstop"]
		if ok {
			bval, berr := strconv.ParseBool(val)
			if berr != nil {
				return nil, fmt.Errorf("flushonstop must be a boolean value")
			}
			flush_on_stop = bval
		} else {
			flush_on_stop = false
		}

	default:
		return nil, fmt.Errorf("unknown buffertype value: \"%s\"", buffer_type)
	}

	b := &BufferController{
		buffer:      buffer,
		interval:    flush_interval,
		flushOnStop: flush_on_stop,

		numErrors:    0,
		retryWait:    retry_wait,
		maxRetryWait: max_retry_wait,
		retryLimit:   retry_limit,
		stopSignal:   make(chan bool, 0),
		logger:       eng.Logger,
	}

	return b, nil
}

func (b *BufferController) Start() {
	if b.interval > 0 {
		go b._flushTimer()
	}
}

func (b *BufferController) Stop() {
	b.buffer.Stop()
	if b.flushOnStop {
		if flushErr := b.buffer.Flush(true); flushErr != nil {
			b.logger.Warningf("buffer flush at stop failed: %s\n", flushErr)
		}
	}
	if b.interval > 0 {
		b.stopSignal <- true
	}
}

func (b *BufferController) BufferEvent(event *events.LogEvent) error {
	err := b.buffer.BufferEvent(event)
	if err != nil {
		b.logger.Warningf("Failed to buffer an event. Is the buffer too small? (err = %s)\n", err)
	}
	return err
}

func (b *BufferController) _flushTimer() {
	ticker := time.NewTicker(b.interval)
spool_runloop:
	for {
		select {
		case <-ticker.C:
			flushErr := b.buffer.Flush(false)
			if flushErr != nil {
				b.numErrors += 1
				if b.retryLimit == 0 || b.numErrors <= b.retryLimit {
					b.logger.Warningf("temporarily failed to flush the buffer: %s\n", flushErr)
					wait := b._calcRetryWait()
					b.logger.Debugf("next flush in %0.2f seconds\n", wait.Seconds())
					ticker.Stop()
					ticker = time.NewTicker(wait)
				} else {
					b.logger.Warningf("failed to flush the buffer: %s\n", flushErr)
					b.logger.Warning("error count exceeds the retry limit. clearing the buffer. events will be lost.\n")
					if clearErr := b.buffer.Clear(); clearErr != nil {
						b.logger.Errorf("WTF! Unexpected error while clearing the buffer: %s\n", clearErr)
					}
					b.numErrors = 0
					ticker.Stop()
					ticker = time.NewTicker(b.interval)
				}
			} else {
				if b.numErrors > 0 {
					b.logger.Info("retry succeeded.\n")
					b.numErrors = 0
					ticker.Stop()
					ticker = time.NewTicker(b.interval)
				}
			}

		case <-b.stopSignal:
			break spool_runloop
		}
	}
	ticker.Stop()
}

func (b *BufferController) _calcRetryWait() time.Duration {
	var wait float64 = b.interval.Seconds() * (b.retryWait * math.Pow(2, float64(b.numErrors-1)))
	wait += rand.Float64()*(wait/4.0) - (wait / 8.0)
	if b.maxRetryWait > 0 {
		wait = math.Min(wait, b.maxRetryWait)
	}
	return time.Duration(wait) * time.Second
}

//////////////////////////////////////////////////////////////////////////////
// THE END
