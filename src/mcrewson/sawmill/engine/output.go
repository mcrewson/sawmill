// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package engine

import (
	"mcrewson/sawmill/events"
	"fmt"

	"github.com/rcrowley/go-metrics"
)

//////////////////////////////////////////////////////////////////////////////

type OutputChainable interface {
	Chainable
	AddRequiredField(string, string) error
	RemoveRequiredField(string) error
	AddExcludedField(string, string) error
	RemoveExcludedField(string) error

	AddInputChannel(events.LogEventChannel) error
	RemoveInputChannel(events.LogEventChannel) error
}

//////////////////////////////////////////////////////////////////////////////

type OutputFunc func(*events.LogEvent) error

type BaseOutput struct {
	BaseChainable
	requiredFields map[string]string
	excludedFields map[string]string
	inputs         []events.LogEventChannel
	stopSignal     chan bool
	outputFunc     OutputFunc
	meter          metrics.Meter
}

func (boc *BaseOutput) InitBaseOutput(args map[string]string, eng *Engine, outputFunc OutputFunc) (err error) {
	err = boc.InitBaseChainable(args, eng, OUTPUT)
	if err != nil {
		return err
	}

	boc.requiredFields = make(map[string]string)
	boc.excludedFields = make(map[string]string)
	boc.inputs = make([]events.LogEventChannel, 0)
	boc.stopSignal = make(chan bool, 0)
	boc.outputFunc = outputFunc

	boc.meter = metrics.NewMeter()
	eng.RegisterMetric("outputs."+boc.chainName+".events", boc.meter)

	return
}

func (boc *BaseOutput) Start() error {
	if boc.IsRunning() {
		return nil
	}

	if len(boc.inputs) == 0 {
		return fmt.Errorf("not starting output(%s). It is not chained to anything and will never receive an event.", boc.Name())
	} else if len(boc.inputs) == 1 {
		boc.StartWithSingleInput()
	} else {
		boc.StartWithMultipleInputs()
	}

	boc.MarkRunning()
	boc.Engine.Logger.Infof("Output(%s): Started.\n", boc.Name())
	return nil
}

func (boc *BaseOutput) StartWithSingleInput() {
	go func() {
	single_runloop:
		for {
			select {
			case ev := <-boc.inputs[0]:
				if !boc.excludeEvent(ev) {
					if err := boc.outputFunc(ev); err != nil {
						boc.Engine.Logger.Warningf("BaseOutput(%s) Cannot output event: %s\n", boc.Name(), err)
					}
				}
			case <-boc.stopSignal:
				break single_runloop
			}
		}
	}()
}

func (boc *BaseOutput) StartWithMultipleInputs() {
	input := make(events.LogEventChannel, boc.Capacity())
	input_stops := make([]chan bool, len(boc.inputs))
	for i := range boc.inputs {
		input_stops[i] = make(chan bool)

		go func(ch events.LogEventChannel, stop chan bool) {
		inputloop:
			for {
				select {
				case ev := <-ch:
					select {
					case input <- ev:
					default:
						boc.Engine.Logger.Warningf("Output(%s): dropped event. Its input channel is blocked.\n", boc.Name())
					}
				case <-stop:
					break inputloop
				}
			}
		}(boc.inputs[i], input_stops[i])
	}

	go func() {
	multi_runloop:
		for {
			select {
			case ev := <-input:
				if !boc.excludeEvent(ev) {
					if err := boc.outputFunc(ev); err != nil {
						boc.Engine.Logger.Warningf("BaseOutput(%s) Cannot output event: %s\n", boc.Name(), err)
					}
				}
			case <-boc.stopSignal:
				for _, signal := range input_stops {
					signal <- true
				}
				break multi_runloop
			}
		}
	}()
}

func (boc *BaseOutput) Stop() error {
	if !boc.IsRunning() {
		return nil
	}
	boc.stopSignal <- true
	boc.MarkNotRunning()
	boc.Engine.Logger.Infof("Output(%s): Stopped.\n", boc.Name())
	return nil
}

func (boc *BaseOutput) Cleanup() error {
	if len(boc.inputs) > 0 {
		return fmt.Errorf("cannot delete output while it is part of a chain")
	}
	if boc.IsRunning() {
		if err := boc.Stop(); err != nil {
			return err
		}
	}
	return nil
}

func (boc *BaseOutput) AddRequiredField(name, value string) error {
	boc.requiredFields[name] = value
	return nil
}

func (boc *BaseOutput) RemoveRequiredField(name string) error {
	delete(boc.requiredFields, name)
	return nil
}

func (boc *BaseOutput) AddExcludedField(name, value string) error {
	boc.excludedFields[name] = value
	return nil
}

func (boc *BaseOutput) RemoveExcludedField(name string) error {
	delete(boc.excludedFields, name)
	return nil
}

func (boc *BaseOutput) AddInputChannel(channel events.LogEventChannel) error {
	boc.inputs = append(boc.inputs, channel)
	return nil
}

func (boc *BaseOutput) RemoveInputChannel(channel events.LogEventChannel) error {
	for i := range boc.inputs {
		if boc.inputs[i] == channel {
			boc.inputs = append(boc.inputs[:i], boc.inputs[i+1:]...)
			return nil
		}
	}
	return fmt.Errorf("input channel not found in this output")
}

func (boc *BaseOutput) MeterMark(n int64) {
	boc.meter.Mark(n)
}

func (boc *BaseOutput) excludeEvent(ev *events.LogEvent) bool {
	if boc.requiredFields != nil && len(boc.requiredFields) > 0 {
		found := false
	requiredloop:
		for rKey, rVal := range boc.requiredFields {
			for evKey, evVal := range ev.Fields {
				if rKey == evKey && (rVal == evVal || rVal == "*") {
					found = true
					break requiredloop
				}
			}
		}
		if !found {
			return true
		}
	}

	if boc.excludedFields != nil && len(boc.excludedFields) > 0 {
		for xKey, xVal := range boc.excludedFields {
			for evKey, evVal := range ev.Fields {
				if xKey == evKey && (xVal == evVal || xVal == "*") {
					return true
				}
			}
		}
	}
	return false
}

//////////////////////////////////////////////////////////////////////////////

type BufferedOutput struct {
	BaseOutput
	buffer *BufferController
}

func (buf *BufferedOutput) InitBufferedOutput(args map[string]string, eng *Engine, flushFunc FlushFunction) (err error) {
	buf.buffer, err = NewBuffer(args, eng, flushFunc)
	buf.InitBaseOutput(args, eng, buf.buffer.BufferEvent)
	return
}

func (buf *BufferedOutput) Start() error {
	if buf.IsRunning() {
		return nil
	}

	buf.buffer.Start()
	return buf.BaseOutput.Start()
}

func (buf *BufferedOutput) Stop() error {
	if !buf.IsRunning() {
		return nil
	}

	buf.buffer.Stop()
	return buf.BaseOutput.Stop()
}

//////////////////////////////////////////////////////////////////////////////
// THE END
