// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package engine

import (
	"mcrewson/sawmill/events"
	"github.com/rcrowley/go-metrics"
)

//////////////////////////////////////////////////////////////////////////////

type FilterChainable interface {
	Chainable
	AddRequiredField(string, string) error
	RemoveRequiredField(string) error
	AddExcludedField(string, string) error
	RemoveExcludedField(string) error

	FilterEvent(*events.LogEvent) *events.LogEvent
}

//////////////////////////////////////////////////////////////////////////////

type FilterFunc func(*events.LogEvent) *events.LogEvent

type BaseFilter struct {
	BaseChainable
	requiredFields map[string]string
	excludedFields map[string]string
	filterFunc     FilterFunc
	meter          metrics.Meter
}

func (bfc *BaseFilter) InitBaseFilter(args map[string]string, eng *Engine, filterFunc FilterFunc) (err error) {
	err = bfc.InitBaseChainable(args, eng, FILTER)
	if err != nil {
		return err
	}

	bfc.requiredFields = make(map[string]string)
	bfc.excludedFields = make(map[string]string)
	bfc.filterFunc = filterFunc

	bfc.meter = metrics.NewMeter()
	eng.RegisterMetric("filters."+bfc.chainName+".events", bfc.meter)

	return
}

func (bfc *BaseFilter) Start() error {
	if bfc.IsRunning() {
		return nil
	}
	bfc.MarkRunning()
	bfc.Engine.Logger.Infof("Filter(%s): Started.\n", bfc.Name())
	return nil
}

func (bfc *BaseFilter) Stop() error {
	if !bfc.IsRunning() {
		return nil
	}
	bfc.MarkNotRunning()
	bfc.Engine.Logger.Infof("Filter(%s): Stopped.\n", bfc.Name())
	return nil
}

func (bfc *BaseFilter) Cleanup() error {
	if bfc.IsRunning() {
		if err := bfc.Stop(); err != nil {
			return err
		}
	}
	return nil
}

func (bfc *BaseFilter) AddRequiredField(name, value string) error {
	bfc.requiredFields[name] = value
	return nil
}

func (bfc *BaseFilter) RemoveRequiredField(name string) error {
	delete(bfc.requiredFields, name)
	return nil
}

func (bfc *BaseFilter) AddExcludedField(name, value string) error {
	bfc.excludedFields[name] = value
	return nil
}

func (bfc *BaseFilter) RemoveExcludedField(name string) error {
	delete(bfc.excludedFields, name)
	return nil
}

func (bfc *BaseFilter) FilterEvent(ev *events.LogEvent) *events.LogEvent {
	if bfc.excludeEvent(ev) == true {
		return ev
	}
	bfc.meter.Mark(1)
	return bfc.filterFunc(ev)
}

func (bfc *BaseFilter) excludeEvent(ev *events.LogEvent) bool {
	if bfc.requiredFields != nil && len(bfc.requiredFields) > 0 {
		found := false
	requiredloop:
		for rKey, rVal := range bfc.requiredFields {
			for evKey, evVal := range ev.Fields {
				if rKey == evKey && (rVal == evVal || rVal == "*") {
					found = true
					break requiredloop
				}
			}
		}
		if !found {
			return true
		}
	}

	if bfc.excludedFields != nil && len(bfc.excludedFields) > 0 {
		for xKey, xVal := range bfc.excludedFields {
			for evKey, evVal := range ev.Fields {
				if xKey == evKey && (xVal == evVal || xVal == "*") {
					return true
				}
			}
		}
	}
	return false
}

//////////////////////////////////////////////////////////////////////////////
// THE END
