// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package inputs

import (
	"fmt"
	"strings"

	"mcrewson/sawmill/engine"
	"mcrewson/sawmill/events"
	"mcrewson/sawmill/util"
	"mcrewson/sawmill/util/filewatch"
)

func init() {
	engine.RegisterInput("file", FileInit)
}

//////////////////////////////////////////////////////////////////////////////

type FileInput struct {
	engine.BaseInput

	Paths   []string
	StartAt int
	SinceDB string

	tail *filewatch.Tail
}

var default_sincedb = "/tmp/sawmill.sincedb"

func FileInit(args map[string]string, eng *engine.Engine) (engine.InputChainable, error) {
	val, ok := args["paths"]
	if !ok {
		val, ok = args["path"]
		if !ok {
			return nil, fmt.Errorf("no paths defined for this file input")
		}
	}
	paths := strings.Split(val, ",")
	for i := range paths {
		paths[i] = strings.TrimSpace(paths[i])
		if paths[i] == "-" {
			return nil, fmt.Errorf("Unsupported file path: %s", paths[i])
		}
	}

	startat := filewatch.FILEWATCH_START_NEW_FILES_AT_END
	val, ok = args["startat"]
	if ok {
		val = strings.ToLower(val)
		if val == "begin" || val == "beginning" {
			startat = filewatch.FILEWATCH_START_NEW_FILES_AT_BEGINNING
		} else if val == "end" {
			startat = filewatch.FILEWATCH_START_NEW_FILES_AT_END
		} else {
			return nil, fmt.Errorf("Invalid 'startat' value: %s", val)
		}
	}

	val, ok = args["sincedb"]
	if !ok {
		val = default_sincedb
	}
	sincedb := val

	tailconfig := &filewatch.TailConfig{
		StartNewFilesAt: startat,
		SinceDBPath:     sincedb,
	}

	chi := &FileInput{
		Paths:   paths,
		StartAt: startat,
		SinceDB: sincedb,
		tail:    filewatch.NewTail(tailconfig),
	}
	chi.InitBaseInput(args, eng)
	return chi, nil
}

func (f *FileInput) String() string {
	s := fmt.Sprintf("name=%s type=file paths=%s", util.SafeFormat(f.Name()), util.SafeFormat(f.Paths...))
	if f.StartAt == filewatch.FILEWATCH_START_NEW_FILES_AT_BEGINNING {
		s = s + " startat=beginning"
	}
	if f.SinceDB != default_sincedb {
		s = s + " sincedb=" + f.SinceDB
	}
	return s
}

func (f *FileInput) Start() error {
	if f.IsRunning() {
		return nil
	}

	tailcallback := func(path, line string) {
		event := events.NewLogEvent(strings.TrimSpace(line))
		event.Fields["path"] = path
		f.AnnotateAndSendEvent(event)
	}

	for _, path := range f.Paths {
		f.tail.Tail(path)
	}

	f.tail.Subscribe(tailcallback)

	f.MarkRunning()
	f.Engine.Logger.Infof("FileInput(%s): Started.\n", f.Name())
	return nil
}

func (f *FileInput) Stop() error {
	if !f.IsRunning() {
		return nil
	}
	f.tail.Quit()
	f.Engine.Logger.Infof("FileInput(%s): Stopped.\n", f.Name())
	return f.BaseInput.Stop()
}

//////////////////////////////////////////////////////////////////////////////
// THE END
