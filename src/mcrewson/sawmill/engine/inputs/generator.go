// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package inputs

import (
	"fmt"
	"math/rand"
	"strconv"
	"strings"
	"time"

	"mcrewson/sawmill/engine"
	"mcrewson/sawmill/events"
	"mcrewson/sawmill/util"
)

func init() {
	engine.RegisterInput("generator", GeneratorInit)
}

//////////////////////////////////////////////////////////////////////////////

type GeneratorInput struct {
	engine.BaseInput
	WordsizeMin, WordsizeMax       int
	MessagesizeMin, MessagesizeMax int
	MessageDelay                   time.Duration

	stopSignal chan bool
}

var default_wordsize string = "4-8"
var default_messagesize string = "7-15"

func GeneratorInit(args map[string]string, eng *engine.Engine) (engine.InputChainable, error) {
	val, ok := args["rate"]
	if !ok {
		return nil, fmt.Errorf("no rate specified for this generator input")
	}
	rate, err := strconv.ParseUint(val, 10, 64)
	if err != nil {
		return nil, fmt.Errorf("rate must be a positive integer")
	}
	message_delay := time.Second / time.Duration(rate)

	var wordsize_min, wordsize_max, messagesize_min, messagesize_max int

	wordsize_min, wordsize_max, err = getNumberRange(args, "wordsize", default_wordsize)
	messagesize_min, messagesize_max, err = getNumberRange(args, "messagesize", default_messagesize)

	chi := &GeneratorInput{
		WordsizeMin:    wordsize_min,
		WordsizeMax:    wordsize_max,
		MessagesizeMin: messagesize_min,
		MessagesizeMax: messagesize_max,
		MessageDelay:   message_delay,
		stopSignal:     make(chan bool, 0),
	}
	chi.InitBaseInput(args, eng)

	return chi, nil
}

func (g *GeneratorInput) String() string {
	s := fmt.Sprintf("name=%s type=generator rate=%d", util.SafeFormat(g.Name()), time.Second/g.MessageDelay)

	var wordsize string
	if g.WordsizeMin == g.WordsizeMax {
		wordsize = strconv.Itoa(g.WordsizeMin)
	} else {
		wordsize = fmt.Sprintf("%d-%d", g.WordsizeMin, g.WordsizeMax)
	}
	if wordsize != default_wordsize {
		s = s + " wordsize=" + wordsize
	}

	var messagesize string
	if g.MessagesizeMin == g.MessagesizeMax {
		messagesize = strconv.Itoa(g.MessagesizeMin)
	} else {
		messagesize = fmt.Sprintf("%d-%d", g.MessagesizeMin, g.MessagesizeMax)
	}
	if messagesize != default_messagesize {
		s = s + " messagesize=" + messagesize
	}

	return s
}

func (g *GeneratorInput) Start() error {
	if g.IsRunning() {
		return nil
	}

	rand.Seed(time.Now().UnixNano())
	loopfunc := func() {
		g.Engine.Logger.Infof("GeneratorInput(%s): Started.\n", g.Name())
	runloop:
		for {
			select {
			case <-g.stopSignal:
				break runloop
			default:
				message := randomMessage(g.WordsizeMin, g.WordsizeMax,
					g.MessagesizeMin, g.MessagesizeMax)

				event := events.NewLogEvent(message)
				g.AnnotateAndSendEvent(event)

				time.Sleep(g.MessageDelay)
			}
		}
		g.Engine.Logger.Infof("GeneratorInput(%s): Stopped.\n", g.Name())
	}

	go loopfunc()
	g.MarkRunning()
	return nil
}

func (g *GeneratorInput) Stop() error {
	if !g.IsRunning() {
		return nil
	}
	g.stopSignal <- true
	return g.BaseInput.Stop()
}

//////////////////////////////////////////////////////////////////////////////

func getNumberRange(args map[string]string, key, defaultval string) (min, max int, err error) {
	val, ok := args[key]
	if !ok {
		val = defaultval
	}

	parts := strings.SplitN(val, "-", 2)
	if len(parts) == 1 {
		num, e := strconv.Atoi(parts[0])
		if e != nil {
			return 0, 0, e
		}
		return num, num, nil
	}

	num, e := strconv.Atoi(parts[0])
	if e != nil {
		return 0, 0, e
	} else if num < 1 {
		return 0, 0, fmt.Errorf("minimum size is 1")
	}
	min = num

	num, e = strconv.Atoi(parts[1])
	if e != nil {
		return 0, 0, e
	} else if num < min {
		return 0, 0, fmt.Errorf("maximize cannot be smaller than minimum")
	}
	max = num

	return
}

const chars = "abcdefghijklmnopqrstuvwxyz1234567890"

func randomString(min, max int) string {
	sz := max
	if min < max {
		sz = rand.Intn(max-min) + min
	}
	buf := make([]byte, sz)
	for i := 0; i < sz; i++ {
		buf[i] = chars[rand.Intn(len(chars)-1)]
	}
	return string(buf)
}

func randomMessage(wordMin, wordMax, messageMin, messageMax int) string {
	sz := messageMax
	if messageMin < messageMax {
		sz = rand.Intn(messageMax-messageMin) + messageMin
	}
	words := make([]string, sz)
	for i := 0; i < sz; i++ {
		words[i] = randomString(wordMin, wordMax)
	}
	return strings.Join(words, " ")
}

//////////////////////////////////////////////////////////////////////////////
// THE END
