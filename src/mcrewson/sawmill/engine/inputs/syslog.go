// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package inputs

import (
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"

	"mcrewson/sawmill/engine"
	"mcrewson/sawmill/events"
	"mcrewson/sawmill/util"
	"mcrewson/sawmill/util/syslogsrv"
)

func init() {
	engine.RegisterInput("syslog", SyslogInit)
}

//////////////////////////////////////////////////////////////////////////////

type SyslogInput struct {
	engine.BaseInput

	Sockets           []string
	IgnoredPriorities []int
	CleanupSockets    bool

	server          *syslogsrv.Server
	cleanup_sockets []string
}

func SyslogInit(args map[string]string, eng *engine.Engine) (engine.InputChainable, error) {

	ignored := make([]int, 0)
	val, ok := args["ignoredpriorities"]
	if ok {
		for _, prior := range strings.Split(val, ";") {
			priors := parsePriorityString(strings.TrimSpace(prior))
			for i := range priors {
				ignored = append(ignored, priors[i])
			}
			eng.Logger.Debugf("SyslogInput: ignoring the following priorities: %v\n", ignored)
		}
	}

	val, ok = args["sockets"]
	if !ok {
		val, ok = args["socket"]
		if !ok {
			return nil, fmt.Errorf("no sockets defined for this syslog input")
		}
	}
	sockets := strings.Split(val, ",")
	for i := range sockets {
		sockets[i] = strings.TrimSpace(sockets[i])
	}

	val, ok = args["cleanup"]
	if !ok {
		val = "false"
	}
	cleanup, err := strconv.ParseBool(val)
	if err != nil {
		return nil, fmt.Errorf("cleanup parameter must have a boolean value for this syslog input")
	}

	chi := &SyslogInput{
		Sockets:           sockets,
		IgnoredPriorities: ignored,
		CleanupSockets:    cleanup,
	}
	chi.InitBaseInput(args, eng)

	return chi, nil
}

func (s *SyslogInput) String() string {
	st := fmt.Sprintf("name=%s type=syslog sockets=%s", util.SafeFormat(s.Name()), util.SafeFormat(s.Sockets...))

	if s.CleanupSockets {
		st = st + " cleanup=true"
	}

	if len(s.IgnoredPriorities) > 0 {
		st = st + " ignoredpriorities=" + util.SafeFormat(PrioritiesToString(s.IgnoredPriorities))
	}

	return st
}

func (s *SyslogInput) Start() error {
	if s.IsRunning() {
		return nil
	}

	srv := syslogsrv.NewServer()
	srv.AddHandler(newHandler(s))
	s.server = srv

	for _, socket := range s.Sockets {
		err := srv.Listen(socket)
		if err != nil {
			return fmt.Errorf("syslog socket: %s", err)
		}
		if s.CleanupSockets && strings.IndexRune(socket, ':') == -1 {
			s.cleanup_sockets = append(s.cleanup_sockets, socket)
		}
	}

	s.MarkRunning()
	s.Engine.Logger.Infof("SyslogInfo(%s): Started (i think).\n", s.Name())
	return nil
}

func (s *SyslogInput) Stop() error {
	if !s.IsRunning() {
		return nil
	}

	s.server.Shutdown()
	for _, sock := range s.cleanup_sockets {
		err := os.Remove(sock)
		if err != nil {
			s.Engine.Logger.Errorf("SyslogInput(%s): Cannot cleanup socket: %s\n", s.Name(), err)
			continue
		}
	}

	s.Engine.Logger.Infof("SyslogInfo(%s): Stopped (i think).\n", s.Name())
	return s.BaseInput.Stop()
}

//////////////////////////////////////////////////////////////////////////////

type syslogHandler struct {
	queue              chan *syslogsrv.Message
	end                chan struct{}
	ignored_priorities []int
	syslogInput        *SyslogInput
}

func newHandler(syslogInput *SyslogInput) *syslogHandler {
	h := &syslogHandler{
		queue:              make(chan *syslogsrv.Message, 5),
		end:                make(chan struct{}),
		syslogInput:        syslogInput,
		ignored_priorities: syslogInput.IgnoredPriorities,
	}

	go h.loop()
	return h
}

func (h *syslogHandler) loop() {
	for {
		msg, ok := <-h.queue
		if !ok {
			break
		}

		event := events.NewLogEvent(msg.Content)
		h.syslogInput.AnnotateAndSendEvent(event)
	}
	close(h.end)
}

func (h *syslogHandler) priorityFilter(m *syslogsrv.Message) bool {
	for i := range h.ignored_priorities {
		if m.Priority == h.ignored_priorities[i] {
			return false
		}
	}
	return true
}

func (h *syslogHandler) Handle(m *syslogsrv.Message) *syslogsrv.Message {
	if m == nil {
		close(h.queue)
		<-h.end
		return nil
	}
	if !h.priorityFilter(m) {
		return m
	}
	// Try queue m
	select {
	case h.queue <- m:
	default:
	}
	return nil
}

//////////////////////////////////////////////////////////////////////////////

var facToStr = [...]string{
	"kern",
	"user",
	"mail",
	"daemon",
	"auth",
	"syslog",
	"lpr",
	"news",
	"uucp",
	"cron",
	"authpriv",
	"system0",
	"system1",
	"system2",
	"system3",
	"system4",
	"local0",
	"local1",
	"local2",
	"local3",
	"local4",
	"local5",
	"local6",
	"local7",
}

var sevToStr = [...]string{
	"emerg",
	"alert",
	"crit",
	"err",
	"warning",
	"notice",
	"info",
	"debug",
}

func parsePriorityString(priorities string) []int {

	priors := make([]int, 0)

	parts := strings.SplitN(priorities, ".", 2)
	facstring := parts[0]
	sevstring := parts[1]

	facs := make([]int, 0)
	if facstring == "ALL" {
		for i := 0; i < len(facToStr); i++ {
			facs = append(facs, i)
		}
	} else {
		for _, fac := range strings.Split(facstring, ",") {
			for i, facname := range facToStr {
				if facname == fac {
					facs = append(facs, i)
					break
				}
			}
		}
	}

	sevs := make([]int, 0)
	if sevstring == "ALL" {
		for i := 0; i < len(sevToStr); i++ {
			sevs = append(sevs, i)
		}
	} else {
		for _, sev := range strings.Split(sevstring, ",") {
			for i, sevname := range sevToStr {
				if sevname == sev {
					sevs = append(sevs, i)
					break
				}
			}
		}
	}

	for f, _ := range facs {
		for s, _ := range sevs {
			prior := (facs[f] << 3) + sevs[s]
			priors = append(priors, prior)
		}
	}
	sort.Ints(priors)
	return priors
}

func PrioritiesToString(priorities []int) string {

	priorityToStrings := func(prior int) (facility, severity string) {
		severity = sevToStr[prior&0x07]
		facility = facToStr[prior>>3]
		return
	}

	priors := make(map[string][]string)
	for _, priority := range priorities {
		fac, sev := priorityToStrings(priority)
		facs, ok := priors[fac]
		if !ok {
			facs = make([]string, 0)
		}
		priors[fac] = append(facs, sev)
	}

	priorStrings := make([]string, 0)
	for facility, severities := range priors {
		if len(severities) == len(sevToStr) {
			priorStrings = append(priorStrings, fmt.Sprintf("%s.ALL", facility))
		} else {
			priorStrings = append(priorStrings, fmt.Sprintf("%s.%s", facility, strings.Join(severities, ",")))
		}
	}

	return strings.Join(priorStrings, ";")
}

//////////////////////////////////////////////////////////////////////////////
// THE END
