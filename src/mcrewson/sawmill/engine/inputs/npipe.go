// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package inputs

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
	"syscall"
	"time"

	"mcrewson/sawmill/engine"
	"mcrewson/sawmill/events"
	"mcrewson/sawmill/util"
)

func init() {
	engine.RegisterInput("npipe", NpipeInit)
}

//////////////////////////////////////////////////////////////////////////////

type NpipeInput struct {
	engine.BaseInput
	Path string

	opened     bool
	file       *os.File
	reader     *bufio.Reader
	stopSignal chan bool
}

func NpipeInit(args map[string]string, eng *engine.Engine) (engine.InputChainable, error) {
	val, ok := args["path"]
	if !ok {
		return nil, fmt.Errorf("no path defined for this file input")
	}

	chi := &NpipeInput{Path: val}
	chi.InitBaseInput(args, eng)
	chi.stopSignal = make(chan bool, 0)
	return chi, nil
}

func (n *NpipeInput) String() string {
	return fmt.Sprintf("name=%s type=npipe path=%s", util.SafeFormat(n.Name()), util.SafeFormat(n.Path))
}

func (n *NpipeInput) Start() error {
	if n.IsRunning() {
		return nil
	}
	go n.ReadPipe()
	n.MarkRunning()
	return nil
}

func (n *NpipeInput) Stop() error {
	if !n.IsRunning() {
		return nil
	}
	n.stopSignal <- true
	return n.BaseInput.Stop()
}

func (n *NpipeInput) ReadPipe() {

	// TODO: make the buffer size tunable
	var read_timeout = 1 * time.Second

	n.Engine.Logger.Infof("NpipeInput(%s): Started.\n", n.Name())
readpipe_runloop:
	for {
		select {
		case <-n.stopSignal:
			break readpipe_runloop
		default:
			if !n.opened {
				n.open()
			}

			text, err := n.readline(read_timeout)

			if err != nil {
				if err == io.EOF {
					if text == "" {
						// no new events since since read_timeout, close the pipe and sleep for a bit
						n.close()
						time.Sleep(2 * time.Second)
						continue readpipe_runloop
					}
				} else {
					e, ok := err.(*os.PathError)
					if ok && (e.Err == syscall.EAGAIN || e.Err == syscall.EWOULDBLOCK || e.Err == syscall.ETIMEDOUT) {
						continue readpipe_runloop
					}

					n.Engine.Logger.Errorf("NpipeInput(%s): unexpected error (%d): %s\n", n.Name(), e.Err, err)
					n.close()
					time.Sleep(500 * time.Millisecond)
					continue readpipe_runloop
				}
			}

			event := events.NewLogEvent(strings.TrimSpace(text))
			n.AnnotateAndSendEvent(event)
		}
	}
	n.Engine.Logger.Infof("NpipeInput(%s): Stopped.\n", n.Name())
}

func (n *NpipeInput) open() {

	for {
		var err error
		n.file, err = os.OpenFile(n.Path, os.O_RDONLY|syscall.O_NONBLOCK, 0400)
		if err == nil {
			break
		}

		// on failure, sleep for a few seconds and then retry
		time.Sleep(500 * time.Millisecond)
	}

	n.reader = bufio.NewReaderSize(n.file, 16<<10) // 16kb buffer by default
	n.opened = true
}

func (n *NpipeInput) close() {
	if !n.opened {
		return
	}

	n.file.Close()
	n.reader = nil
	n.opened = false
}

func (n *NpipeInput) readline(eof_timeout time.Duration) (line string, err error) {
	var frag []byte
	var full [][]byte
	err = nil

	start_time := time.Now()
	for {
		var e error
		frag, e = n.reader.ReadSlice('\n')
		if e == nil {
			break
		} else if e == io.EOF {
			if len(frag) == 0 {
				time.Sleep(200 * time.Millisecond) // TODO: implement backoff

				// give up waiting for data after a certain amount of time
				// if we timeout, return the error (eof)
				if time.Since(start_time) > eof_timeout {
					if len(full) > 0 {
						break
					}
					return "", e
				}
				continue
			}
		} else if e != bufio.ErrBufferFull {
			err = e
			break
		}

		// make a copy of the buffer
		buf := make([]byte, len(frag))
		copy(buf, frag)
		full = append(full, buf)
	}

	sz := 0
	for i := range full {
		sz += len(full[i])
	}
	sz += len(frag)

	buf := make([]byte, sz)
	sz = 0
	for i := range full {
		sz += copy(buf[sz:], full[i])
	}
	copy(buf[sz:], frag)
	return string(buf), err
}

//////////////////////////////////////////////////////////////////////////////
// THE END
