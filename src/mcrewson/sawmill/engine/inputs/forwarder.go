// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package inputs

import (
	"mcrewson/sawmill/engine"
	"mcrewson/sawmill/events"
	"mcrewson/sawmill/util/clustersrv"
	"fmt"
	"strconv"
)

func init() {
	engine.RegisterInput("forwarder", ForwarderInit)
}

//////////////////////////////////////////////////////////////////////////////

type ForwarderInput struct {
	engine.BaseInput
	codec      events.EventCodec
	receiver   clustersrv.ClusterReceiver
	channel    chan []byte
	stopSignal chan bool
}

func ForwarderInit(args map[string]string, eng *engine.Engine) (engine.InputChainable, error) {
	max_connections := uint32(4096)
	val, ok := args["maxconnections"]
	if ok {
		i, err := strconv.ParseUint(val, 10, 32)
		if err != nil {
			return nil, fmt.Errorf("maxconnections must be a positive integer")
		}
		max_connections = uint32(i)
	}

	channel := make(chan []byte, max_connections)
	receiver, err := clustersrv.NewClusterReceiver(args, channel, eng.Logger)
	if err != nil {
		return nil, err
	}

	chi := &ForwarderInput{
		codec:      events.NewProtobufEventCodec(false),
		receiver:   receiver,
		channel:    channel,
		stopSignal: make(chan bool),
	}

	if err := chi.InitBaseInput(args, eng); err != nil {
		return nil, err
	}
	return chi, nil
}

func (f *ForwarderInput) String() string {
	return ""
}

func (f *ForwarderInput) Start() error {
	if f.IsRunning() {
		return nil
	}

	go func() {
		f.Engine.Logger.Infof("ForwarderInput(%s): Started.\n", f.Name())
	runloop:
		for {
			select {
			case payload := <-f.channel:
				if len(payload) > 0 {
					frame, _ := f.codec.DecodeFrame(payload)
					for i := uint32(0); i < frame.Count; i++ {
						f.AnnotateAndSendEvent(frame.Events[i])
					}
				}

			case <-f.stopSignal:
				break runloop
			}
		}
		f.Engine.Logger.Infof("ForwarderInput(%s): Stopped.\n", f.Name())
	}()

	if err := f.receiver.Start(); err != nil {
		return err
	}

	f.MarkRunning()
	return nil
}

func (f *ForwarderInput) Stop() error {
	if !f.IsRunning() {
		return nil
	}
	f.MarkNotRunning()

	if err := f.receiver.Stop(); err != nil {
		return err
	}

	f.stopSignal <- true
	return f.BaseInput.Stop()
}

//////////////////////////////////////////////////////////////////////////////
// THE END
