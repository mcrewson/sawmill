// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package inputs

import (
	"fmt"
	"math/rand"
	"net"
	"time"

	"github.com/garyburd/redigo/redis"

	"mcrewson/sawmill/engine"
	"mcrewson/sawmill/events"
	"mcrewson/sawmill/util"
)

func init() {
	engine.RegisterInput("redis", RedisInit)
}

//////////////////////////////////////////////////////////////////////////////

type RedisInput struct {
	engine.BaseInput

	Server   string
	List     string
	Timeouts map[string]time.Duration

	codec      events.EventCodec
	conn       redis.Conn
	stopSignal chan bool
}

var default_timeouts = map[string]time.Duration{
	"connecttimeout": time.Duration(2) * time.Second,
	"readtimeout":    time.Duration(2) * time.Second,
	"writetimeout":   time.Duration(2) * time.Second,
}

func RedisInit(args map[string]string, eng *engine.Engine) (engine.InputChainable, error) {
	val, ok := args["server"]
	if !ok {
		return nil, fmt.Errorf("no server specified for this redis input")
	}
	server := val

	val, ok = args["list"]
	if !ok {
		return nil, fmt.Errorf("no list specifed for this redis input")
	}
	list := val

	timeouts, err := util.ParseNetworkTimeouts(args, default_timeouts)
	if err != nil {
		return nil, err
	}

	chi := &RedisInput{
		Server:     server,
		List:       list,
		Timeouts:   timeouts,
		codec:      events.NewProtobufEventCodec(true),
		stopSignal: make(chan bool),
	}
	chi.InitBaseInput(args, eng)
	return chi, nil
}

func (r *RedisInput) String() string {
	s := fmt.Sprintf("name=%s type=redis server=%s list=%s", util.SafeFormat(r.Name()), util.SafeFormat(r.Server), util.SafeFormat(r.List))

	if r.Timeouts["connect"] != default_timeouts["connecttimeout"] {
		s = s + " connecttimeout=" + util.DurationToString(r.Timeouts["connect"])
	}
	if r.Timeouts["read"] != default_timeouts["readtimeout"] {
		s = s + " readtimeout=" + util.DurationToString(r.Timeouts["read"])
	}
	if r.Timeouts["write"] != default_timeouts["writetimeout"] {
		s = s + " writetimeout=" + util.DurationToString(r.Timeouts["write"])
	}

	return s
}

func (r *RedisInput) Start() error {
	if r.IsRunning() {
		return nil
	}

	loopfunc := func() {
		r.Engine.Logger.Infof("RedisInput(%s): Started.\n", r.Name())
		r.connect()
	runloop:
		for {
			select {
			case <-r.stopSignal:
				break runloop
			default:
				payload, err := r.conn.Do("BLPOP", r.List, 1)
				if err != nil {
					r.Engine.Logger.Errorf("RedisInput(%s): Redis error, will reconnect: %s\n", r.Name(), err)
					r.reconnect()
					continue runloop
				}

				if payload != nil {
					frame, _ := r.codec.DecodeFrame(payload.([]interface{})[1].([]byte))
					for i := uint32(0); i < frame.Count; i++ {
						r.AnnotateAndSendEvent(frame.Events[i])
					}
				}
			}
		}
		r.Engine.Logger.Infof("RedisInput(%s): Stopped.\n", r.Name())
	}

	go loopfunc()
	r.MarkRunning()
	return nil
}

func (r *RedisInput) Stop() error {
	if !r.IsRunning() {
		return nil
	}
	r.MarkNotRunning()
	r.stopSignal <- true
	if r.conn != nil {
		r.conn.Close()
	}
	return r.BaseInput.Stop()
}

func (r *RedisInput) connect() {

	for {
		if !r.IsRunning() {
			// stop attempting to connect if no longer running
			return
		}
		host, port, err := net.SplitHostPort(r.Server)
		addresses, err := net.LookupHost(host)

		if err != nil {
			r.Engine.Logger.Errorf("RedisInput(%s): DNS lookup failure \"%s\": %s\n", r.Name(), host, err)
			time.Sleep(1 * time.Second)
			continue
		}

		address := net.JoinHostPort(addresses[rand.Int()%len(addresses)], port)
		r.Engine.Logger.Infof("RedisInput(%s): Connecting to REDIS @ %s (%s)\n", r.Name(), address, host)

		connTimeout := r.Timeouts["connect"]
		writeTimeout := r.Timeouts["write"]
		r.conn, err = redis.DialTimeout("tcp", address, connTimeout, 0, writeTimeout)
		if err != nil {
			r.Engine.Logger.Errorf("RedisInput(%s): Failed to connect with %s: %s\n", r.Name(), address, err)
			time.Sleep(1 * time.Second)
			continue
		}

		r.Engine.Logger.Infof("RedisInput(%s): Successfully connected to REDIS @ %s\n", r.Name(), address)
		return
	}
}

func (r *RedisInput) reconnect() {
	r.conn.Close()
	r.connect()
}

//////////////////////////////////////////////////////////////////////////////
// THE END
