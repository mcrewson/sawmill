// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package inputs

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"

	"mcrewson/sawmill/engine"
	"mcrewson/sawmill/events"
	"mcrewson/sawmill/util"
)

func init() {
	engine.RegisterInput("stdin", StdinInit)
}

//////////////////////////////////////////////////////////////////////////////

type StdinInput struct {
	engine.BaseInput

	shutdownOnError bool
	stopSignal      chan bool
	reader          *bufio.Reader
}

var _default_stdin_shutdownoneof bool = true

func StdinInit(args map[string]string, eng *engine.Engine) (engine.InputChainable, error) {
	shutdown_on_eof := _default_stdin_shutdownoneof
	val, ok := args["shutdownonerror"]
	if !ok {
		val, ok = args["shutdownoneof"]
	}
	if ok {
		if bval, berr := strconv.ParseBool(val); berr != nil {
			return nil, fmt.Errorf("shutdownoneof parameter much has a boolean value")
		} else {
			shutdown_on_eof = bval
		}
	}

	chi := &StdinInput{
		shutdownOnError: shutdown_on_eof,
		stopSignal:      make(chan bool, 0),
		reader:          bufio.NewReader(os.Stdin),
	}
	chi.InitBaseInput(args, eng)
	return chi, nil
}

func (s *StdinInput) String() string {
	return fmt.Sprintf("name=%s type=stdin shutdownonerror=%s", util.SafeFormat(s.Name()), strconv.FormatBool(s.shutdownOnError))
}

func (s *StdinInput) Start() error {
	if s.IsRunning() {
		return nil
	}
	go s.ReadStdin()
	s.MarkRunning()
	return nil
}

func (s *StdinInput) Stop() error {
	if !s.IsRunning() {
		return nil
	}
	s.stopSignal <- true
	return s.BaseInput.Stop()
}

func (s *StdinInput) ReadStdin() {
	s.Engine.Logger.Infof("StdinInput(%s): Started.\n", s.Name())
	goterror := false
readstdin_loop:
	for {
		select {
		case <-s.stopSignal:
			break readstdin_loop
		default:
			if goterror == false {
				text, err := s.reader.ReadString('\n')
				if err != nil {
					goterror = true
					if err == io.EOF {
						s.Engine.Logger.Debugf("StdinInput(%s): EOF\n", s.Name())
						if text != "" {
							event := events.NewLogEvent(strings.TrimSpace(text))
							s.AnnotateAndSendEvent(event)
						}

					} else {
						// unexpected error!
						s.Engine.Logger.Errorf("StdinInput(%s): unexpected error: %s\n", s.Name(), err)
					}

					if s.shutdownOnError && s.IsRunning() {
						go s.Engine.Shutdown()
					} else {
						s.Engine.Logger.Warningf("StdinInput(%s): no longer reading from stdin.\n", s.Name())
					}
				} else {
					event := events.NewLogEvent(strings.TrimSpace(text))
					s.AnnotateAndSendEvent(event)
				}
			}
		}
	}
	s.Engine.Logger.Infof("StdinInput(%s): Stopped.\n", s.Name())
}

//////////////////////////////////////////////////////////////////////////////
// THE END
