// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package inputs

import (
	"fmt"
	"os"
	"strconv"
	"strings"
	"syscall"
	"time"

	"mcrewson/sawmill/engine"
	"mcrewson/sawmill/events"
	"mcrewson/sawmill/util"
)

func init() {
	engine.RegisterInput("kmsg", KmsgInit)
}

//////////////////////////////////////////////////////////////////////////////

type KmsgInput struct {
	engine.BaseInput

	Path      string
	FromStart bool

	opened     bool
	file       *os.File
	stopSignal chan bool
}

var default_path = "/dev/kmsg"
var default_fromstart = "false"

func KmsgInit(args map[string]string, eng *engine.Engine) (engine.InputChainable, error) {
	val, ok := args["path"]
	if !ok {
		val = default_path
	}
	path := val

	val, ok = args["fromstart"]
	if !ok {
		val = default_fromstart
	}
	fromstart, err := strconv.ParseBool(val)
	if err != nil {
		return nil, fmt.Errorf("fromstart parameter must have a boolean value")
	}

	chi := &KmsgInput{
		Path:      path,
		FromStart: fromstart,
	}
	chi.InitBaseInput(args, eng)
	chi.stopSignal = make(chan bool, 0)
	return chi, nil
}

func (k *KmsgInput) String() string {
	s := fmt.Sprintf("name=%s type=kmsg", util.SafeFormat(k.Name()))
	if k.Path != default_path {
		s = s + " path=" + util.SafeFormat(k.Path)
	}
	return s
}

func (k *KmsgInput) Start() error {
	if k.IsRunning() {
		return nil
	}
	go k.ReadKmsg()
	k.MarkRunning()
	return nil
}

func (k *KmsgInput) Stop() error {
	if !k.IsRunning() {
		return nil
	}
	k.MarkNotRunning()
	k.stopSignal <- true
	return k.BaseInput.Stop()
}

func (k *KmsgInput) ReadKmsg() {
	buffer := make([]string, 0)
	sendbuffer := func() {
		event := events.NewLogEvent(strings.TrimSpace(strings.Join(buffer, "")))
		k.AnnotateAndSendEvent(event)
		buffer = make([]string, 0)
	}

	k.Engine.Logger.Infof("KmsgInput(%s): Started.\n", k.Name())
readkmsg_loop:
	for {
		select {
		case <-k.stopSignal:
			break readkmsg_loop
		default:
			if !k.opened {
				k.open()
			}

			text, err := k.readrecord()
			if err != nil {
				e, ok := err.(*os.PathError)
				if ok && (e.Err == syscall.EAGAIN || e.Err == syscall.EWOULDBLOCK || e.Err == syscall.ETIMEDOUT) {
					// no data...
					if len(buffer) > 0 {
						sendbuffer()
					}
					time.Sleep(100 * time.Millisecond)
					continue readkmsg_loop
				} else {
					// unexpected error!
					k.Engine.Logger.Errorf("KmsgInput(%s): unexpected error (%d): %s\n", k.Name(), e.Err, err)
					k.close()
					time.Sleep(500 * time.Millisecond)
					continue readkmsg_loop
				}
			}

			if len(buffer) == 0 || text[0] == ' ' {
				buffer = append(buffer, text)
			} else if len(buffer) > 0 {
				sendbuffer()
				buffer = append(buffer, text)
			}
		}
	}
	k.Engine.Logger.Infof("KmsgInput(%s): Stopped.\n", k.Name())
}

func (k *KmsgInput) open() {
	for {
		if !k.IsRunning() {
			// stop attempting to open if no longer running
			return
		}
		var err error
		k.file, err = os.OpenFile(k.Path, os.O_RDONLY|syscall.O_NONBLOCK, 0400)
		if err == nil {
			break
		}

		// on failure, sleep for a bit and then retry
		time.Sleep(500 * time.Millisecond)
	}

	if k.FromStart {
		k.file.Seek(0, os.SEEK_SET)
	} else {
		k.file.Seek(0, os.SEEK_END)
	}

	k.opened = true
}

func (k *KmsgInput) close() {
	if !k.opened {
		return
	}

	k.file.Close()
	k.opened = false
}

func (k *KmsgInput) readrecord() (string, error) {
	frag := make([]byte, 8*1024)

	n, e := k.file.Read(frag)
	if n > 0 {
		buf := make([]byte, n)
		copy(buf, frag)
		return string(buf), e
	}

	return "", e
}

//////////////////////////////////////////////////////////////////////////////
// THE END
