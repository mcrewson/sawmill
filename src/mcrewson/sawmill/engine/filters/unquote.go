// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package filters

import (
	"mcrewson/sawmill/engine"
	"mcrewson/sawmill/events"
	"mcrewson/sawmill/util"
	"fmt"
	"regexp"
	"strconv"
)

func init() {
	engine.RegisterFilter("unquote", UnquoteInit)
}

//////////////////////////////////////////////////////////////////////////////

type unquote_function func(string) (string, error)

type UnquoteFilter struct {
	engine.BaseFilter

	inField      string
	unquoteFuncs []unquote_function
}

var _default_unquote_infield = "message"

func UnquoteInit(args map[string]string, eng *engine.Engine) (engine.FilterChainable, error) {
	val, ok := args["infield"]
	if !ok {
		val = _default_unquote_infield
	}
	inField := val

	unquoteFuncs := make([]unquote_function, 0)

	val, ok = args["rsyslogescapes"]
	if ok {
		v, err := strconv.ParseBool(val)
		if err != nil {
			return nil, fmt.Errorf("Invalid value for 'rsyslogescapes' parameter")
		}
		if v {
			unquoteFuncs = append(unquoteFuncs, unquote_rsyslog_escapes)
		}
	}

	chf := &UnquoteFilter{
		inField:      inField,
		unquoteFuncs: unquoteFuncs,
	}
	chf.InitBaseFilter(args, eng, chf.UnquoteFilterFunc)
	return chf, nil
}

func (u *UnquoteFilter) String() string {
	s := fmt.Sprintf("name=%s type=unquote", u.Name())
	if u.inField != _default_unquote_infield {
		s = s + " infield=%s" + util.SafeFormat(u.inField)
	}

	return s
}

func (u *UnquoteFilter) UnquoteFilterFunc(ev *events.LogEvent) *events.LogEvent {
	input, exists := ev.Fields[u.inField]
	if !exists {
		return ev
	}

	for _, unquoteFunc := range u.unquoteFuncs {
		result, err := unquoteFunc(input)
		if err != nil {
			u.Engine.Logger.Debugf("UnquoteFilter(%s): error when unquoting field(%s): %v\n", u.Name(), u.inField, err)
		} else {
			input = result
		}
	}

	ev.Fields[u.inField] = input
	return ev
}

//////////////////////////////////////////////////////////////////////////////

var unquote_rsyslog_regexp = regexp.MustCompile(`#0\d{2}`)

func unquote_rsyslog_escapes(input string) (string, error) {
	val := unquote_rsyslog_regexp.ReplaceAllStringFunc(input, func(in string) string {
		i, _ := strconv.ParseInt(in[1:], 8, 8)
		return string(i)
	})
	return val, nil
}

//////////////////////////////////////////////////////////////////////////////
// THE END
