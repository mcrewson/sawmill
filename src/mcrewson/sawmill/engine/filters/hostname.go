// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package filters

import (
	"mcrewson/sawmill/engine"
	"mcrewson/sawmill/events"
	"mcrewson/sawmill/util"
	"fmt"
	"os"
	"strings"
	"time"
)

func init() {
	engine.RegisterFilter("hostname", HostnameInit)
}

//////////////////////////////////////////////////////////////////////////////

type HostnameFilter struct {
	engine.BaseFilter
	field  string
	format string
	poll   time.Duration

	currentHostname string
	nextPoll        time.Time
}

var _default_hostname_field = "hostname"
var _default_hostname_format = "fqdn"
var _default_hostname_poll = time.Duration(4) * time.Hour

func HostnameInit(args map[string]string, eng *engine.Engine) (engine.FilterChainable, error) {
	val, ok := args["field"]
	if !ok {
		val = _default_hostname_field
	}
	field := val

	val, ok = args["format"]
	if !ok {
		val = _default_hostname_format
	}
	format := strings.ToLower(val)
	if format != "short" && format != "long" && format != "fqdn" {
		return nil, fmt.Errorf("Invalid format: %s\n", format)
	}

	poll := _default_hostname_poll
	val, ok = args["poll"]
	if ok {
		duration, err := util.ParseDuration(val)
		if err != nil {
			return nil, fmt.Errorf("Invalid poll: %s\n", val)
		}
		poll = duration
	}

	chf := &HostnameFilter{
		field:    field,
		format:   format,
		poll:     poll,
		nextPoll: time.Now().Add(poll),
	}
	chf.InitBaseFilter(args, eng, chf.HostnameFilterFunc)
	return chf, nil
}

func (hf *HostnameFilter) String() string {
	return fmt.Sprintf("name=%s type=hostname", util.SafeFormat(hf.Name()))
}

func (hf *HostnameFilter) HostnameFilterFunc(ev *events.LogEvent) *events.LogEvent {
	if hf.currentHostname == "" || time.Now().After(hf.nextPoll) {
		if err := hf.discoverHostname(); err != nil {
			return ev
		}
	}

	ev.Fields[hf.field] = hf.currentHostname
	return ev
}

func (hf *HostnameFilter) discoverHostname() error {
	hostname, err := os.Hostname()
	if err != nil {
		return err
	}

	if hf.format == "short" {
		hostname = strings.Split(hostname, ".")[0]
	}

	hf.currentHostname = hostname
	hf.nextPoll = time.Now().Add(hf.poll)
	return nil
}

//////////////////////////////////////////////////////////////////////////////
// THE END
