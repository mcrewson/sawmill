// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package filters

import (
	"mcrewson/sawmill/engine"
	"mcrewson/sawmill/events"
	"mcrewson/sawmill/util"
	"mcrewson/sawmill/util/grok"
	"fmt"
	"strings"
)

func init() {
	engine.RegisterFilter("grok", GrokInit)
}

//////////////////////////////////////////////////////////////////////////////

type GrokFilter struct {
	engine.BaseFilter

	Field          string
	Match          string
	GrokPatternDir string

	pattern *grok.Grok
}

var _default_field = "message"
var _default_grokpatterndir = "/etc/grok"

func GrokInit(args map[string]string, eng *engine.Engine) (engine.FilterChainable, error) {
	val, ok := args["field"]
	if !ok {
		val = _default_field
	}
	field := val

	val, ok = args["match"]
	if !ok {
		return nil, fmt.Errorf("grok filter requires a 'match' argument")
	}
	match := val

	val, ok = args["grokpatterndir"]
	if !ok {
		val, ok = eng.GetVariable("grokpatterndir")
		if !ok {
			val = _default_grokpatterndir
		}
	}
	grokpatterndir := val

	pattern := grok.NewGrok()

	err := pattern.AddPatternsFromDirectory(grokpatterndir)
	if err != nil {
		return nil, err
	}

	err = pattern.Compile(match)
	if err != nil {
		return nil, err
	}

	chf := &GrokFilter{
		Field:          field,
		Match:          match,
		GrokPatternDir: grokpatterndir,
		pattern:        pattern,
	}
	chf.InitBaseFilter(args, eng, chf.GrokFilterFunc)
	eng.Logger.Debugf("GrokFilter(%s): regexp = %s\n", chf.Name(), pattern.ExpandedPattern)
	return chf, nil
}

func (g *GrokFilter) String() string {
	s := fmt.Sprintf("name=%s type=grok", g.Name())
	if g.Field != _default_field {
		s = s + " field=" + util.SafeFormat(g.Field)
	}
	s = s + " match=" + util.SafeFormat(g.Match)
	if g.GrokPatternDir != _default_grokpatterndir {
		s = s + " grokpatterndir=" + util.SafeFormat(g.GrokPatternDir)
	}
	return s
}

func (g *GrokFilter) GrokFilterFunc(ev *events.LogEvent) *events.LogEvent {
	input, exists := ev.Fields[g.Field]
	if !exists {
		return ev
	}

	match := g.pattern.Match(input)
	if match == nil {
		return ev
	}

	match.ForEachCapture(g._handle_match, *ev)
	return ev
}

func (g *GrokFilter) _handle_match(capture string, value string, data interface{}) {
	if value == "" {
		return
	}
	parts := strings.SplitN(capture, ":", 3)
	if len(parts) < 2 {
		return
	}

	ev := data.(events.LogEvent)
	ev.Fields[parts[1]] = value
}

//////////////////////////////////////////////////////////////////////////////
// THE END
