// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package filters

import (
	"mcrewson/sawmill/engine"
	"mcrewson/sawmill/events"
	"mcrewson/sawmill/util"
	"fmt"
)

func init() {
	engine.RegisterFilter("copy", CopyInit)
}

//////////////////////////////////////////////////////////////////////////////

type CopyFilter struct {
	engine.BaseFilter
}

func CopyInit(args map[string]string, eng *engine.Engine) (engine.FilterChainable, error) {
	chf := &CopyFilter{}
	chf.InitBaseFilter(args, eng, chf.CopyFilterFunc)
	return chf, nil
}

func (d *CopyFilter) String() string {
	return fmt.Sprintf("name=%s type=copy", util.SafeFormat(d.Name()))
}

func (d *CopyFilter) CopyFilterFunc(ev *events.LogEvent) *events.LogEvent {
	return ev.Copy()
}

//////////////////////////////////////////////////////////////////////////////
// THE END
