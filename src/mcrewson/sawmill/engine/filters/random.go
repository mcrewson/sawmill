// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package filters

import (
	"mcrewson/sawmill/engine"
	"mcrewson/sawmill/events"
	"mcrewson/sawmill/util"
	"fmt"
	"math/rand"
)

func init() {
	engine.RegisterFilter("random", RandomInit)
}

//////////////////////////////////////////////////////////////////////////////

type RandomFilter struct {
	engine.BaseFilter
	Field string
}

func RandomInit(args map[string]string, eng *engine.Engine) (engine.FilterChainable, error) {
	val, ok := args["field"]
	if !ok {
		return nil, fmt.Errorf("random filter requires a 'field' argument")
	}
	field := val

	chf := &RandomFilter{
		Field: field,
	}
	chf.InitBaseFilter(args, eng, chf.RandomFilterFunc)
	return chf, nil
}

func (r *RandomFilter) String() string {
	return fmt.Sprintf("name=%s type=random field=%s", util.SafeFormat(r.Name()), util.SafeFormat(r.Field))
}

func (r *RandomFilter) RandomFilterFunc(ev *events.LogEvent) *events.LogEvent {
	_, exists := ev.Fields[r.Field]
	if exists {
		r.Engine.Logger.Debugf("RandomFilter(%s): event already has this field. not overwriting it: %s\n", r.Name, r.Field)
		return ev
	}

	ev.Fields[r.Field] = randomString()
	return ev
}

func randomString() string {
	const chars = "abcdefghijklmnopqrstuvwxyz1234567890"
	const sz = 10
	buf := make([]byte, sz)
	for i := 0; i < sz; i++ {
		buf[i] = chars[rand.Intn(len(chars)-1)]
	}
	return string(buf)
}

//////////////////////////////////////////////////////////////////////////////
// THE END
