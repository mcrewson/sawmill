// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package filters

import (
	"mcrewson/sawmill/engine"
	"mcrewson/sawmill/events"
	"mcrewson/sawmill/util"
	"fmt"
	"sync"
	"time"
)

func init() {
	engine.RegisterFilter("debug", DebugInit)
}

//////////////////////////////////////////////////////////////////////////////

type DebugFilter struct {
	engine.BaseFilter

	writeLock sync.Mutex
}

func DebugInit(args map[string]string, eng *engine.Engine) (engine.FilterChainable, error) {
	chf := &DebugFilter{}
	chf.InitBaseFilter(args, eng, chf.DebugFilterFunc)
	return chf, nil
}

func (d *DebugFilter) String() string {
	return fmt.Sprintf("name=%s type=debug", util.SafeFormat(d.Name()))
}

func (d *DebugFilter) DebugFilterFunc(ev *events.LogEvent) *events.LogEvent {
	d.writeLock.Lock()
	defer d.writeLock.Unlock()
	d.Engine.Logger.Debugf("DebugFilter(%s): TIMESTAMP = %s\n", d.Name(), ev.Timestamp.UTC().Format(time.RFC3339Nano))
	for k, v := range ev.Fields {
		d.Engine.Logger.Debugf("DebugFilter: FIELD: %s = \"%s\"\n", k, v)
	}
	return ev
}

//////////////////////////////////////////////////////////////////////////////
// THE END
