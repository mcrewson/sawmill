// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package filters

import (
	"mcrewson/sawmill/engine"
	"mcrewson/sawmill/events"
	"mcrewson/sawmill/util"
	"mcrewson/sawmill/util/grok"
	"fmt"
	"strconv"
)

func init() {
	engine.RegisterFilter("multiline", MultilineInit)
}

//////////////////////////////////////////////////////////////////////////////

type MultilineFilter struct {
	engine.BaseFilter

	Field          string
	Match          string
	Negate         bool
	What           string
	GrokPatternDir string

	pattern *grok.Grok
	pending *events.LogEvent
}

var _default_multiline_field = "message"
var _default_multiline_negate = false
var _default_multiline_grokpatterndir = "/etc/grok"

func MultilineInit(args map[string]string, eng *engine.Engine) (engine.FilterChainable, error) {
	val, ok := args["field"]
	if !ok {
		val = _default_multiline_field
	}
	field := val

	val, ok = args["match"]
	if !ok {
		return nil, fmt.Errorf("multiline filter requires a 'match' argument")
	}
	match := val

	negate := _default_multiline_negate
	val, ok = args["negate"]
	if ok {
		n, err := strconv.ParseBool(val)
		if err != nil {
			return nil, err
		}
		negate = n
	}

	val, ok = args["what"]
	if !ok {
		return nil, fmt.Errorf("multiline filter requires a 'what' argument")
	}
	if val != "previous" && val != "next" {
		return nil, fmt.Errorf("invalid value for 'what' parameter")
	}
	what := val

	val, ok = args["grokpatterndir"]
	if !ok {
		val, ok = eng.GetVariable("grokpatterndir")
		if !ok {
			val = _default_multiline_grokpatterndir
		}
	}
	grokpatterndir := val

	pattern := grok.NewGrok()
	err := pattern.AddPatternsFromDirectory(grokpatterndir)
	if err != nil {
		return nil, err
	}

	err = pattern.Compile(match)
	if err != nil {
		return nil, err
	}

	chf := &MultilineFilter{
		Field:          field,
		Match:          match,
		Negate:         negate,
		What:           what,
		GrokPatternDir: grokpatterndir,
		pattern:        pattern,
	}
	chf.InitBaseFilter(args, eng, chf.MultilineFilterFunc)
	return chf, nil
}

func (m *MultilineFilter) String() string {
	s := fmt.Sprintf("name=%s type=multiline", m.Name())
	if m.Field != _default_multiline_field {
		s = s + " field=" + util.SafeFormat(m.Field)
	}
	s = s + " match=" + util.SafeFormat(m.Match)
	if m.Negate != _default_multiline_negate {
		s = s + " negate=" + util.SafeFormat(strconv.FormatBool(m.Negate))
	}
	s = s + " what=" + util.SafeFormat(m.What)
	if m.GrokPatternDir != _default_multiline_grokpatterndir {
		s = s + " grokpatterndir=" + util.SafeFormat(m.GrokPatternDir)
	}
	return s
}

func (m *MultilineFilter) MultilineFilterFunc(ev *events.LogEvent) *events.LogEvent {
	input, exists := ev.Fields[m.Field]
	if !exists {
		return ev
	}

	match := m.pattern.Match(input)
	matched := (match != nil && m.Negate == false) || (match == nil && m.Negate == true)

	switch m.What {
	case "previous":
		if matched {
			if m.pending == nil {
				m.pending = ev
			} else {
				m.pending.Merge(ev)
			}
			return nil
		} else {
			// this event is not part of the previous event
			// if we have a pending event, it's done, send it.
			// put the current event into pending
			if m.pending != nil {
				prevEvent := m.pending
				m.pending = ev
				return prevEvent
			} else {
				m.pending = ev
				return nil
			}
		}

	case "next":
		if matched {
			if m.pending == nil {
				m.pending = ev
			} else {
				m.pending.Merge(ev)
			}
			return nil
		} else {
			// if we have something in pending, join it with this message
			// and send it. otherwise, this is a new message and not part
			// of multiline, send it.
			if m.pending != nil {
				m.pending.Merge(ev)
				nextEvent := m.pending
				m.pending = nil
				return nextEvent
			}
		}
	}
	return ev
}

//////////////////////////////////////////////////////////////////////////////
// THE END
