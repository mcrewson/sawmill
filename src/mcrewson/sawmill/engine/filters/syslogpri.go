// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package filters

import (
	"mcrewson/sawmill/engine"
	"mcrewson/sawmill/events"
	"mcrewson/sawmill/util"
	"fmt"
	"strconv"
)

func init() {
	engine.RegisterFilter("syslogpri", SyslogpriInit)
}

//////////////////////////////////////////////////////////////////////////////

type SyslogpriFilter struct {
	engine.BaseFilter

	Field  string
	Labels bool
}

var _default_syslogpri_field = "syslog_priority"
var _default_syslogpri_labels = true

func SyslogpriInit(args map[string]string, eng *engine.Engine) (engine.FilterChainable, error) {
	val, ok := args["field"]
	if !ok {
		val = _default_syslogpri_field
	}
	field := val

	labels := _default_syslogpri_labels
	val, ok = args["labels"]
	if ok {
		l, err := strconv.ParseBool(val)
		if err != nil {
			return nil, fmt.Errorf("labels parameter must have a boolean value")
		}
		labels = l
	}

	chf := &SyslogpriFilter{
		Field:  field,
		Labels: labels,
	}
	chf.InitBaseFilter(args, eng, chf.SyslogpriFilterFunc)
	return chf, nil
}

func (d *SyslogpriFilter) String() string {
	s := fmt.Sprintf("name=%s type=syslogpri", d.Name())
	if d.Field != _default_syslogpri_field {
		s = s + " field=%s" + util.SafeFormat(d.Field)
	}
	if d.Labels != _default_syslogpri_labels {
		s = s + " labels=" + util.SafeFormat(strconv.FormatBool(d.Labels))
	}

	return s
}

func (d *SyslogpriFilter) SyslogpriFilterFunc(ev *events.LogEvent) *events.LogEvent {
	input, exists := ev.Fields[d.Field]
	if !exists {
		return ev
	}

	priority, err := strconv.Atoi(input)
	if err != nil {
		d.Engine.Logger.Warningf("SyslogpriFilter(%s): invalid value for priority: \"%s\"\n", d.Name(), input)
		return ev
	}

	// Per RFC3164, priority = (facility * 8) + severity
	// = (facility << 3) & (severity)

	severity := priority & 0x07
	facility := priority >> 3

	ev.Fields["syslog_severity_code"] = strconv.Itoa(severity)
	ev.Fields["syslog_facility_code"] = strconv.Itoa(facility)

	if d.Labels {
		ev.Fields["syslog_facility"] = facility_labels[facility]
		ev.Fields["syslog_severity"] = severity_labels[severity]
	}

	return ev
}

//////////////////////////////////////////////////////////////////////////////

var facility_labels = [...]string{
	"kernel",
	"user",
	"mail",
	"daemon",
	"auth",
	"syslog",
	"lpr",
	"news",
	"uucp",
	"cron",
	"authpriv",
	"system0",
	"system1",
	"system2",
	"system3",
	"system4",
	"local0",
	"local1",
	"local2",
	"local3",
	"local4",
	"local5",
	"local6",
	"local7",
}

var severity_labels = [...]string{
	"emerg",
	"alert",
	"crit",
	"err",
	"warning",
	"notice",
	"info",
	"debug",
}

//////////////////////////////////////////////////////////////////////////////
// THE END
