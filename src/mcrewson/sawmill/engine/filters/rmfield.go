// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package filters

import (
	"mcrewson/sawmill/engine"
	"mcrewson/sawmill/events"
	"mcrewson/sawmill/util"
	"fmt"
)

func init() {
	engine.RegisterFilter("rmfield", RmfieldInit)
}

//////////////////////////////////////////////////////////////////////////////

type RmfieldFilter struct {
	engine.BaseFilter
	Fields []string
}

func RmfieldInit(args map[string]string, eng *engine.Engine) (engine.FilterChainable, error) {
	val, ok := args["fields"]
	if !ok {
		val, ok = args["field"]
		if !ok {
			return nil, fmt.Errorf("rmfield filter requires a 'field' argument")
		}
	}
	fields := util.TokenizeString(val)

	chf := &RmfieldFilter{
		Fields: fields,
	}
	chf.InitBaseFilter(args, eng, chf.RmfieldFilterFunc)
	return chf, nil
}

func (rm *RmfieldFilter) String() string {
	return fmt.Sprintf("name=%s type=rmfield fields=%s", util.SafeFormat(rm.Name()), util.SafeFormat(rm.Fields...))
}

func (rm *RmfieldFilter) RmfieldFilterFunc(ev *events.LogEvent) *events.LogEvent {
	for _, field := range rm.Fields {
		delete(ev.Fields, field)
	}
	return ev
}

//////////////////////////////////////////////////////////////////////////////
// THE END
