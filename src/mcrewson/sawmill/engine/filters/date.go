// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package filters

import (
	"mcrewson/sawmill/engine"
	"mcrewson/sawmill/events"
	"mcrewson/sawmill/util"
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"time"
)

func init() {
	engine.RegisterFilter("date", DateInit)
}

//////////////////////////////////////////////////////////////////////////////

type DateFilter struct {
	engine.BaseFilter

	inField       string
	timezone      *time.Location
	matches       []string
	removeInField bool
}

var _default_inField = "@timestamp"
var _default_timezone = "America/Vancouver"
var _default_removeinfield = false

var comma_fracsecond_regexp = regexp.MustCompile(`(\d{2}),(\d{3,9})\b`)

func DateInit(args map[string]string, eng *engine.Engine) (engine.FilterChainable, error) {
	val, ok := args["infield"]
	if !ok {
		val = _default_inField
	}
	inField := val

	val, ok = args["matches"]
	if !ok {
		val = ""
	}
	matches := strings.Split(val, "||")
	for i := range matches {
		matches[i] = strings.TrimSpace(matches[i])
	}

	val, ok = args["timezone"]
	if !ok {
		val = _default_timezone
	}
	timezone, err := time.LoadLocation(val)
	if err != nil {
		return nil, fmt.Errorf("Invalid timezone: %s\n", timezone)
	}

	remove_infield := _default_removeinfield
	val, ok = args["removeinfield"]
	if ok {
		v, err := strconv.ParseBool(val)
		if err != nil {
			return nil, fmt.Errorf("Invalid value for 'removeinfield' parameter")
		}
		remove_infield = v
	}

	chf := &DateFilter{
		inField:       inField,
		timezone:      timezone,
		matches:       matches,
		removeInField: remove_infield,
	}
	chf.InitBaseFilter(args, eng, chf.DateFilterFunc)
	return chf, nil
}

func (d *DateFilter) String() string {
	s := fmt.Sprintf("name=%s type=date", d.Name())
	if d.inField != _default_inField {
		s = s + " infield=%s" + util.SafeFormat(d.inField)
	}
	if len(d.matches) > 0 {
		s = s + " matches=" + util.SafeFormat(strings.Join(d.matches, "||"))
	}
	if d.timezone.String() != _default_timezone {
		s = s + " timezone=" + util.SafeFormat(d.timezone.String())
	}
	return s
}

func (d *DateFilter) DateFilterFunc(ev *events.LogEvent) *events.LogEvent {
	input, exists := ev.Fields[d.inField]
	if !exists {
		return ev
	}

	// convert a fractional second delimited by a comma to a fractional second delimited by a period
	if strings.IndexRune(input, ',') >= 0 {
		input = comma_fracsecond_regexp.ReplaceAllString(input, "$1.$2")
	}

	for _, match := range d.matches {
		parsed, err := time.ParseInLocation(match, input, d.timezone)
		if err == nil {
			// add a year to the parsed date if non exist (this happens for syslog dates)
			if parsed.Year() == 0 {
				parsed = parsed.AddDate(time.Now().Year(), 0, 0)
			}

			ev.Timestamp = parsed
			if d.removeInField {
				delete(ev.Fields, d.inField)
			}
		} else {
			d.Engine.Logger.Debugf("DateFilter(%s): failed to parse date: %v\n", d.Name, err)
		}
	}
	return ev
}

//////////////////////////////////////////////////////////////////////////////
// THE END
