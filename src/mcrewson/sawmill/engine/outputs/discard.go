// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package outputs

import (
	"mcrewson/sawmill/engine"
	"mcrewson/sawmill/events"
	"mcrewson/sawmill/util"
	"fmt"
)

func init() {
	engine.RegisterOutput("discard", DiscardInit)
}

//////////////////////////////////////////////////////////////////////////////

type DiscardOutput struct {
	engine.BufferedOutput
}

func DiscardInit(args map[string]string, eng *engine.Engine) (engine.OutputChainable, error) {
	cho := &DiscardOutput{}
	cho.InitBufferedOutput(args, eng, cho.BufferFlushFunc)
	return cho, nil
}

func (d *DiscardOutput) String() string {
	return fmt.Sprintf("name=%s type=discard", util.SafeFormat(d.Name()))
}

func (d *DiscardOutput) BufferFlushFunc(frame *events.LogFrame) error {
	d.Engine.Logger.Debugf("DiscardOutput(%s): throwing away %d events\n", d.Name(), frame.Count)
	return nil
}

//////////////////////////////////////////////////////////////////////////////
// THE END
