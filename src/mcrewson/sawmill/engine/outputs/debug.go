// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package outputs

import (
	"mcrewson/sawmill/engine"
	"mcrewson/sawmill/events"
	"mcrewson/sawmill/util"
	"fmt"
	"sync"
	"time"
)

func init() {
	engine.RegisterOutput("debug", DebugInit)
}

//////////////////////////////////////////////////////////////////////////////

type DebugOutput struct {
	engine.BufferedOutput

	writeLock sync.Mutex
}

func DebugInit(args map[string]string, eng *engine.Engine) (engine.OutputChainable, error) {
	cho := &DebugOutput{}
	cho.InitBufferedOutput(args, eng, cho.BufferFlushFunc)
	return cho, nil
}

func (d *DebugOutput) String() string {
	return fmt.Sprintf("name=%s type=debug", util.SafeFormat(d.Name()))
}

func (d *DebugOutput) BufferFlushFunc(frame *events.LogFrame) error {
	d.writeLock.Lock()
	defer d.writeLock.Unlock()
	for i := uint32(0); i < frame.Count; i++ {
		ev := frame.Events[i]
		d.Engine.Logger.Debugf("DebugOutput(%s): TIMESTAMP = %s\n", d.Name(), ev.Timestamp.UTC().Format(time.RFC3339Nano))
		for k, v := range ev.Fields {
			d.Engine.Logger.Debugf("DebugOutput(%s): FIELD: %s = \"%s\"\n", d.Name(), k, v)
		}
	}
	return nil
}

//////////////////////////////////////////////////////////////////////////////
// THE END
