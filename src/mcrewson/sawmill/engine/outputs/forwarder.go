// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package outputs

import (
	"mcrewson/sawmill/engine"
	"mcrewson/sawmill/events"
	"mcrewson/sawmill/util/clustersrv"
	"fmt"
	"strconv"
)

func init() {
	engine.RegisterOutput("forwarder", ForwarderInit)
}

//////////////////////////////////////////////////////////////////////////////

type ForwarderOutput struct {
	engine.BufferedOutput
	codec  events.EventCodec
	sender clustersrv.ClusterSender
}

var default_forwarder_compress = false

func ForwarderInit(args map[string]string, eng *engine.Engine) (engine.OutputChainable, error) {
	sender, err := clustersrv.NewClusterSender(args, eng.Logger)
	if err != nil {
		return nil, err
	}

	compress := default_forwarder_compress
	val, ok := args["compress"]
	if ok {
		bval, berr := strconv.ParseBool(val)
		if berr != nil {
			return nil, fmt.Errorf("invalid compress parameter. Must be a boolean value")
		}
		compress = bval
	}

	cho := &ForwarderOutput{
		codec:  events.NewProtobufEventCodec(compress),
		sender: sender,
	}

	err = cho.InitBufferedOutput(args, eng, cho.BufferFlushFunc)
	if err != nil {
		return nil, err
	}
	return cho, nil
}

func (f *ForwarderOutput) String() string {
	return ""
}

func (f *ForwarderOutput) Start() error {
	if f.IsRunning() {
		return nil
	}

	if err := f.sender.Start(); err != nil {
		return err
	}
	return f.BufferedOutput.Start()
}

func (f *ForwarderOutput) Stop() error {
	if !f.IsRunning() {
		return nil
	}
	if err := f.BufferedOutput.Stop(); err != nil {
		return err
	}

	if err := f.sender.Stop(); err != nil {
		return err
	}
	return nil
}

func (f *ForwarderOutput) BufferFlushFunc(frame *events.LogFrame) error {
	if frame.Count == 0 {
		return nil
	}

	payload, _ := f.codec.EncodeFrame(frame)
	err := f.sender.SendData(payload)
	if err != nil {
		return err
	}
	f.MeterMark(int64(frame.Count))
	return nil
}

//////////////////////////////////////////////////////////////////////////////
// THE END
