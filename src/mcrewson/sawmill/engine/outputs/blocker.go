// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package outputs

import (
	"mcrewson/sawmill/engine"
	"mcrewson/sawmill/util"
	"fmt"
)

func init() {
	engine.RegisterOutput("blocker", BlockerInit)
}

//////////////////////////////////////////////////////////////////////////////

type BlockerOutput struct {
	engine.BaseOutput
	stopSignal chan bool
}

func BlockerInit(args map[string]string, eng *engine.Engine) (engine.OutputChainable, error) {
	cho := &BlockerOutput{}
	cho.InitBaseOutput(args, eng, nil)
	cho.stopSignal = make(chan bool, 0)
	return cho, nil
}

func (b *BlockerOutput) String() string {
	return fmt.Sprintf("name=%s type=blocker", util.SafeFormat(b.Name()))
}

func (b *BlockerOutput) Start() error {
	if b.IsRunning() {
		return nil
	}

	// THIS OUTPUT ACTUALLY DOES NOTHING BUT LISTEN FOR A STOP SIGNAL, CAUSING
	// THE CHAINABLES SENDING MESSAGES TO IT TO FILL UP THEIR OUTPUT CHANNEL(S)
	// AND BLOCK...

	go func() {
	runloop:
		for {
			select {
			case <-b.stopSignal:
				break runloop
			}
		}
	}()

	b.MarkRunning()
	b.Engine.Logger.Infof("Output(%s): Started.\n", b.Name())
	return nil
}

func (b *BlockerOutput) Stop() error {
	if !b.IsRunning() {
		return nil
	}
	b.stopSignal <- true
	b.MarkNotRunning()
	b.Engine.Logger.Infof("Output(%s): Stopped.\n", b.Name())
	return nil
}

//////////////////////////////////////////////////////////////////////////////
// THE END
