// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package outputs

import (
	"fmt"
	"math/rand"
	"net"
	"strings"
	"time"

	"github.com/garyburd/redigo/redis"

	"mcrewson/sawmill/engine"
	"mcrewson/sawmill/events"
	"mcrewson/sawmill/util"
)

func init() {
	engine.RegisterOutput("redis", RedisInit)
}

//////////////////////////////////////////////////////////////////////////////

type RedisOutput struct {
	engine.BufferedOutput

	Servers  []string
	List     string
	Timeouts map[string]time.Duration

	codec events.EventCodec
	conn  redis.Conn
}

var default_timeouts = map[string]time.Duration{
	"connecttimeout": time.Duration(2) * time.Second,
	"readtimeout":    time.Duration(10) * time.Second,
	"writetimeout":   time.Duration(10) * time.Second,
}

func RedisInit(args map[string]string, eng *engine.Engine) (engine.OutputChainable, error) {

	val, ok := args["servers"]
	if !ok {
		val, ok = args["server"]
		if !ok {
			return nil, fmt.Errorf("no servers defined for this redis output")
		}
	}
	servers := strings.Split(val, ",")
	for i := range servers {
		servers[i] = strings.TrimSpace(servers[i])
	}

	val, ok = args["list"]
	if !ok {
		return nil, fmt.Errorf("list not specified for this redis output")
	}
	list := val

	timeouts, err := util.ParseNetworkTimeouts(args, default_timeouts)
	if err != nil {
		return nil, err
	}

	cho := &RedisOutput{
		Servers:  servers,
		List:     list,
		Timeouts: timeouts,
		codec:    events.NewProtobufEventCodec(true),
	}

	cho.InitBufferedOutput(args, eng, cho.BufferFlushFunc)

	return cho, nil
}

func (r *RedisOutput) String() string {
	s := fmt.Sprintf("name=%s type=redis servers=%s list=%s",
		util.SafeFormat(r.Name()), util.SafeFormat(r.Servers...), util.SafeFormat(r.List))
	if r.Timeouts["connect"] != default_timeouts["connecttimeout"] {
		s = s + " connecttimeout=" + util.DurationToString(r.Timeouts["connect"])
	}
	if r.Timeouts["read"] != default_timeouts["readtimeout"] {
		s = s + " readtimeout=" + util.DurationToString(r.Timeouts["read"])
	}
	if r.Timeouts["write"] != default_timeouts["writetimeout"] {
		s = s + " writetimeout=" + util.DurationToString(r.Timeouts["write"])
	}

	return s
}

func (r *RedisOutput) BufferFlushFunc(frame *events.LogFrame) error {
	r.Engine.Logger.Debugf("RedisOutput(%s): writing %d events\n", r.Name(), frame.Count)
	payload, _ := r.codec.EncodeFrame(frame)

	if r.conn == nil {
		if connected := r.connect(); connected == false {
			return fmt.Errorf("Cannot connect to redis server")
		}
	}

	for {
		if _, err := r.conn.Do("RPUSH", r.List, payload); err != nil {
			r.Engine.Logger.Errorf("RedisOuput(%s): Redis error, will reconnect: %s\n", r.Name(), err)
			r.conn.Close()
			if connected := r.connect(); connected == true {
				continue
			}
		}
		break
	}
	return nil
}

func (r *RedisOutput) connect() bool {

	for {
		if !r.IsRunning() {
			// stop attempting to connect when shutting down
			break
		}
		host := r.Servers[rand.Int()%len(r.Servers)]
		host, port, err := net.SplitHostPort(host)
		addresses, err := net.LookupHost(host)

		if err != nil {
			r.Engine.Logger.Errorf("RedisOutput(%s): DNS lookup failure \"%s\": %s\n", r.Name(), host, err)
			time.Sleep(1 * time.Second)
			continue
		}

		address := net.JoinHostPort(addresses[rand.Int()%len(addresses)], port)
		r.Engine.Logger.Infof("RedisOutput(%s): Connecting to REDIS @ %s (%s)\n", r.Name(), address, host)

		r.conn, err = redis.DialTimeout("tcp", address, r.Timeouts["connect"], r.Timeouts["read"], r.Timeouts["write"])
		if err != nil {
			r.Engine.Logger.Errorf("RedisOutput(%s): Failed to connect with %s: %s\n", r.Name(), address, err)
			time.Sleep(1 * time.Second)
			continue
		}

		r.Engine.Logger.Infof("RedisOutput(%s): Successfully connected to REDIS @ %s\n", r.Name(), address)
		return true
	}
	return false
}

//////////////////////////////////////////////////////////////////////////////
// THE END
