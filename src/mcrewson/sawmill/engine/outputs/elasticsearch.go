// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package outputs

import (
	"mcrewson/sawmill/engine"
	"mcrewson/sawmill/events"
	"mcrewson/sawmill/util"
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

func init() {
	engine.RegisterOutput("elasticsearch", ElasticSearchInit)
}

//////////////////////////////////////////////////////////////////////////////

type ElasticSearchOutput struct {
	engine.BufferedOutput

	host        string
	port        string
	username    string
	password    string
	replication string

	index          string
	indexFormatter *events.EventFormatter

	indexType          string
	indexTypeFormatter *events.EventFormatter

	documentId          string
	documentIdFormatter *events.EventFormatter
}

var default_elasticsearch_index = "logevents-%{+2006.01.02}"
var default_elasticsearch_indextype = ""
var default_elasticsearch_port = "9200"
var default_elasticsearch_username = ""
var default_elasticsearch_password = ""
var default_elasticsearch_documentid = ""
var default_elasticsearch_replication = "sync"

func ElasticSearchInit(args map[string]string, eng *engine.Engine) (engine.OutputChainable, error) {

	val, ok := args["host"]
	if !ok {
		return nil, fmt.Errorf("No host defined for this elasticsearch output")
	}
	host := val

	getStringWithDefault := func(key, defval string) string {
		val, ok := args[key]
		if !ok {
			return defval
		}
		return val
	}

	port := getStringWithDefault("port", default_elasticsearch_port)
	username := getStringWithDefault("username", default_elasticsearch_username)
	password := getStringWithDefault("password", default_elasticsearch_password)

	replication := getStringWithDefault("replication", default_elasticsearch_replication)
	if replication != "async" && replication != "sync" {
		return nil, fmt.Errorf("Replication must be one of \"async\",\"sync\" in this elasticsearch output")
	}

	index := getStringWithDefault("index", default_elasticsearch_index)
	indexFormatter, err := events.NewEventFormatter(index)
	if err != nil {
		return nil, err
	}

	var indextypeFormatter *events.EventFormatter = nil
	indexType := getStringWithDefault("indextype", default_elasticsearch_indextype)
	if indexType != "" {
		indextypeFormatter, err = events.NewEventFormatter(indexType)
		if err != nil {
			return nil, err
		}
	}

	var documentidFormatter *events.EventFormatter = nil
	documentId := getStringWithDefault("documentId", default_elasticsearch_documentid)
	if documentId != "" {
		documentidFormatter, err = events.NewEventFormatter(documentId)
		if err != nil {
			return nil, err
		}
	}

	cho := &ElasticSearchOutput{
		host:                host,
		port:                port,
		username:            username,
		password:            password,
		replication:         replication,
		index:               index,
		indexFormatter:      indexFormatter,
		indexType:           indexType,
		indexTypeFormatter:  indextypeFormatter,
		documentId:          documentId,
		documentIdFormatter: documentidFormatter,
	}
	cho.InitBufferedOutput(args, eng, cho.BufferFlushFunc)
	return cho, nil
}

func (es *ElasticSearchOutput) String() string {
	parts := make([]string, 0)
	parts = append(parts, fmt.Sprintf("name=%s type=elasticsearch", util.SafeFormat(es.Name())))
	parts = append(parts, fmt.Sprintf("host=%s", util.SafeFormat(es.host)))
	parts = append(parts, fmt.Sprintf("port=%s", util.SafeFormat(es.port)))
	if es.username != "" {
		parts = append(parts, fmt.Sprintf("username=%s", util.SafeFormat(es.username)))
	}
	if es.password != "" {
		parts = append(parts, fmt.Sprintf("password=%s", util.SafeFormat(es.password)))
	}
	parts = append(parts, fmt.Sprintf("index=%s", util.SafeFormat(es.index)))
	if es.indexType != "" {
		parts = append(parts, fmt.Sprintf("indextype=%s", util.SafeFormat(es.indexType)))
	}
	if es.documentId != "" {
		parts = append(parts, fmt.Sprintf("documentid=%s", util.SafeFormat(es.documentId)))
	}
	parts = append(parts, fmt.Sprintf("replication=%s", util.SafeFormat(es.replication)))

	return strings.Join(parts, " ")
}

func (es *ElasticSearchOutput) BufferFlushFunc(frame *events.LogFrame) error {
	es.Engine.Logger.Debugf("ElasticSearchOutput(%s): Writing %d events\n", es.Name, frame.Count)
	body := es._build_body(frame)
	es._post(body)
	es.MeterMark(int64(frame.Count))
	return nil
}

//////////////////////////////////////////////////////////////////////////////

func (es *ElasticSearchOutput) _build_body(frame *events.LogFrame) string {
	var buffer bytes.Buffer
	for i := uint32(0); i < frame.Count; i++ {
		ev := frame.Events[i]
		index := es.indexFormatter.Format(ev)

		var typ string
		if es.indexTypeFormatter == nil {
			etype, exists := ev.Fields["type"]
			if !exists {
				etype = "logs"
			}
			typ = etype
		} else {
			typ = string(es.indexTypeFormatter.Format(ev))
		}

		id := ""
		if es.documentIdFormatter != nil {
			id = fmt.Sprintf(",\"_id\":\"%s\"", es.documentIdFormatter.Format(ev))
		}
		header := fmt.Sprintf("{\"index\":{\"_index\":\"%s\",\"_type\":\"%s\"%s}}", index, typ, id)
		buffer.WriteString(header + "\n")
		buffer.Write(ev.EncodeJson())
		buffer.WriteByte('\n')
	}
	return buffer.String()
}

func (es *ElasticSearchOutput) _post(body string) {
	auth := ""
	if es.username != "" && es.password != "" {
		auth = es.username + ":" + es.password + "@"
	}

	bulkUrl := fmt.Sprintf("http://%s%s:%s/_bulk?replication=%s", auth, es.host, es.port, es.replication)

	resp, err := http.Post(bulkUrl, "text/json", strings.NewReader(body))
	if err != nil {
		es.Engine.Logger.Errorf("ElasticSearchOutput(%s): Error while POSTing (%s:%s): %s\n", es.Name, es.host, es.port, err)
		return
	}
	defer resp.Body.Close()

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		es.Engine.Logger.Errorf("ElasticSearchOutput(%s): Error while reading response from POST (%s:%s): %s\n", es.Name, es.host, es.port, err)
		return
	}

	if resp.StatusCode != 200 {
		es.Engine.Logger.Errorf("ElasticSearchOutput(%s): Invalid response while POSTing (%s:%s): status=%d, response=%s\n", es.Name, es.host, es.port, resp.StatusCode, respBody)
		return
	}
}

//////////////////////////////////////////////////////////////////////////////
// THE END
