// vim:set ts=4 sw=4 et nowrap syntax=go:
//

package outputs

import (
	"mcrewson/sawmill/engine"
	"mcrewson/sawmill/events"
	"mcrewson/sawmill/util"
	"fmt"
)

func init() {
	engine.RegisterOutput("failer", FailerInit)
}

//////////////////////////////////////////////////////////////////////////////

type FailerOutput struct {
	engine.BufferedOutput
}

func FailerInit(args map[string]string, eng *engine.Engine) (engine.OutputChainable, error) {
	cho := &FailerOutput{}
	cho.InitBufferedOutput(args, eng, cho.BufferFlushFunc)
	return cho, nil
}

func (f *FailerOutput) String() string {
	return fmt.Sprintf("name=%s type=failer", util.SafeFormat(f.Name()))
}

func (f *FailerOutput) BufferFlushFunc(frame *events.LogFrame) error {
	return fmt.Errorf("i hate you")
}

//////////////////////////////////////////////////////////////////////////////
// THE END
