// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package outputs

import (
	"mcrewson/sawmill/engine"
	"mcrewson/sawmill/events"
	"mcrewson/sawmill/util"
	"mcrewson/sawmill/util/lru"
	"bufio"
	"compress/gzip"
	"fmt"
	"os"
	"path"
	"strconv"
	"sync"
	"time"
)

func init() {
	engine.RegisterOutput("file", FileInit)
}

//////////////////////////////////////////////////////////////////////////////

type FileOutput struct {
	engine.BufferedOutput

	Path          string
	PathFormatter *events.EventFormatter
	Format        string
	Formatter     *events.EventFormatter
	MaxSize       int64
	FlushInterval time.Duration
	Gzip          bool

	files        *lru.Cache
	last_flush   time.Time
	last_cleanup time.Time

	lock sync.Mutex
}

var default_flush_interval = time.Duration(2) * time.Second
var default_open_files = 512
var default_gzip = false

var stale_cleanup_interval = time.Duration(5) * time.Second

func FileInit(args map[string]string, eng *engine.Engine) (engine.OutputChainable, error) {
	val, ok := args["path"]
	if !ok {
		return nil, fmt.Errorf("no path defined for this file output")
	}
	fpath := val
	pathformatter, pfmtErr := events.NewEventFormatter(fpath)
	if pfmtErr != nil {
		return nil, pfmtErr
	}

	val, ok = args["format"]
	if !ok {
		val = ""
	}
	format := val
	formatter, fmtErr := events.NewEventFormatter(format)
	if fmtErr != nil {
		return nil, fmtErr
	}

	val, ok = args["maxsize"]
	if !ok {
		val = "0"
	}
	maxsize, err := util.ParseSize(val)
	if err != nil {
		return nil, fmt.Errorf("invalid maxsize parameter: %s", err)
	}

	flushinterval := default_flush_interval
	val, ok = args["flushinterval"]
	if ok {
		ival, ierr := strconv.ParseUint(val, 10, 64)
		if ierr != nil {
			return nil, fmt.Errorf("invalid flushinterval parameter. Must be an integer value")
		}
		flushinterval = time.Duration(ival) * time.Second
	}

	gzip := default_gzip
	val, ok = args["gzip"]
	if ok {
		bval, berr := strconv.ParseBool(val)
		if berr != nil {
			return nil, fmt.Errorf("invalid gzip parameter. Must be a boolean value")
		}
		gzip = bval
	}

	now := time.Now()
	cho := &FileOutput{
		Path:          fpath,
		PathFormatter: pathformatter,
		Format:        format,
		Formatter:     formatter,
		MaxSize:       maxsize,
		FlushInterval: flushinterval,
		Gzip:          gzip,
		last_flush:    now,
		last_cleanup:  now,
	}

	cho.files = lru.New(default_open_files)
	cho.files.OnEvicted = func(key lru.Key, value interface{}) { value.(*IOWriter).Close() }

	cho.InitBufferedOutput(args, eng, cho.BufferFlushFunc)
	return cho, nil
}

func (f *FileOutput) String() string {
	r := fmt.Sprintf("name=%s type=file path=%s", util.SafeFormat(f.Name()), util.SafeFormat(f.Path))
	if f.Format != "" {
		r = fmt.Sprintf("%s format=%s", r, util.SafeFormat(f.Format))
	}
	if f.MaxSize != 0 {
		r = fmt.Sprintf("%s maxsize=%d", r, f.MaxSize)
	}
	if f.FlushInterval != default_flush_interval {
		r = fmt.Sprintf("%s flushinterval=%d", r, f.FlushInterval/time.Second)
	}
	if f.Gzip == true {
		r = fmt.Sprintf("%s gzip=true", r)
	}
	return r
}

func (f *FileOutput) Stop() error {
	if !f.IsRunning() {
		return nil
	}

	if err := f.BufferedOutput.Stop(); err != nil {
		return err
	}

	f.lock.Lock()
	defer f.lock.Unlock()

	for {
		if f.files.Len() == 0 {
			break
		}
		f.files.RemoveOldest()
	}

	return nil
}

func (f *FileOutput) BufferFlushFunc(frame *events.LogFrame) error {
	f.lock.Lock()
	defer f.lock.Unlock()
	for i := uint32(0); i < frame.Count; i++ {
		f.writeEvent(frame.Events[i])
	}
	f.flush()
	f.MeterMark(int64(frame.Count))
	return nil
}

func (f *FileOutput) writeEvent(ev *events.LogEvent) bool {

	fpath := f.PathFormatter.FormatString(ev)
	w, err := f.open(fpath)
	if err != nil {
		f.Engine.Logger.Errorf("Cannot open output file: %s - %s\n", fpath, err)
		f.closeStaleFiles()
		return false
	}

	var output []byte
	if f.Format != "" {
		output = f.Formatter.Format(ev)
	} else {
		output = []byte(ev.Fields["message"])
	}
	output = append(output, '\n')

	_, err = w.Write(output)
	if err != nil {
		f.Engine.Logger.Errorf("Failed to write event: %s\n", err)
		f.closeStaleFiles()
		return false
	}
	f.closeStaleFiles()
	return true
}

func (f *FileOutput) open(fpath string) (*IOWriter, error) {
	now := time.Now()
	val, ok := f.files.Get(fpath)
	if ok && val != nil {
		iow := val.(*IOWriter)
		if now.Sub(iow.lastStat) < time.Second {
			return iow, nil
		}

		info, staterr := os.Stat(fpath)
		if staterr != nil {
			// cannot stat the file? WTF?
			// Lets just discard it and try again from scratch
			iow.Close()
			f.files.Remove(fpath)
		} else {
			iow.lastStat = now
			if f.MaxSize != 0 && info.Size() >= f.MaxSize {
				// reach maxsize. Rotate it
				iow.Close()
				f.files.Remove(fpath)
				if err := f.rotateFile(fpath); err != nil {
					f.Engine.Logger.Warningf("Failed to rotate file (%s): %s\n", err, fpath)
					// reuse existing IOWriter
					return iow, nil
				}
			} else {
				return iow, nil
			}
		}
	}

	info, staterr := os.Stat(fpath)
	if staterr == nil {
		// file already exists. check the file size
		if f.MaxSize != 0 && info.Size() >= f.MaxSize {
			// reached maxsize. Rotate it
			if err := f.rotateFile(fpath); err != nil {
				f.Engine.Logger.Warningf("Failed to rotate file (%s): %s\n", err, fpath)
			}
		}
	}

	f.Engine.Logger.Infof("Opening file: %s\n", fpath)

	dir := path.Dir(fpath)
	if err := os.MkdirAll(dir, 0777); err != nil {
		return nil, err
	}

	fp, err := os.OpenFile(fpath, os.O_RDWR|os.O_APPEND|os.O_CREATE, 0666)
	if err != nil {
		return nil, err
	}

	writer := bufio.NewWriter(fp)

	iow := &IOWriter{w: writer}
	if f.Gzip {
		iow.g = gzip.NewWriter(iow.w)
	}
	f.files.Add(fpath, iow)
	return iow, nil
}

func (f *FileOutput) flush() {
	if time.Since(f.last_flush) > f.FlushInterval {
		f.Engine.Logger.Debug("Starting flush cycle\n")
		f.files.OnEach(func(key lru.Key, value interface{}) {
			fpath := key.(string)
			iow := value.(*IOWriter)
			f.Engine.Logger.Debugf("Flushing file %s\n", fpath)
			iow.Flush()
		})
		f.last_flush = time.Now()

	}
}

func (f *FileOutput) closeStaleFiles() {
	if time.Since(f.last_cleanup) > stale_cleanup_interval {
		f.Engine.Logger.Debug("Starting stale file cleanup cycle\n")
		f.files.OnEach(func(key lru.Key, value interface{}) {
			fpath := key.(string)
			iow := value.(*IOWriter)
			if iow.active == false {
				f.Engine.Logger.Debugf("Closing stale file %s\n", fpath)
				f.files.Remove(key)
			}
		})

		// mark all files inactive, a call to write will mark them active again
		f.files.OnEach(func(key lru.Key, value interface{}) {
			iow := value.(*IOWriter)
			iow.active = false
		})
		f.last_cleanup = time.Now()
	}

}

func (f *FileOutput) rotateFile(fpath string) error {
	f.Engine.Logger.Infof("Rotating file: %s\n", fpath)
	num := 1
	newfpath := fmt.Sprintf("%s.%d", fpath, num)
	for {
		if _, err := os.Stat(newfpath); os.IsNotExist(err) {
			break
		}
		num += 1
		newfpath = fmt.Sprintf("%s.%d", fpath, num)
	}
	return os.Rename(fpath, newfpath)
}

//////////////////////////////////////////////////////////////////////////////

type IOWriter struct {
	w        *bufio.Writer
	g        *gzip.Writer
	lastStat time.Time
	active   bool
}

func (iow *IOWriter) Write(b []byte) (n int, err error) {
	if iow.g != nil {
		n, err = iow.g.Write(b)
	} else {
		n, err = iow.w.Write(b)
	}
	iow.active = true
	return
}

func (iow *IOWriter) Flush() {
	iow.w.Flush()
	if iow.g != nil {
		iow.g.Flush()
	}
}

func (iow *IOWriter) Close() error {
	iow.Flush()
	if iow.g != nil {
		return iow.g.Close()
	}
	return nil
}

//////////////////////////////////////////////////////////////////////////////
// THE END
