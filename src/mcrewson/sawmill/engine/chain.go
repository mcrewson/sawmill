// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package engine

import (
	"mcrewson/sawmill/events"
	"fmt"
	"strings"

	"github.com/rcrowley/go-metrics"
)

//////////////////////////////////////////////////////////////////////////////

type ChainChainable interface {
	Chainable
	CallChain(*events.LogEvent) *events.LogEvent
}

func init() {
	RegisterChain("chain", ChainInit)
}

//////////////////////////////////////////////////////////////////////////////

type ChainStep struct {
	stepType        ChainStepType
	internalFilters []FilterChainable
	externalChain   ChainChainable
}

type ChainStepType int

const (
	INTERNALFILTER ChainStepType = iota
	EXTERNALCHAIN
)

type BaseChain struct {
	BaseChainable
	chainText  string
	steps      []ChainStep
	input      InputChainable
	inpChannel events.LogEventChannel
	output     OutputChainable
	outChannel events.LogEventChannel
	stopSignal chan bool
	dropMeter  metrics.Meter
	inOutLoop  func()
}

func ChainInit(args map[string]string, eng *Engine) (ChainChainable, error) {
	chainables := make([]Chainable, 0)

	val, ok := args["chain"]
	if !ok {
		return nil, fmt.Errorf("chain not specified")
	}
	for _, elem := range strings.Split(val, ",") {
		elem = strings.TrimSpace(elem)
		chainable, ok := eng.GetChainable(elem)
		if !ok {
			return nil, fmt.Errorf("unknown element in chain: %s", elem)
		}
		chainables = append(chainables, chainable)
	}

	names := make([]string, 0)
	for _, ch := range chainables {
		names = append(names, ch.Name())
	}
	chainText := strings.Join(names, ",")

	chc := &BaseChain{chainText: chainText}
	err := chc.InitBaseChain(args, eng)
	if err != nil {
		return nil, err
	}
	err = chc.makeChain(chainables)
	if err != nil {
		return nil, err
	}

	return chc, nil
}

func (bcc *BaseChain) InitBaseChain(args map[string]string, eng *Engine) (err error) {
	err = bcc.InitBaseChainable(args, eng, CHAIN)
	if err != nil {
		return err
	}
	bcc.steps = make([]ChainStep, 0)

	bcc.dropMeter = metrics.NewMeter()
	eng.RegisterMetric("chains."+bcc.chainName+".dropped-events", bcc.dropMeter)

	return nil
}

func (bcc *BaseChain) String() string {
	return fmt.Sprintf("name=%s chain=%s", bcc.Name(), bcc.chainText)
}

func (bcc *BaseChain) Start() error {
	if bcc.IsRunning() {
		return nil
	}
	bcc.MarkRunning()
	if bcc.inOutLoop != nil {
		go bcc.inOutLoop()
	}
	bcc.Engine.Logger.Infof("Chain(%s): Started.\n", bcc.Name())
	return nil
}

func (bcc *BaseChain) Stop() error {
	if !bcc.IsRunning() {
		return nil
	}
	if bcc.stopSignal != nil {
		bcc.stopSignal <- true
	}
	bcc.MarkNotRunning()
	bcc.Engine.Logger.Infof("Chain(%s): Stopped.\n", bcc.Name())
	return nil
}

func (bcc *BaseChain) Cleanup() error {
	if bcc.IsRunning() {
		if err := bcc.Stop(); err != nil {
			return err
		}
	}
	if bcc.input != nil {
		bcc.input.RemoveOutputChannel(bcc.inpChannel)
	}
	if bcc.output != nil {
		bcc.output.RemoveInputChannel(bcc.outChannel)
	}
	return nil
}

func (bcc *BaseChain) makeChain(chainables []Chainable) error {
	// first, some validation
	if len(chainables) < 2 {
		return fmt.Errorf("chain must have at least two elements")
	}
	for _, chain := range chainables[1:] {
		if chain.Type() == INPUT {
			return fmt.Errorf("input is only allowed at the beginning of a chain")
		}
	}
	for _, chain := range chainables[:len(chainables)-1] {
		if chain.Type() == OUTPUT {
			return fmt.Errorf("output is only allowed at the end of a chain")
		}
	}

	filters := make([]FilterChainable, 0)

	for _, chainable := range chainables {
		switch chainable.Type() {
		case FILTER:
			filter := chainable.(FilterChainable)
			filters = append(filters, filter)

		case INPUT:
			if len(filters) > 0 {
				bcc.makeFilterStep(filters)
				filters = make([]FilterChainable, 0)
			}
			bcc.input = chainable.(InputChainable)

		case OUTPUT:
			if len(filters) > 0 {
				bcc.makeFilterStep(filters)
				filters = make([]FilterChainable, 0)
			}
			bcc.output = chainable.(OutputChainable)

		case CHAIN:
			if len(filters) > 0 {
				bcc.makeFilterStep(filters)
				filters = make([]FilterChainable, 0)
			}
			chain := chainable.(ChainChainable)
			bcc.makeChainStep(chain)
		}
	}
	if len(filters) > 0 {
		bcc.makeFilterStep(filters)
	}

	// now if we have input or outputs on this chain, setup the goroutine that hooks them up
	//
	if bcc.input != nil && bcc.output != nil {
		// both input and outputs defined
		bcc.inpChannel = make(events.LogEventChannel, bcc.input.Capacity())
		bcc.outChannel = make(events.LogEventChannel, bcc.output.Capacity())
		bcc.stopSignal = make(chan bool)
		bcc.inOutLoop = func() {
			multiout := bcc.input.HasMultipleOutputs()
		runloop:
			for {
				select {
				case ev := <-bcc.inpChannel:
					if multiout {
						ev = bcc.CallChain(ev.Copy())
					} else {
						ev = bcc.CallChain(ev)
					}
					if ev != nil {
						select {
						case bcc.outChannel <- ev:
						default:
							bcc.Engine.Logger.Warningf("Chain(%s): dropped event. Its output channel is blocked.\n", bcc.Name())
							bcc.dropMeter.Mark(1)
						}
					}
				case <-bcc.stopSignal:
					break runloop
				}
			}
		}
		bcc.input.AddOutputChannel(bcc.inpChannel)
		bcc.output.AddInputChannel(bcc.outChannel)

	} else if bcc.input != nil {
		return fmt.Errorf("input but no output specified for chain. not implemented yet")

	} else if bcc.output != nil {
		return fmt.Errorf("output but no input specified for chain. not implemented yet")
	}

	return nil
}

func (bcc *BaseChain) CallChain(ev *events.LogEvent) *events.LogEvent {
planloop:
	for _, step := range bcc.steps {
		if step.stepType == INTERNALFILTER {
			for _, filter := range step.internalFilters {
				ev = filter.FilterEvent(ev)
				if ev == nil {
					break planloop
				}
			}
		} else if step.stepType == EXTERNALCHAIN {
			ev = step.externalChain.CallChain(ev)
			if ev == nil {
				break planloop
			}
		}
	}

	return ev
}

func (bcc *BaseChain) makeFilterStep(filters []FilterChainable) {
	internalFilters := make([]FilterChainable, len(filters))
	copy(internalFilters, filters)
	step := ChainStep{
		stepType:        INTERNALFILTER,
		internalFilters: internalFilters,
	}
	bcc.steps = append(bcc.steps, step)
}

func (bcc *BaseChain) makeChainStep(chain ChainChainable) {
	step := ChainStep{
		stepType:      EXTERNALCHAIN,
		externalChain: chain,
	}
	bcc.steps = append(bcc.steps, step)
}

//////////////////////////////////////////////////////////////////////////////
// THE END
