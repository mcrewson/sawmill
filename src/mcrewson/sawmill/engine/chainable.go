// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package engine

import (
	"fmt"
	"strconv"
)

//////////////////////////////////////////////////////////////////////////////

type Chainable interface {
	Name() string
	Type() ChainableType
	String() string

	IsRunning() bool
	Start() error
	Stop() error

	Cleanup() error

	Capacity() uint64
}

type ChainableType int

const (
	INPUT ChainableType = iota
	FILTER
	OUTPUT
	CHAIN
)

//////////////////////////////////////////////////////////////////////////////

type BaseChainable struct {
	Engine        *Engine
	chainName     string
	chainType     ChainableType
	chainCapacity uint64
	running       bool
}

var default_capacity uint64 = 512

func (bc *BaseChainable) Name() string {
	return bc.chainName
}

func (bc *BaseChainable) Type() ChainableType {
	return bc.chainType
}

func (bc *BaseChainable) InitBaseChainable(args map[string]string, eng *Engine, typ ChainableType) error {
	name, ok := args["name"]
	if !ok {
		return fmt.Errorf("chainable requires a 'name' argument")
	}

	capacity := default_capacity
	val, ok := args["capacity"]
	if ok {
		ival, ierr := strconv.ParseUint(val, 10, 64)
		if ierr != nil {
			return fmt.Errorf("invalid capacity parameter. Must be an integer value")
		}
		capacity = ival
	}

	bc.chainName = name
	bc.chainType = typ
	bc.chainCapacity = capacity
	bc.Engine = eng
	return nil
}

func (bc *BaseChainable) MarkRunning() {
	bc.running = true
}

func (bc *BaseChainable) MarkNotRunning() {
	bc.running = false
}

func (bc *BaseChainable) IsRunning() bool {
	return bc.running
}

func (bc *BaseChainable) Cleanup() error {
	return nil
}

func (bc *BaseChainable) Capacity() uint64 {
	return bc.chainCapacity
}

//////////////////////////////////////////////////////////////////////////////
// THE END
