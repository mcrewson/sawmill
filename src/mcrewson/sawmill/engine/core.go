// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package engine

import (
	"bytes"
	"fmt"
	"sort"
	"sync"

	"mcrewson/sawmill/util/logging"
	"github.com/rcrowley/go-metrics"
)

//////////////////////////////////////////////////////////////////////////////

type Engine struct {
	Logger *logging.Logger

	allChainables     map[string]Chainable
	allChainablesLock sync.Mutex

	variables     map[string]string
	variablesLock sync.Mutex

	metrics metrics.Registry

	stopWg sync.WaitGroup
}

func GetTheEngine() (eng *Engine, err error) {
	if _theEngine == nil {
		_theEngine, err = newEngine()
		if err != nil {
			return nil, err
		}
	}
	return _theEngine, nil
}

//////////////////////////////////////////////////////////////////////////////

// Access Methods

func (e *Engine) GetChainable(name string) (chain Chainable, exists bool) {
	e.allChainablesLock.Lock()
	defer e.allChainablesLock.Unlock()
	chain, exists = e.allChainables[name]
	return
}

func (e *Engine) GetAllChainables() (chains []Chainable) {
	e.allChainablesLock.Lock()
	defer e.allChainablesLock.Unlock()
	for _, chain := range e.allChainables {
		chains = append(chains, chain)
	}
	return
}

func (e *Engine) NewInput(args map[string]string) (chain InputChainable, err error) {
	inName, inType, err := e.getNameAndType(args)
	if err != nil {
		return nil, err
	}
	initFunc, ok := _inputTypes[inType]
	if !ok {
		return nil, fmt.Errorf("unknown input type: %s", inType)
	}
	chain, err = initFunc(args, e)
	if err != nil {
		return nil, err
	}
	e.addChainable(inName, chain)
	return
}

func (e *Engine) DeleteInput(name string) error {
	chain, ok := e.GetInput(name)
	if ok {
		if err := chain.Cleanup(); err != nil {
			return err
		}
		e.removeChainable(name)
	}
	return nil
}

func (e *Engine) GetInput(name string) (chain InputChainable, exists bool) {
	e.allChainablesLock.Lock()
	defer e.allChainablesLock.Unlock()
	ch, ok := e.allChainables[name]
	if !ok {
		return nil, false
	}
	if ch.Type() != INPUT {
		return nil, false
	}
	return ch.(InputChainable), true
}

func (e *Engine) GetAllInputs() (chains []InputChainable) {
	e.allChainablesLock.Lock()
	defer e.allChainablesLock.Unlock()
	for _, chain := range e.allChainables {
		if chain.Type() == INPUT {
			chains = append(chains, chain.(InputChainable))
		}
	}
	return
}

func (e *Engine) NewFilter(args map[string]string) (chain FilterChainable, err error) {
	flName, flType, err := e.getNameAndType(args)
	if err != nil {
		return nil, err
	}
	initFunc, ok := _filterTypes[flType]
	if !ok {
		return nil, fmt.Errorf("unknown filter type: %s", flType)
	}
	chain, err = initFunc(args, e)
	if err != nil {
		return nil, err
	}
	e.addChainable(flName, chain)
	return
}

func (e *Engine) DeleteFilter(name string) error {
	chain, ok := e.GetFilter(name)
	if ok {
		if err := chain.Cleanup(); err != nil {
			return err
		}
		e.removeChainable(name)
	}
	return nil
}

func (e *Engine) GetFilter(name string) (chain FilterChainable, exists bool) {
	e.allChainablesLock.Lock()
	defer e.allChainablesLock.Unlock()
	ch, ok := e.allChainables[name]
	if !ok {
		return nil, false
	}
	if ch.Type() != FILTER {
		return nil, false
	}
	return ch.(FilterChainable), true
}

func (e *Engine) GetAllFilters() (chains []FilterChainable) {
	e.allChainablesLock.Lock()
	defer e.allChainablesLock.Unlock()
	for _, chain := range e.allChainables {
		if chain.Type() == FILTER {
			chains = append(chains, chain.(FilterChainable))
		}
	}
	return
}

func (e *Engine) NewOutput(args map[string]string) (chain OutputChainable, err error) {
	outName, outType, err := e.getNameAndType(args)
	if err != nil {
		return nil, err
	}
	initFunc, ok := _outputTypes[outType]
	if !ok {
		return nil, fmt.Errorf("unknown output type: %s", outType)
	}
	chain, err = initFunc(args, e)
	if err != nil {
		return nil, err
	}
	e.addChainable(outName, chain)
	return
}

func (e *Engine) DeleteOutput(name string) error {
	chain, ok := e.GetOutput(name)
	if ok {
		if err := chain.Cleanup(); err != nil {
			return err
		}
		e.removeChainable(name)
	}
	return nil
}

func (e *Engine) GetOutput(name string) (chain OutputChainable, exists bool) {
	e.allChainablesLock.Lock()
	defer e.allChainablesLock.Unlock()
	ch, ok := e.allChainables[name]
	if !ok {
		return nil, false
	}
	if ch.Type() != OUTPUT {
		return nil, false
	}
	return ch.(OutputChainable), true
}

func (e *Engine) GetAllOutputs() (chains []OutputChainable) {
	e.allChainablesLock.Lock()
	defer e.allChainablesLock.Unlock()
	for _, chain := range e.allChainables {
		if chain.Type() == OUTPUT {
			chains = append(chains, chain.(OutputChainable))
		}
	}
	return
}

func (e *Engine) NewChain(args map[string]string) (chain ChainChainable, err error) {
	chName, chType, err := e.getNameAndType(args)
	if err != nil {
		return nil, err
	}
	initFunc, ok := _chainTypes[chType]
	if !ok {
		return nil, fmt.Errorf("unknown chain type: %s", chType)
	}
	chain, err = initFunc(args, e)
	if err != nil {
		return nil, err
	}
	e.addChainable(chName, chain)
	return
}

func (e *Engine) DeleteChain(name string) error {
	chain, ok := e.GetChain(name)
	if ok {
		if err := chain.Cleanup(); err != nil {
			return err
		}
		e.removeChainable(name)
	}
	return nil
}

func (e *Engine) GetChain(name string) (chain ChainChainable, exists bool) {
	e.allChainablesLock.Lock()
	defer e.allChainablesLock.Unlock()
	ch, ok := e.allChainables[name]
	if !ok {
		return nil, false
	}
	if ch.Type() != CHAIN {
		return nil, false
	}
	return ch.(ChainChainable), true
}

func (e *Engine) GetAllChains() (chains []ChainChainable) {
	e.allChainablesLock.Lock()
	defer e.allChainablesLock.Unlock()
	for _, chain := range e.allChainables {
		if chain.Type() == CHAIN {
			chains = append(chains, chain.(ChainChainable))
		}
	}
	return
}

func (e *Engine) FlushAll() error {
	anyrunning := false
	for _, ch := range e.GetAllChainables() {
		if ch.IsRunning() {
			anyrunning = true
			break
		}
	}
	if anyrunning {
		return fmt.Errorf("some chainables still running. Cannot flush")
	}

	// delete chains, outputs, filters, and inputs (in that order)
	for _, ch := range e.GetAllChains() {
		e.DeleteChain(ch.Name())
	}
	for _, ch := range e.GetAllOutputs() {
		e.DeleteOutput(ch.Name())
	}
	for _, ch := range e.GetAllFilters() {
		e.DeleteFilter(ch.Name())
	}
	for _, ch := range e.GetAllInputs() {
		e.DeleteInput(ch.Name())
	}

	// delete variables
	e.variablesLock.Lock()
	defer e.variablesLock.Unlock()
	e.variables = make(map[string]string)

	return nil
}

func (e *Engine) SetVariable(name, value string) {
	e.variablesLock.Lock()
	defer e.variablesLock.Unlock()
	if e.variables == nil {
		e.variables = make(map[string]string)
	}
	e.variables[name] = value
}

func (e *Engine) GetVariable(name string) (value string, exists bool) {
	e.variablesLock.Lock()
	defer e.variablesLock.Unlock()
	value, exists = e.variables[name]
	return
}

func (e *Engine) GetVariableNames() []string {
	e.variablesLock.Lock()
	defer e.variablesLock.Unlock()
	keys := make([]string, len(e.variables))
	i := 0
	for k, _ := range e.variables {
		keys[i] = k
		i += 1
	}
	sort.Strings(keys)
	return keys
}

func (e *Engine) RegisterMetric(name string, metric interface{}) {
	e.metrics.Register(name, metric)
}

func (e *Engine) DumpMetrics() string {
	buffer := new(bytes.Buffer)
	metrics.WriteOnce(e.metrics, buffer)
	return buffer.String()
}

func (e *Engine) WaitForShutdown() {
	e.stopWg.Wait()
}

func (e *Engine) Shutdown() {
	e.Logger.Notice("Shutdown command received. Stopping...\n")

	for _, ch := range e.GetAllChainables() {
		e.Logger.Debugf("Asking chainable(%s) to stop...\n", ch.Name())
		ch.Stop()
	}

	// Flush the engine
	e.FlushAll()

	// signal all routines waiting for the engine to shutdown
	e.stopWg.Done()
}

//////////////////////////////////////////////////////////////////////////////

// internal functions

var _theEngine *Engine

func newEngine() (*Engine, error) {
	e := &Engine{
		allChainables: make(map[string]Chainable),
		metrics:       metrics.NewRegistry(),
	}
	e.stopWg.Add(1)
	return e, nil
}

func (e *Engine) addChainable(name string, chain Chainable) {
	e.allChainablesLock.Lock()
	defer e.allChainablesLock.Unlock()
	e.allChainables[name] = chain
}

func (e *Engine) removeChainable(name string) {
	e.allChainablesLock.Lock()
	defer e.allChainablesLock.Unlock()
	delete(e.allChainables, name)
}

func (e *Engine) getNameAndType(args map[string]string) (inName, inType string, err error) {
	val, ok := args["name"]
	if !ok {
		return "", "", fmt.Errorf("name not specified")
	}
	inName = val
	val, ok = args["type"]
	if !ok {
		return "", "", fmt.Errorf("type not specified")
	}
	inType = val
	return
}

//////////////////////////////////////////////////////////////////////////////

// Chainable Type Registration (generally called during module init() methods)

type InputInitFunc func(map[string]string, *Engine) (InputChainable, error)
type FilterInitFunc func(map[string]string, *Engine) (FilterChainable, error)
type OutputInitFunc func(map[string]string, *Engine) (OutputChainable, error)
type ChainInitFunc func(map[string]string, *Engine) (ChainChainable, error)

var _inputTypes map[string]InputInitFunc
var _filterTypes map[string]FilterInitFunc
var _outputTypes map[string]OutputInitFunc
var _chainTypes map[string]ChainInitFunc

func RegisterInput(typeName string, initFunc InputInitFunc) error {
	if _inputTypes == nil {
		_inputTypes = make(map[string]InputInitFunc)
	}
	_, exists := _inputTypes[typeName]
	if exists {
		return fmt.Errorf("already registered an Input with this type: %s", typeName)
	}
	_inputTypes[typeName] = initFunc
	return nil
}

func RegisterFilter(typeName string, initFunc FilterInitFunc) error {
	if _filterTypes == nil {
		_filterTypes = make(map[string]FilterInitFunc)
	}
	_, exists := _filterTypes[typeName]
	if exists {
		return fmt.Errorf("already registered an Filter with this type: %s", typeName)
	}
	_filterTypes[typeName] = initFunc
	return nil
}

func RegisterOutput(typeName string, initFunc OutputInitFunc) error {
	if _outputTypes == nil {
		_outputTypes = make(map[string]OutputInitFunc)
	}
	_, exists := _outputTypes[typeName]
	if exists {
		return fmt.Errorf("already registered an Output with this type: %s", typeName)
	}
	_outputTypes[typeName] = initFunc
	return nil
}

func RegisterChain(typeName string, initFunc ChainInitFunc) error {
	if _chainTypes == nil {
		_chainTypes = make(map[string]ChainInitFunc)
	}
	_, exists := _chainTypes[typeName]
	if exists {
		return fmt.Errorf("already registered an Chain with this type: %s", typeName)
	}
	_chainTypes[typeName] = initFunc
	return nil
}

//////////////////////////////////////////////////////////////////////////////
// THE END
