// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package util

import (
	"bufio"
	"fmt"
	"strings"
	"unicode/utf8"
)

//////////////////////////////////////////////////////////////////////////////

func TokenizeString(src string) (toks []string) {
	scanner := bufio.NewScanner(strings.NewReader(src))
	scanner.Split(scanQuotedWords)

	for scanner.Scan() {
		toks = append(toks, string(scanner.Bytes()))
	}
	return
}

func TrimQuotes(src string) string {
	var r1, r2 rune
	var w1, w2 int
	r1, w1 = utf8.DecodeRuneInString(src)
	r2, w2 = utf8.DecodeLastRuneInString(src)
	if isQuote(r1) && isQuote(r2) {
		return src[w1 : len(src)-w2]
	}
	return src
}

func Quote(src string) string {
	if strings.IndexAny(src, " \t\n\v\f\r") >= 0 {
		return fmt.Sprintf("\"%s\"", src)
	}
	return src
}

func SafeFormat(src ...string) string {
	if len(src) == 0 {
		return "\"\""
	}

	return Quote(strings.Join(src, ","))
}

func ArgumentsToMap(args []string) (map[string]string, error) {
	cmdargs := make(map[string]string)
	for i := range args {
		parts := strings.SplitN(args[i], "=", 2)
		if len(parts) != 2 {
			return nil, fmt.Errorf("argument should be in the form of 'key=value': %s", args[i])
		}
		key := TrimQuotes(parts[0])
		val := TrimQuotes(parts[1])
		cmdargs[key] = val
	}
	return cmdargs, nil
}

//////////////////////////////////////////////////////////////////////////////

func isQuote(r rune) bool {
	switch r {
	case '\u0022', '\u0027', '\u0060', '\u00b4', '\u2018', '\u2019', '\u201c', '\u201d':
		return true
	}
	return false
}

func isSpace(r rune) bool {
	if r <= '\u00FF' {
		// obvious ascii ones: \t through \r plus space. Pluts two Latin-1 oddballs
		switch r {
		case ' ', '\t', '\n', '\v', '\f', '\r':
			return true
		case '\u0085', '\u00A0':
			return true
		}
		return false
	}

	// high-valued ones
	if '\u2000' <= r && r <= '\u200a' {
		return true
	}
	switch r {
	case '\u1680', '\u180e', '\u2028', '\u2029', '\u202f', '\u205f', '\u3000':
		return true
	}
	return false
}

func scanQuotedWords(data []byte, atEOF bool) (advance int, token []byte, err error) {
	// skip leading spaces.
	start := 0
	for width := 0; start < len(data); start += width {
		var r rune
		r, width = utf8.DecodeRune(data[start:])
		if !isSpace(r) {
			break
		}
	}
	if atEOF && len(data) == 0 {
		return 0, nil, nil
	}

	// scan until space, marking end of word.
	inquotes := false
	for width, i := 0, start; i < len(data); i += width {
		var r rune
		r, width = utf8.DecodeRune(data[i:])
		if isQuote(r) {
			inquotes = !inquotes
		} else if isSpace(r) && !inquotes {
			return i + width, data[start:i], nil
		}
	}

	// if we're atEOF, we have a final, non-empty, non-terminated word. Return it
	if atEOF && len(data) > start {
		return len(data), data[start:], nil
	}

	// request more data
	return 0, nil, nil
}

//////////////////////////////////////////////////////////////////////////////
// THE END
