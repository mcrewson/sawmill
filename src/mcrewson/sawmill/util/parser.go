// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package util

import (
	"fmt"
	"math"
	"regexp"
	"strconv"
	"strings"
	"time"
)

//////////////////////////////////////////////////////////////////////////////

var duration_regexp = regexp.MustCompile(`^(\d+)(ns|nano|nanoseconds?|us|micro|microseconds?|ms|milli|milliseconds?|s|sec|seconds?|m|min|minutes?|h|hours?|d|days?)$`)

const DAY = time.Hour * 24

func ParseDuration(arg string) (time.Duration, error) {
	matches := duration_regexp.FindStringSubmatch(strings.TrimSpace(arg))
	if len(matches) != 3 {
		return 0, fmt.Errorf("invalid duration: %s", arg)
	}
	num, err := strconv.ParseUint(matches[1], 10, 64)
	if err != nil {
		return 0, fmt.Errorf("invalid duration: %s", arg)
	}
	var duration time.Duration
	switch matches[2] {
	case "ns", "nano", "nanosecond", "nanoseconds":
		duration = time.Nanosecond
	case "us", "micro", "microsecond", "microseconds":
		duration = time.Microsecond
	case "ms", "milli", "millisecond", "milliseconds":
		duration = time.Millisecond
	case "s", "sec", "second", "seconds":
		duration = time.Second
	case "m", "min", "minute", "minutes":
		duration = time.Minute
	case "h", "hour", "hours":
		duration = time.Hour
	case "d", "day", "days":
		duration = DAY
	default:
		return 0, fmt.Errorf("invalid duration: %s", arg)
	}
	return time.Duration(num) * duration, nil
}

func DurationToString(arg time.Duration) string {
	if arg >= DAY {
		if rem := math.Remainder(float64(arg), float64(DAY)); rem == 0.0 {
			return fmt.Sprintf("%dd", arg/DAY)
		}
	}
	if arg >= time.Hour {
		if rem := math.Remainder(float64(arg), float64(time.Hour)); rem == 0.0 {
			return fmt.Sprintf("%dh", arg/time.Hour)
		}
	}
	if arg >= time.Minute {
		if rem := math.Remainder(float64(arg), float64(time.Minute)); rem == 0.0 {
			return fmt.Sprintf("%dm", arg/time.Minute)
		}
	}
	if arg >= time.Second {
		if rem := math.Remainder(float64(arg), float64(time.Second)); rem == 0.0 {
			return fmt.Sprintf("%ds", arg/time.Second)
		}
	}
	if arg >= time.Millisecond {
		if rem := math.Remainder(float64(arg), float64(time.Millisecond)); rem == 0.0 {
			return fmt.Sprintf("%dms", arg/time.Millisecond)
		}
	}
	if arg >= time.Microsecond {
		if rem := math.Remainder(float64(arg), float64(time.Microsecond)); rem == 0.0 {
			return fmt.Sprintf("%dus", arg/time.Microsecond)
		}
	}
	return fmt.Sprintf("%dns", arg/time.Nanosecond)
}

func CompareDurationStrings(s1, s2 string) (bool, error) {
	var d1, d2 time.Duration
	var err error
	if d1, err = ParseDuration(s1); err != nil {
		return false, err
	}
	if d2, err = ParseDuration(s2); err != nil {
		return false, err
	}

	return d1 == d2, nil
}

func ParseNetworkTimeouts(args map[string]string, defaults map[string]time.Duration) (map[string]time.Duration, error) {

	getTimeout := func(key string) (timeout time.Duration, err error) {
		val, ok := args[key]
		if !ok {
			defaultval, defaultok := defaults[key]
			if !defaultok {
				panic("WTF? a fallback default timeout should have been provided here")
			}
			return defaultval, nil
		}
		timeout, err = ParseDuration(val)
		if err != nil {
			return 0, fmt.Errorf("%s must be a duration", key)
		}
		return
	}

	var err error
	var connTimeout, readTimeout, writTimeout time.Duration

	// connect
	connTimeout, err = getTimeout("connecttimeout")
	if err != nil {
		return nil, err
	}

	// read
	readTimeout, err = getTimeout("readtimeout")
	if err != nil {
		return nil, err
	}

	// write
	writTimeout, err = getTimeout("writetimeout")
	if err != nil {
		return nil, err
	}

	return map[string]time.Duration{
		"connect": connTimeout,
		"read":    readTimeout,
		"write":   writTimeout,
	}, nil
}

var size_regexp = regexp.MustCompile(`^(\d+)\s?(b|bytes?|k|kb|kilobytes?|m|mb|mega?|megabytes?|g|gb|giga?|gigabytes?|t|tb|tera?|terabytes?)$`)

func ParseSize(arg string) (int64, error) {
	matches := size_regexp.FindStringSubmatch(strings.TrimSpace(arg))
	if len(matches) != 3 {
		return 0, fmt.Errorf("invalid size; %s", arg)
	}
	num, err := strconv.ParseInt(matches[1], 10, 64)
	if err != nil {
		return 0, fmt.Errorf("invalid size: %s", arg)
	}
	var multiplier int64
	switch matches[2] {
	case "b", "byte", "bytes":
		multiplier = 1
	case "k", "kb", "kilobyte", "kilobytes":
		multiplier = 1024
	case "m", "mb", "meg", "mega", "megabyte", "megabytes":
		multiplier = 1048576
	case "g", "gb", "gig", "giga", "gigabyte", "gigabytes":
		multiplier = 1073741824
	case "t", "tb", "ter", "tera", "terabyte", "terabytes":
		multiplier = 1099511627776
	default:
		return 0, fmt.Errorf("invalid size: %s", arg)
	}
	return num * multiplier, nil
}

//////////////////////////////////////////////////////////////////////////////
// THE END
