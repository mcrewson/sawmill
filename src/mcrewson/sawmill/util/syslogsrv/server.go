// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package syslogsrv

import (
	"bytes"
	"log"
	"net"
	"strconv"
	"strings"
	"time"
)

//////////////////////////////////////////////////////////////////////////////

// Syslog server library. It is based on RFC 3164 so it doesn't parse properly
// packets with new header format (described in RFC 5424).

type Message struct {
	Time     time.Time
	Source   net.Addr
	Priority int
	Content  string
}

// Handler handles syslog messages
type Handler interface {
	// Handle should return Message (mayby modified) for future processing by
	// other handlers or return nil. If Handle is called with nil message it
	// should complete all remaining work and properly shutdown before return.
	Handle(*Message) *Message
}

type Server struct {
	conns    []net.PacketConn
	handlers []Handler
	shutdown bool
}

//  NewServer creates idle server
func NewServer() *Server {
	return &Server{}
}

// AddHandler adds h to internal ordered list of handlers
func (s *Server) AddHandler(h Handler) {
	s.handlers = append(s.handlers, h)
}

// Listen starts gorutine that receives syslog messages on specified address.
// addr can be a path (for unix domain sockets) or host:port (for UDP).
func (s *Server) Listen(addr string) error {
	var c net.PacketConn
	if strings.IndexRune(addr, ':') != -1 {
		a, err := net.ResolveUDPAddr("udp", addr)
		if err != nil {
			return err
		}
		c, err = net.ListenUDP("udp", a)
		if err != nil {
			return err
		}
	} else {
		a, err := net.ResolveUnixAddr("unixgram", addr)
		if err != nil {
			return err
		}
		c, err = net.ListenUnixgram("unixgram", a)
		if err != nil {
			return err
		}
	}
	s.conns = append(s.conns, c)
	go s.receiver(c)
	return nil
}

// Shutdown stops server.
func (s *Server) Shutdown() {
	s.shutdown = true
	for _, c := range s.conns {
		err := c.Close()
		if err != nil {
			log.Fatalln(err)
		}
	}
	s.passToHandlers(nil)
	s.conns = nil
	s.handlers = nil
}

func isNulCrLf(r rune) bool {
	return r == 0 || r == '\r' || r == '\n'
}

func (s *Server) passToHandlers(m *Message) {
	for _, h := range s.handlers {
		m = h.Handle(m)
		if m == nil {
			break
		}
	}
}

func (s *Server) receiver(c net.PacketConn) {
	//q := (chan<- Message)(s.q)
	buf := make([]byte, 1024)
	for {
		n, addr, err := c.ReadFrom(buf)
		if err != nil {
			if !s.shutdown {
				log.Fatalln("Read error:", err)
			}
			return
		}
		pkt := buf[:n]

		m := new(Message)
		m.Source = addr
		m.Time = time.Now()

		// Parse priority (if exists)
		prio := 13 // default priority
		if pkt[0] == '<' {
			n = 1 + bytes.IndexByte(pkt[1:], '>')
			if n > 1 && n < 5 {
				p, err := strconv.Atoi(string(pkt[1:n]))
				if err == nil && p >= 0 {
					prio = p
					//pkt = pkt[n+1:]
				}
			}
		}
		m.Priority = prio

		// Parse msg part
		msg := string(bytes.TrimRightFunc(pkt, isNulCrLf))
		m.Content = msg

		s.passToHandlers(m)
	}
}

//////////////////////////////////////////////////////////////////////////////
// THE END
