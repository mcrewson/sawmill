// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package logging

import (
	"fmt"
	"os"
)

//////////////////////////////////////////////////////////////////////////////

func (logger *Logger) Log(level Level, args ...interface{}) {
	logger.log(level, args...)
}

func (logger *Logger) Fatal(args ...interface{}) {
	logger.log(CRITICAL, args...)
	logger.Destroy()
	os.Exit(1)
}

func (logger *Logger) Panic(args ...interface{}) {
	logger.log(CRITICAL, args...)
	logger.Destroy()
	panic(fmt.Sprint(args...))
}

func (logger *Logger) Critical(args ...interface{}) {
	logger.log(CRITICAL, args...)
}

func (logger *Logger) Error(args ...interface{}) {
	logger.log(ERROR, args...)
}

func (logger *Logger) Warning(args ...interface{}) {
	logger.log(WARNING, args...)
}

func (logger *Logger) Notice(args ...interface{}) {
	logger.log(NOTICE, args...)
}

func (logger *Logger) Info(args ...interface{}) {
	logger.log(INFO, args...)
}

func (logger *Logger) Debug(args ...interface{}) {
	logger.log(DEBUG, args...)
}

//////////////////////////////////////////////////////////////////////////////

func (logger *Logger) Logf(level Level, format string, args ...interface{}) {
	logger.logf(level, format, args...)
}

func (logger *Logger) Fatalf(format string, args ...interface{}) {
	logger.logf(CRITICAL, format, args...)
	logger.Destroy()
	os.Exit(1)
}

func (logger *Logger) Panicf(format string, args ...interface{}) {
	logger.logf(CRITICAL, format, args...)
	logger.Destroy()
	panic(fmt.Sprintf(format, args...))
}

func (logger *Logger) Criticalf(format string, args ...interface{}) {
	logger.logf(CRITICAL, format, args...)
}

func (logger *Logger) Errorf(format string, args ...interface{}) {
	logger.logf(ERROR, format, args...)
}

func (logger *Logger) Warningf(format string, args ...interface{}) {
	logger.logf(WARNING, format, args...)
}

func (logger *Logger) Noticef(format string, args ...interface{}) {
	logger.logf(NOTICE, format, args...)
}

func (logger *Logger) Infof(format string, args ...interface{}) {
	logger.logf(INFO, format, args...)
}

func (logger *Logger) Debugf(format string, args ...interface{}) {
	logger.logf(DEBUG, format, args...)
}

//////////////////////////////////////////////////////////////////////////////
// THE END
