// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package logging

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"strings"
	"sync/atomic"
	"time"
	"unsafe"
)

//////////////////////////////////////////////////////////////////////////////

const (
	defaultFormat = "%{time} %{level} %{message}" // default log format
	bufferSize    = 1000                          // buffer size for writer
	queueSize     = 10000                         // chan queue size
)

//////////////////////////////////////////////////////////////////////////////

// Logging Level

type Level int32

const (
	CRITICAL Level = iota
	ERROR
	WARNING
	NOTICE
	INFO
	DEBUG
)

var levelNames = []string{
	"CRITICAL",
	"ERROR",
	"WARNING",
	"NOTICE",
	"INFO",
	"DEBUG",
}

func (level Level) String() string {
	return levelNames[level]
}

func LevelFromName(level string) (Level, error) {
	for i, name := range levelNames {
		if strings.EqualFold(name, level) {
			return Level(i), nil
		}
	}
	return ERROR, fmt.Errorf("logger: invalid log level: %s", level)
}

//////////////////////////////////////////////////////////////////////////////

type record struct {
	level  Level
	time   time.Time
	format string
	args   []interface{}
}

func (rec *record) Message() string {
	if rec.format == "" {
		return fmt.Sprint(rec.args...)
	}
	return fmt.Sprintf(rec.format, rec.args...)
}

//////////////////////////////////////////////////////////////////////////////

type Logger struct {
	name   string
	level  Level
	format string
	out    io.Writer

	formatter *formatter
	queue     chan record
	flush     chan bool
	ringdump  chan []byte
	ringclear chan bool
	quit      chan bool
	fd        *os.File

	inmem_head, inmem_tail    unsafe.Pointer
	inmem_size, inmem_maxsize int32
}

//////////////////////////////////////////////////////////////////////////////

var loggers map[string]*Logger = nil

func GetLogger() (*Logger, error) {
	return GetLoggerByName("")
}

func GetLoggerByName(name string) (*Logger, error) {
	logger, ok := loggers[name]
	if !ok {
		return nil, fmt.Errorf("no logger with this name: %s", name)
	}
	return logger, nil
}

//////////////////////////////////////////////////////////////////////////////

func SimpleLogger(name string) (*Logger, error) {
	return NewLogger(name, NOTICE, defaultFormat, os.Stdout)
}

func FileLogger(name string, level Level, format string, filename string) (*Logger, error) {
	out, err := os.OpenFile(filename, os.O_WRONLY|os.O_APPEND|os.O_CREATE, os.ModeAppend|0666)
	if err != nil {
		return nil, err
	}
	logger, err := NewLogger(name, level, format, out)
	if err != nil {
		return nil, err
	}
	logger.fd = out
	return logger, nil
}

func NullLogger(name string, level Level, format string) (*Logger, error) {
	return NewLogger(name, level, format, ioutil.Discard)
}

func NewLogger(name string, level Level, format string, out io.Writer) (*Logger, error) {
	formatter, err := createFormatter(format)
	if err != nil {
		return nil, err
	}

	logger := &Logger{
		name:      name,
		level:     level,
		format:    format,
		out:       out,
		formatter: formatter,
		queue:     make(chan record, queueSize),
		flush:     make(chan bool),
		ringdump:  make(chan []byte),
		ringclear: make(chan bool),
		quit:      make(chan bool),
		fd:        nil,

		inmem_maxsize: 1024,
	}
	go logger.watcher()
	return logger, nil
}

//////////////////////////////////////////////////////////////////////////////

func (logger *Logger) Destroy() {
	// send the quit signal
	logger.quit <- true
	// wait for the watcher to exit
	<-logger.quit

	// clean up
	if logger.fd != nil {
		logger.fd.Close()
	}
}

func (logger *Logger) DumpMemory() []byte {
	// send the dump signal
	logger.ringdump <- []byte("x")
	// wait for the dump to finish, and return its contents
	return <-logger.ringdump
}

func (logger *Logger) ClearMemory() {
	// send the clear signal
	logger.ringclear <- true
	// wait for the clear to finish
	<-logger.ringclear
}

func (logger *Logger) Flush() {
	// send the flush signal
	logger.flush <- true
	// wait for the flush to finish
	<-logger.flush
}

func (logger *Logger) Name() string {
	return logger.name
}

func (logger *Logger) Level() Level {
	return Level(atomic.LoadInt32((*int32)(&logger.level)))
}

func (logger *Logger) Format() string {
	return logger.format
}

func (logger *Logger) Writer() io.Writer {
	return logger.out
}

// Setter Functions

func (logger *Logger) SetLevel(level Level) {
	atomic.StoreInt32((*int32)(&logger.level), int32(level))
}

func (logger *Logger) SetWriter(out ...io.Writer) {
	logger.out = io.MultiWriter(out...)
}

//////////////////////////////////////////////////////////////////////////////

func (logger *Logger) watcher() {
	var buf bytes.Buffer
	for {
		timeout := time.After(time.Second / 10)
		for i := 0; i < bufferSize; i++ {
			select {
			case rec := <-logger.queue:
				logger.writeRecord(&buf, &rec)
			case <-timeout:
				i = bufferSize
			case <-logger.flush:
				logger.flushBuf(&buf)
				logger.flush <- true
				i = bufferSize
			case <-logger.ringdump:
				// do something here
				logger.ringdump <- logger.getRingBuffer()
			case <-logger.ringclear:
				logger.resetRingBuffer()
				logger.ringclear <- true
			case <-logger.quit:
				// if quit signal is received, cleanse the channel
				// and write all of the pending records to io.Writer
				for {
					select {
					case rec := <-logger.queue:
						logger.writeRecord(&buf, &rec)
					case <-logger.flush:
						// do nothing
					default:
						logger.flushBuf(&buf)
						logger.quit <- true
						return
					}
				}
			}
		}
		logger.flushBuf(&buf)
	}
}

func (logger *Logger) flushBuf(b *bytes.Buffer) {
	if len(b.Bytes()) > 0 {
		logger.out.Write(b.Bytes())
		b.Reset()
	}
}

func (logger *Logger) writeRecord(b *bytes.Buffer, rec *record) {
	msg, err := logger.formatter.format(rec)
	if err == nil {
		logger.addMessageToRingBuffer(msg)
		b.Write(msg)
	}
}

func (logger *Logger) log(level Level, args ...interface{}) {
	if int32(level) <= atomic.LoadInt32((*int32)(&logger.level)) {
		r := &record{
			level: level,
			time:  time.Now(),
			args:  args,
		}
		logger.queue <- *r
	}
}

func (logger *Logger) logf(level Level, format string, args ...interface{}) {
	if int32(level) <= atomic.LoadInt32((*int32)(&logger.level)) {
		r := &record{
			level:  level,
			time:   time.Now(),
			format: format,
			args:   args,
		}
		logger.queue <- *r
	}
}

//////////////////////////////////////////////////////////////////////////////

type rbuf struct {
	next  *rbuf
	bytes []byte
}

func (logger *Logger) addMessageToRingBuffer(msg []byte) {
	var size int32

	el := &rbuf{bytes: msg}
	elp := unsafe.Pointer(el)

	// add the record to the tail. If there is no records available, tail and head
	// will both be nil. When we succesfully set the tail and the previous value
	// was nil, it's safe to set the head to the current value too
	for {
		tailp := logger.inmem_tail
		swapped := atomic.CompareAndSwapPointer(&logger.inmem_tail, tailp, elp)
		if swapped == true {
			if tailp == nil {
				logger.inmem_head = elp
			} else {
				(*rbuf)(tailp).next = el
			}
			size = atomic.AddInt32(&logger.inmem_size, 1)
			break
		}
	}

	// since one record was added, we might have overflowed the ring buffer.
	// remove a record if that is the case.
	if logger.inmem_maxsize > 0 && size > logger.inmem_maxsize {
		for {
			headp := logger.inmem_head
			head := (*rbuf)(logger.inmem_head)
			if head.next == nil {
				break
			}
			swapped := atomic.CompareAndSwapPointer(&logger.inmem_head, headp, unsafe.Pointer(head.next))
			if swapped == true {
				atomic.AddInt32(&logger.inmem_size, -1)
				break
			}
		}
	}
}

func (logger *Logger) getRingBuffer() []byte {
	buffer := &bytes.Buffer{}

	el := (*rbuf)(logger.inmem_head)
	for {
		if el == nil {
			break
		}
		buffer.Write(el.bytes)
		el = el.next
	}
	return buffer.Bytes()
}

func (logger *Logger) resetRingBuffer() {
	if logger.inmem_size > 0 {
		atomic.StorePointer(&logger.inmem_head, unsafe.Pointer(nil))
		atomic.StorePointer(&logger.inmem_tail, unsafe.Pointer(nil))
		atomic.StoreInt32(&logger.inmem_size, 0)
	}
}

//////////////////////////////////////////////////////////////////////////////
// THE END
