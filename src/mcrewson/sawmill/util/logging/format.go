// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package logging

import (
	"bytes"
	"fmt"
	"regexp"
	"time"
)

//////////////////////////////////////////////////////////////////////////////

type fmtVerb int

const (
	fmtVerbTime fmtVerb = iota
	fmtVerbLevel
	fmtVerbMessage

	// keep last, there are matches for these below
	fmtVerbUnknown
	fmtVerbStatic
)

var fmtVerbs = []string{
	"time",
	"level",
	"message",
}

var defaultVerbsLayout = []string{
	"2006-01-02T15:04:05.999Z-0700",
	"s",
	"s",
}

func getFmtVerbByName(name string) fmtVerb {
	for i, verb := range fmtVerbs {
		if name == verb {
			return fmtVerb(i)
		}
	}
	return fmtVerbUnknown
}

//////////////////////////////////////////////////////////////////////////////

type formatPart struct {
	verb   fmtVerb
	layout string
}

type formatter struct {
	parts []formatPart
}

var formatRegex *regexp.Regexp = regexp.MustCompile(`%{([a-z]+)(?::(.*?[^\\]))?}`)

func createFormatter(format string) (*formatter, error) {
	var fmter = &formatter{}

	// find the boundaries of all %{vars}
	matches := formatRegex.FindAllStringSubmatchIndex(format, -1)
	if matches == nil {
		return nil, fmt.Errorf("logger: invalid log format: %s", format)
	}

	// collect all variables and static text for the format
	prev := 0
	for _, m := range matches {
		start, end := m[0], m[1]
		if start > prev {
			fmter.add(fmtVerbStatic, format[prev:start])
		}

		name := format[m[2]:m[3]]
		verb := getFmtVerbByName(name)
		if verb == fmtVerbUnknown {
			return nil, fmt.Errorf("logger: unknown variable: %s", name)
		}

		// handle the layout customizations or use the default. if this is not
		// for the time formatting, we need to prefix with %.
		layout := defaultVerbsLayout[verb]
		if m[4] != -1 {
			layout = format[m[4]:m[5]]
		}
		if verb != fmtVerbTime {
			layout = "%" + layout
		}

		fmter.add(verb, layout)
		prev = end
	}
	end := format[prev:]
	if end != "" {
		fmter.add(fmtVerbStatic, end)
	}

	// make a test run to make sure we can format it correctly
	t, err := time.Parse(time.RFC3339, "2014-12-25T21:00:57-08:00")
	if err != nil {
		panic(err)
	}
	r := &record{
		time:   t,
		format: "hello %s",
		args:   []interface{}{"world"},
	}
	if _, err := fmter.format(r); err != nil {
		return nil, err
	}

	return fmter, nil
}

func (fmter *formatter) add(verb fmtVerb, layout string) {
	fmter.parts = append(fmter.parts, formatPart{verb, layout})
}

func (fmter *formatter) format(rec *record) ([]byte, error) {
	output := &bytes.Buffer{}
	for _, part := range fmter.parts {
		if part.verb == fmtVerbStatic {
			output.Write([]byte(part.layout))
		} else if part.verb == fmtVerbTime {
			output.Write([]byte(rec.time.Format(part.layout)))
		} else {
			var v interface{}
			switch part.verb {
			case fmtVerbLevel:
				v = rec.level
			case fmtVerbMessage:
				v = rec.Message()
			default:
				panic("unhandled format part")
			}
			fmt.Fprintf(output, part.layout, v)
		}
	}
	return output.Bytes(), nil
}

//////////////////////////////////////////////////////////////////////////////
// THE END
