// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package grok

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"
)

//////////////////////////////////////////////////////////////////////////////

const PATTERN_REGEXP = `
%\{                                         # match '%{' not prefixed with '\'
  (?<name>                                  # match the pattern name
    (?<pattern>[A-z0-9]+)
    (?::(?<subname>[@\[\]A-z0-9_:.-]+))?
  )
  (?:=(?<definition>
    (?:
      (?:[^{}\\]+|\\.+)+
      |
      (?<curly>\{(?:(?>[^{}]+|(?>\\[{}])+)|(\g<curly>))*\})+
    )+
  ))?
  [^}]*
\}`

var PATTERN_REX = PcreMustCompile(PATTERN_REGEXP, PCRE_EXTENDED)

type Grok struct {
	pattern         string
	patterns        map[string]string
	capture_map     map[string]string
	regexp          PcreRegexp
	ExpandedPattern string
}

func NewGrok() *Grok {
	g := &Grok{
		patterns: make(map[string]string),
	}

	return g
}

func (g *Grok) AddPattern(name, pattern string) {
	g.patterns[name] = pattern
}

func (g *Grok) AddPatternsFromFile(path string) error {
	file, err := os.Open(path)
	defer file.Close()
	if err != nil {
		return err
	}
	reader := bufio.NewReader(file)
	for {
		line, lerr := reader.ReadString('\n')
		if lerr != nil {
			if lerr == io.EOF {
				break
			}
			return lerr
		}

		line = strings.TrimSpace(line)
		if line == "" || line[0] == '#' {
			continue
		}
		parts := strings.SplitN(line, " ", 2)
		if len(parts) == 2 {
			g.AddPattern(strings.TrimSpace(parts[0]), strings.TrimSpace(parts[1]))
		}
	}

	return nil
}

func (g *Grok) AddPatternsFromDirectory(path string) error {
	walker := func(pth string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		} else if info.Mode()&os.ModeType != 0 {
			// skip entries that are not regular files
			return nil
		} else if info.Name()[0] == '.' {
			// skip entries that begin with '.'
			return nil
		}
		return g.AddPatternsFromFile(pth)
	}

	info, err := os.Stat(path)
	if err != nil {
		if os.IsNotExist(err) {
			return nil
		}
		return err
	} else if info.Mode().IsDir() == false {
		return fmt.Errorf("expected a directory")
	}

	err = filepath.Walk(path, walker)
	return err
}

func (g *Grok) Compile(pattern string) error {
	g.capture_map = make(map[string]string)
	g.pattern = pattern
	exp_pattern := pattern
	index := 0

	for {
		m := PATTERN_REX.MatcherString(exp_pattern, 0)
		if !m.Matches() {
			break
		}

		definition := m.NamedString("definition")
		if definition != "" {
			g.AddPattern(m.NamedString("pattern"), definition)
		}

		pattern_val := m.NamedString("pattern")
		regex, ok := g.patterns[pattern_val]
		if !ok {
			regex, ok = DEFAULT_PATTERNS[pattern_val]
		}
		if ok {
			capture := fmt.Sprintf("a%d", index)
			replacement_pattern := fmt.Sprintf("(?<%s>%s)", capture, regex)
			g.capture_map[capture] = m.NamedString("name")

			exp_pattern = strings.Replace(exp_pattern, m.GroupString(0), replacement_pattern, 1)
			index += 1
		} else {
			return fmt.Errorf("pattern %s not defined", m.GroupString(0))
		}
	}

	g.regexp = PcreMustCompile(exp_pattern, 0)
	g.ExpandedPattern = exp_pattern
	return nil
}

func (g *Grok) Match(text string) *GrokMatch {
	matcher := g.regexp.MatcherString(text, 0)
	if matcher.Matches() {
		grokmatch := &GrokMatch{
			subject: text,
			grok:    g,
			names:   matcher.Names(),
			matcher: matcher,
		}
		return grokmatch
	} else {
		return nil
	}
}

//////////////////////////////////////////////////////////////////////////////

type GrokMatch struct {
	subject string
	grok    *Grok
	names   []string
	matcher *PcreMatcher
}

func (grokmatch *GrokMatch) Captures() map[string]string {
	captures := make(map[string]string)
	for _, name := range grokmatch.names {
		value := grokmatch.matcher.NamedString(name)
		captures[grokmatch.grok.capture_map[name]] = value
	}
	return captures
}

func (grokmatch *GrokMatch) ForEachCapture(handler func(string, string, interface{}), extradata interface{}) {
	for _, name := range grokmatch.names {
		value := grokmatch.matcher.NamedString(name)
		handler(grokmatch.grok.capture_map[name], value, extradata)
	}
}

//////////////////////////////////////////////////////////////////////////////

var DEFAULT_PATTERNS = map[string]string{
	"USERNAME":    `[a-zA-Z0-9._-]+`,
	"USER":        `%{USERNAME}`,
	"INT":         `(?:[+-]?(?:[0-9]+))`,
	"BASE10NUM":   `(?<![0-9.+-])(?>[+-]?(?:(?:[0-9]+(?:\.[0-9]+)?)|(?:\.[0-9]+)))`,
	"NUMBER":      `(?:%{BASE10NUM})`,
	"BASE16NUM":   `(?<![0-9A-Fa-f])(?:[+-]?(?:0x)?(?:[0-9A-Fa-f]+))`,
	"BASE16FLOAT": `\b(?<![0-9A-Fa-f.])(?:[+-]?(?:0x)?(?:(?:[0-9A-Fa-f]+(?:\.[0-9A-Fa-f]*)?)|(?:\.[0-9A-Fa-f]+)))\b`,

	"POSINT":       `\b(?:[1-9][0-9]*)\b`,
	"NONNEGINT":    `\b(?:[0-9]+)\b`,
	"WORD":         `\b\w+\b`,
	"NOTSPACE":     `\S+`,
	"SPACE":        `\s*`,
	"DATA":         `.*?`,
	"GREEDYDATA":   `.*`,
	"QUOTEDSTRING": `(?>(?<!\\)(?>"(?>\\.|[^\\"]+)+"|""|(?>'(?>\\.|[^\\']+)+')|''|(?>` + "`" + `(?>\\.|[^\\` + "`]+)+`)|``))",
	"UUID":         `[A-Fa-f0-9]{8}-(?:[A-Fa-f0-9]{4}-){3}[A-Fa-f0-9]{12}`,

	// Networking
	"MAC":        `(?:%{CISCOMAC}|%{WINDOWSMAC}|%{COMMONMAC})`,
	"CISCOMAC":   `(?:(?:[A-Fa-f0-9]{4}\.){2}[A-Fa-f0-9]{4})`,
	"WINDOWSMAC": `(?:(?:[A-Fa-f0-9]{2}-){5}[A-Fa-f0-9]{2})`,
	"COMMONMAC":  `(?:(?:[A-Fa-f0-9]{2}:){5}[A-Fa-f0-9]{2})`,
	"IPV6":       `((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?`,
	"IPV4":       `(?<![0-9])(?:(?:25[0-5]|2[0-4][0-9]|[0-1]?[0-9]{1,2})[.](?:25[0-5]|2[0-4][0-9]|[0-1]?[0-9]{1,2})[.](?:25[0-5]|2[0-4][0-9]|[0-1]?[0-9]{1,2})[.](?:25[0-5]|2[0-4][0-9]|[0-1]?[0-9]{1,2}))(?![0-9])`,
	"IP":         `(?:%{IPV6}|%{IPV4})`,
	"HOSTNAME":   `\b(?:[0-9A-Za-z][0-9A-Za-z-]{0,62})(?:\.(?:[0-9A-Za-z][0-9A-Za-z-]{0,62}))*(\.?|\b)`,
	"HOST":       `%{HOSTNAME}`,
	"IPORHOST":   `(?:%{HOSTNAME}|%{IP})`,
	"HOSTPORT":   `%{IPORHOST}:%{POSINT}`,

	// paths
	"PATH":     `(?:%{UNIXPATH}|%{WINPATH})`,
	"UNIXPATH": `(?>/(?>[\w_%!$@:.,-]+|\\.)*)+`,
	"TTY":      `(?:/dev/(pts|tty([pq])?)(\w+)?/?(?:[0-9]+))`,
	"WINPATH":  `(?>[A-Za-z]+:|\\)(?:\\[^\\?*]*)+`,
	"URIPROTO": `[A-Za-z]+(\+[A-Za-z+]+)?`,
	"URIHOST":  `%{IPORHOST}(?::%{POSINT:port})?`,
	// uripath come loosely from RFC1738, but mostly from what Firefox
	// doesn't turn into %XX
	"URIPATH":      `(?:/[A-Za-z0-9$.+!*'(){},~:;=@#%_\-]*)+`,
	"URIPARAM":     `\?[A-Za-z0-9$.+!*'|(){},~@#%&/=:;_?\-\[\]]*`,
	"URIPATHPARAM": `%{URIPATH}(?:%{URIPARAM})?`,
	"URI":          `%{URIPROTO}://(?:%{USER}(?::[^@]*)?@)?(?:%{URIHOST})?(?:%{URIPATHPARAM})?`,

	// Months: January, Feb, 3, 03, 12, December
	"MONTH":     `\b(?:Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?)\b`,
	"MONTHNUM":  `(?:0?[1-9]|1[0-2])`,
	"MONTHNUM2": `(?:0[1-9]|1[0-2])`,
	"MONTHDAY":  `(?:(?:0[1-9])|(?:[12][0-9])|(?:3[01])|[1-9])`,

	// Days: Monday, Tue, Thu, etc...
	"DAY": `(?:Mon(?:day)?|Tue(?:sday)?|Wed(?:nesday)?|Thu(?:rsday)?|Fri(?:day)?|Sat(?:urday)?|Sun(?:day)?)`,

	// Years?
	"YEAR": `(?>\d\d){1,2}`,

	// Hour, minute second
	"HOUR":   `(?:2[0123]|[01]?[0-9])`,
	"MINUTE": `(?:[0-5][0-9])`,
	// '60' is a leap second in most time standards and thus is valid.
	"SECOND": `(?:(?:[0-5]?[0-9]|60)(?:[:.,][0-9]+)?)`,

	"TIME": `(?!<[0-9])%{HOUR}:%{MINUTE}(?::%{SECOND})(?![0-9])`,

	// datestamp is YYYY/MM/DD-HH:MM:SS.UUUU (or something like it)
	"DATE_US":            `%{MONTHNUM}[/-]%{MONTHDAY}[/-]%{YEAR}`,
	"DATE_EU":            `%{MONTHDAY}[./-]%{MONTHNUM}[./-]%{YEAR}`,
	"ISO8601_TIMEZONE":   `(?:Z|[+-]%{HOUR}(?::?%{MINUTE}))`,
	"ISO8601_SECOND":     `(?:%{SECOND}|60)`,
	"TIMESTAMP_ISO8601":  `%{YEAR}-%{MONTHNUM}-%{MONTHDAY}[T ]%{HOUR}:?%{MINUTE}(?::?%{SECOND})?%{ISO8601_TIMEZONE}?`,
	"DATE":               `%{DATE_US}|%{DATE_EU}`,
	"DATESTAMP":          `%{DATE}[- ]%{TIME}`,
	"TZ":                 `(?:[PMCE][SD]T|UTC)`,
	"DATESTAMP_RFC822":   `%{DAY} %{MONTH} %{MONTHDAY} %{YEAR} %{TIME} %{TZ}`,
	"DATESTAMP_RFC2822":  `%{DAY}, %{MONTHDAY} %{MONTH} %{YEAR} %{TIME} %{ISO8601_TIMEZONE}`,
	"DATESTAMP_OTHER":    `%{DAY} %{MONTH} %{MONTHDAY} %{TIME} %{TZ} %{YEAR}`,
	"DATESTAMP_EVENTLOG": `%{YEAR}%{MONTHNUM2}%{MONTHDAY}%{HOUR}%{MINUTE}%{SECOND}`,
}

//////////////////////////////////////////////////////////////////////////////
// THE END
