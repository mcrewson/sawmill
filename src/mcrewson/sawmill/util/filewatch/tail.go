// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package filewatch

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	"mcrewson/sawmill/util"
)

//////////////////////////////////////////////////////////////////////////////

const (
	FILEWATCH_START_NEW_FILES_AT_BEGINNING = 1
	FILEWATCH_START_NEW_FILES_AT_END       = 2
)

type TailCallback func(path, line string)

type TailConfig struct {
	StartNewFilesAt      int
	FileStatInterval     time.Duration
	FileDiscoverInterval int
	SinceDBPath          string
	SinceDBWriteInterval time.Duration
}

type Tail struct {
	Config *TailConfig

	watch              *Watch
	files              map[string]*os.File
	statcache          map[string]fileDescriptor
	sincedb            map[fileDescriptor]int64
	sincedb_last_write time.Time
	quitting           bool
}

var default_config = &TailConfig{
	StartNewFilesAt:      FILEWATCH_START_NEW_FILES_AT_END,
	FileStatInterval:     1 * time.Second,
	FileDiscoverInterval: 5,
	SinceDBWriteInterval: 10 * time.Second,
}

//////////////////////////////////////////////////////////////////////////////

func NewTail(config *TailConfig) *Tail {
	if err := util.MergeStruct(config, *default_config); err != nil {
		panic(err)
	}

	t := &Tail{Config: config}
	t.watch = NewWatch()
	t.files = make(map[string]*os.File)
	t.statcache = make(map[string]fileDescriptor)
	t.sincedb = make(map[fileDescriptor]int64)
	t.quitting = false
	t.sincedb_open()

	return t
}

func (t *Tail) Tail(path string) {
	t.watch.Watch(path)
}

func (t *Tail) Subscribe(tailcb TailCallback) {
	watchcb := func(event int, path string) {
		if t.quitting {
			return
		}
		switch event {
		case FILEWATCH_CREATE, FILEWATCH_CREATE_INITIAL:
			for fpath := range t.files {
				if fpath == path {
					// file already exists in t.files
					return
				}
			}
			if t.open_file(path, event) {
				t.read_file(path, tailcb)
			}

		case FILEWATCH_MODIFY:
			found := false
			for fpath := range t.files {
				if fpath == path {
					found = true
					break
				}
			}
			if !found {
				if t.open_file(path, event) {
					t.read_file(path, tailcb)
				}
			} else {
				t.read_file(path, tailcb)
			}

		case FILEWATCH_DELETE:
			t.read_file(path, tailcb)
			t.files[path].Close()
			delete(t.files, path)
			delete(t.sincedb, t.statcache[path])
			delete(t.statcache, path)

		default:
			// Unknown WatchType
		}

	}
	t.watch.Subscribe(t.Config.FileStatInterval, t.Config.FileDiscoverInterval, watchcb)
}

func (t *Tail) Quit() {
	t.quitting = true
	t.watch.Quit()
}

func (t *Tail) open_file(path string, event int) bool {
	file, err := os.Open(path)
	if err != nil {
		// failed to open
		delete(t.files, path)
		return false
	}
	t.files[path] = file

	info, err := os.Stat(path)
	if err != nil {
		t.files[path].Close()
		return false
	}

	size := info.Size()
	descriptor := stat2filedesc(info)

	t.statcache[path] = descriptor

	found := false
	for sincedesc := range t.sincedb {
		if sincedesc == descriptor {
			found = true
			break
		}
	}
	if found {
		last_size := t.sincedb[descriptor]
		if last_size <= size {
			// seek to last_size
			t.files[path].Seek(last_size, os.SEEK_SET)
		} else {
			// last_size is greaterthan current value, start over
			t.sincedb[descriptor] = 0
		}
	} else if event == FILEWATCH_CREATE_INITIAL {
		// allow starting at beginning
		if t.Config.StartNewFilesAt == FILEWATCH_START_NEW_FILES_AT_BEGINNING {
			t.files[path].Seek(0, os.SEEK_SET)
			t.sincedb[descriptor] = 0
		} else {
			t.files[path].Seek(size, os.SEEK_SET)
			t.sincedb[descriptor] = size
		}

	} else {
		// staying at position 0, no sincedb
	}

	return true
}

func (t *Tail) read_file(path string, tailcb TailCallback) {
	changed := false
	reader := bufio.NewReaderSize(t.files[path], 16<<10)

read_runloop:
	for {
		if t.quitting == true {
			break read_runloop
		}

		data, err := reader.ReadSlice('\n')
		if err != nil {
			break read_runloop
		}

		changed = true
		tailcb(path, string(data))
		t.sincedb[t.statcache[path]] += int64(len(data))
	}

	if changed {
		delta := time.Since(t.sincedb_last_write)
		if delta >= t.Config.SinceDBWriteInterval || t.quitting == true {
			t.sincedb_write()
			t.sincedb_last_write = time.Now()
		}
	}
}

func (t *Tail) sincedb_open() {
	if len(t.Config.SinceDBPath) == 0 {
		// no SinceDBPath defined. skip it
		return
	}

	db, err := os.Open(t.Config.SinceDBPath)
	defer db.Close()
	if err != nil {
		// failed to open
		return
	}
	reader := bufio.NewReader(db)
	for {
		line, err := reader.ReadString('\n')
		if err != nil {
			break
		}

		parts := strings.SplitN(strings.TrimSpace(line), " ", 3)
		inode, _ := strconv.ParseUint(parts[0], 10, 64)
		device, _ := strconv.ParseUint(parts[1], 10, 64)
		offset, _ := strconv.ParseInt(parts[2], 10, 64)
		descriptor := fileDescriptor{inode: inode, device: device}
		t.sincedb[descriptor] = offset
	}
}

func (t *Tail) sincedb_write() {
	if len(t.Config.SinceDBPath) == 0 {
		// no SinceDBPath defined. skip it
		return
	}

	tmp := t.Config.SinceDBPath + ".new"
	db, err := os.Create(tmp)
	defer db.Close()
	if err != nil {
		// failed to create
		return
	}

	for descriptor := range t.sincedb {
		db.WriteString(fmt.Sprintf("%d %d %d\n", descriptor.inode, descriptor.device, t.sincedb[descriptor]))
	}

	os.Rename(tmp, t.Config.SinceDBPath)
}

//////////////////////////////////////////////////////////////////////////////
// THE END
