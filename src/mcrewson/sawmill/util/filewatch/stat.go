// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package filewatch

import (
	"os"
	"syscall"
)

//////////////////////////////////////////////////////////////////////////////

type fileDescriptor struct {
	inode  uint64
	device uint64
}

func stat2inode(info os.FileInfo) (inode, device uint64) {
	fstat := info.Sys().(*syscall.Stat_t)
	inode = fstat.Ino
	device = fstat.Dev
	return
}

func stat2filedesc(info os.FileInfo) fileDescriptor {
	inode, device := stat2inode(info)
	return fileDescriptor{inode: inode, device: device}
}

//////////////////////////////////////////////////////////////////////////////
