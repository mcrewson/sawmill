// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package filewatch

import (
	"os"
	"path"
	"path/filepath"
	"time"
)

//////////////////////////////////////////////////////////////////////////////

const (
	FILEWATCH_CREATE_INITIAL = iota
	FILEWATCH_CREATE
	FILEWATCH_MODIFY
	FILEWATCH_DELETE
)

type watchedfile struct {
	size        int64
	descriptor  fileDescriptor
	initial     bool
	create_sent bool
}

type Watch struct {
	watching   []string
	excludes   []string
	files      map[string]*watchedfile
	stopSignal chan bool
}

type WatchCallback func(watchtype int, path string)

//////////////////////////////////////////////////////////////////////////////

func NewWatch() *Watch {
	watching := make([]string, 0)
	excludes := make([]string, 0)
	files := make(map[string]*watchedfile)
	stop := make(chan bool, 0)
	w := &Watch{
		watching:   watching,
		excludes:   excludes,
		files:      files,
		stopSignal: stop,
	}
	return w
}

func (w *Watch) Exclude(expath string) {
	w.excludes = append(w.excludes, expath)
}

func (w *Watch) Watch(wpath string) {
	for _, p := range w.watching {
		if wpath == p {
			return
		}
	}

	w.watching = append(w.watching, wpath)
	w.discover_file(wpath, true)
}

func (w *Watch) Subscribe(stat_interval time.Duration, discover_interval int, watchcb WatchCallback) {
	go func() {
		glob := 0
	watch_runloop:
		for {
			select {
			case <-w.stopSignal:
				break watch_runloop
			default:
				w.scan(watchcb)
				glob += 1
				if glob == discover_interval {
					w.discover()
					glob = 0
				}
				time.Sleep(stat_interval)
			}
		}
	}()
}

func (w *Watch) Quit() {
	w.stopSignal <- true
}

func (w *Watch) scan(watchcb WatchCallback) {
	for fpath, _ := range w.files {
		if w.files[fpath].create_sent == false {
			if w.files[fpath].initial == true {
				watchcb(FILEWATCH_CREATE_INITIAL, fpath)
			} else {
				watchcb(FILEWATCH_CREATE, fpath)
			}
			w.files[fpath].create_sent = true
		}
	}

	for fpath, _ := range w.files {
		info, err := os.Stat(fpath)
		if err != nil {
			// file has gone away or we can't ready it anymore
			delete(w.files, fpath)
			watchcb(FILEWATCH_DELETE, fpath)
			continue
		}

		descriptor := stat2filedesc(info)
		size := info.Size()
		if descriptor != w.files[fpath].descriptor {
			// inode has changed!
			watchcb(FILEWATCH_DELETE, fpath)
			watchcb(FILEWATCH_CREATE, fpath)
		} else if size < w.files[fpath].size {
			// file rolled
			watchcb(FILEWATCH_DELETE, fpath)
			watchcb(FILEWATCH_CREATE, fpath)
		} else if size > w.files[fpath].size {
			// file grew
			watchcb(FILEWATCH_MODIFY, fpath)
		}

		w.files[fpath].size = size
		w.files[fpath].descriptor = descriptor
	}
}

func (w *Watch) discover() {
	for _, wpath := range w.watching {
		w.discover_file(wpath, false)
	}
}

func (w *Watch) discover_file(wpath string, initial bool) {
	globs, err := filepath.Glob(wpath)
	if err != nil || globs == nil {
		return
	}

discover_loop:
	for _, file := range globs {
		for f := range w.files {
			if file == f {
				continue discover_loop
			}
		}

		info, err := os.Stat(file)
		if err != nil || (info.Mode()&os.ModeType) != 0 {
			// skip non-regular files (such as directories, symlinks, sockets, named pipes, etc)
			continue discover_loop
		}

		for _, exclude := range w.excludes {
			matched, err := path.Match(exclude, file)
			if err != nil || matched == true {
				continue discover_loop
			}
		}

		descriptor := stat2filedesc(info)
		wfile := &watchedfile{
			size:        0,
			descriptor:  descriptor,
			initial:     initial,
			create_sent: false,
		}

		w.files[file] = wfile
	}
}

//////////////////////////////////////////////////////////////////////////////
// THE END
