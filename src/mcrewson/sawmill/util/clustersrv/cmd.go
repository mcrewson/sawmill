// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package clustersrv

import (
	"mcrewson/sawmill/util"
	"mcrewson/sawmill/util/logging"
	"bytes"
	stdlog "log"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func Commandline() {
	stop := false

	logger, lerr := logging.GetLogger()
	if lerr != nil {
		stdlog.Fatalf("Cannot create internal loggers: %s\n", lerr)
	}

	side := os.Args[1]
	if side != "sender" && side != "receiver" {
		stdlog.Fatalf("Arg[1] must be either 'sender' or 'reciever'\n")
	}

	args, argsErr := util.ArgumentsToMap(os.Args[2:])
	if argsErr != nil {
		logger.Fatal(argsErr)
	}

	termChannel := make(chan os.Signal, 2)
	go func() {
		<-termChannel
		stop = true
	}()
	signal.Notify(termChannel, syscall.SIGTERM, syscall.SIGINT)

	switch side {
	case "sender":
		sender, err := NewClusterSender(args, logger)
		if err != nil {
			logger.Fatal(err)
		}
		if err := sender.Start(); err != nil {
			logger.Fatal(err)
		}

		var data bytes.Buffer
		for data.Len() < 1024*69 {
			data.WriteString("hello. My name is Mark Crewson. I live here.")
		}

		for !stop {
			err := sender.SendData(data.Bytes())
			if err != nil {
				logger.Error(err)
			}
			time.Sleep(time.Duration(2000) * time.Millisecond)
		}
		sender.Stop()

	case "receiver":
		dataChan := make(chan []byte, 0)
		receiver, err := NewClusterReceiver(args, dataChan, logger)
		if err != nil {
			logger.Fatal(err)
		}
		if err := receiver.Start(); err != nil {
			logger.Fatal(err)
		}

		for !stop {
			select {
			case data := <-dataChan:
				logger.Debugf("Received a data frame (%d bytes)\n", len(data))

			default:
				time.Sleep(time.Duration(50) * time.Millisecond)
			}
		}
		receiver.Stop()

	}
	logger.Info("shutdown.\n")
}

//////////////////////////////////////////////////////////////////////////////
// THE END
