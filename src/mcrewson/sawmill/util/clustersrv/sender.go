// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package clustersrv

import (
	"encoding/binary"
	"fmt"
	"math/rand"
	"net"
	"strconv"
	"strings"
	"time"

	"mcrewson/sawmill/util"
	"mcrewson/sawmill/util/logging"
)

//////////////////////////////////////////////////////////////////////////////

type ClusterSender interface {
	Start() error
	Stop() error
	SendData([]byte) error
}

type ClusterSenderImpl struct {
	Servers           []string
	Timeouts          map[string]time.Duration
	HeartbeatInterval time.Duration
	PhiThreshold      float64
	HardTimeout       time.Duration
	RecoverSampleSize uint32

	nodes          []*ClusterNode
	weighted_nodes []*ClusterNode
	rr             int

	stopSignal chan bool

	Logger *logging.Logger
}

func NewClusterSender(args map[string]string, logger *logging.Logger) (ClusterSender, error) {
	heartbeat_interval := default_heartbeatinterval
	phi_threshold := default_phithreshold
	recover_wait := default_recover_wait

	var err error

	val, ok := args["servers"]
	if !ok {
		val, ok = args["server"]
		if !ok {
			return nil, fmt.Errorf("no servers defined for this cluster sender")
		}
	}
	servers := strings.Split(val, ",")
	for i := range servers {
		servers[i] = strings.TrimSpace(servers[i])
	}

	timeouts, err := util.ParseNetworkTimeouts(args, default_timeouts)
	if err != nil {
		return nil, err
	}

	val, ok = args["heartbeatinterval"]
	if ok {
		heartbeat_interval, err = util.ParseDuration(val)
		if err != nil {
			return nil, fmt.Errorf("invalid heartbeatinterval parameter: %s", err)
		}
	}

	val, ok = args["recoverwait"]
	if ok {
		recover_wait, err = util.ParseDuration(val)
		if err != nil {
			return nil, fmt.Errorf("invalid recoverwait paramter: %s", err)
		}
	}
	recover_sample_size := uint32(recover_wait.Seconds() / heartbeat_interval.Seconds())

	val, ok = args["phithreshold"]
	if ok {
		f, err := strconv.ParseFloat(val, 64)
		if err != nil {
			return nil, fmt.Errorf("phithreshold must be a positive number")
		}
		phi_threshold = f
	}

	var hard_timeout time.Duration
	val, ok = args["hardtimeout"]
	if ok {
		hard_timeout, err = util.ParseDuration(val)
		if err != nil {
			return nil, fmt.Errorf("invalid hardtimeout parameter: %s", err)
		}
	} else {
		//hard_timeout = timeouts["writetimeout"]
		hard_timeout = time.Duration(60) * time.Second
	}

	sender := &ClusterSenderImpl{
		Servers:           servers,
		Timeouts:          timeouts,
		HeartbeatInterval: heartbeat_interval,
		PhiThreshold:      phi_threshold,
		HardTimeout:       hard_timeout,
		RecoverSampleSize: recover_sample_size,
		nodes:             make([]*ClusterNode, 0),
		stopSignal:        make(chan bool, 0),
		Logger:            logger,
	}

	for _, server := range servers {
		failure := NewFailureDetector(heartbeat_interval, hard_timeout, time.Now())
		node, nodeErr := NewClusterNode(server, failure, sender)
		if nodeErr != nil {
			return nil, fmt.Errorf("invalid node (%s): %s", server, nodeErr)
		}
		sender.nodes = append(sender.nodes, node)
	}

	return sender, nil
}

func (f *ClusterSenderImpl) String() string {
	return ""
}

func (f *ClusterSenderImpl) Start() error {
	rand.Seed(time.Now().Unix())
	f.rebuildWeightArray()

	addr, resolveErr := net.ResolveUDPAddr("udp4", ":0")
	if resolveErr != nil {
		return resolveErr
	}

	sock, listenErr := net.ListenUDP("udp4", addr)
	if listenErr != nil {
		return listenErr
	}
	go func() {
		f.Logger.Debugf("starting to listen for heartbeats on %s\n", sock.LocalAddr())
		var buf [16]byte
		for {
			_, remote, err := sock.ReadFromUDP(buf[:])
			f.Logger.Debugf("received heartbeat from %s (err=%s)\n", remote, err)
			if err != nil {
				continue
			}

			f._onHeartbeat(remote, buf[:])
		}
		f.Logger.Debug("heartbeat listener goroutine exited... (ok?)\n")
	}()

	go func() {
		ticker := time.NewTicker(f.HeartbeatInterval)
	tickerloop:
		for {
			select {
			case <-ticker.C:
				for _, node := range f.nodes {
					if node.Check() {
						f.rebuildWeightArray()
					}
					naddr, _ := net.ResolveUDPAddr("udp4", node.Address)
					f.Logger.Debugf("sending heartbeat request to %s\n", naddr)
					sock.WriteTo([]byte("\x00"), naddr)
				}
			case <-f.stopSignal:
				break tickerloop
			}
		}
		ticker.Stop()
	}()

	return nil
}

func (f *ClusterSenderImpl) Stop() error {
	f.stopSignal <- true
	return nil
}

func (f *ClusterSenderImpl) SendData(data []byte) error {
	if len(data) == 0 {
		return nil
	}

	var lastErr error
	wlen := len(f.weighted_nodes)
	for i := 0; i < wlen; i++ {
		f.rr = (f.rr + 1) % wlen
		node := f.weighted_nodes[f.rr]
		if node.IsAvailable() {
			lastErr = f._reallySendData(node, data)
			if lastErr == nil {
				return nil
			}
		}
	}

	if lastErr != nil {
		return lastErr
	}
	return fmt.Errorf("no nodes available")
}

func (f *ClusterSenderImpl) rebuildWeightArray() {
	standby_nodes := make([]*ClusterNode, 0)
	regular_nodes := make([]*ClusterNode, 0)
	for _, node := range f.nodes {
		if node.IsStandby() {
			standby_nodes = append(standby_nodes, node)
		} else {
			regular_nodes = append(regular_nodes, node)
		}
	}

	var lost_weight uint32 = 0
	for _, node := range regular_nodes {
		if !node.IsAvailable() {
			lost_weight += node.Weight
		}
	}

	if lost_weight > 0 {
		for _, node := range standby_nodes {
			if node.IsAvailable() {
				regular_nodes = append(regular_nodes, node)
				f.Logger.Warningf("using standby node %s\n", node.Name)
				lost_weight -= node.Weight
				if lost_weight <= 0 {
					break
				}
			}
		}
	}

	weight_array := make([]*ClusterNode, 0)

	var gcd uint32 = 0
	for _, node := range regular_nodes {
		// calc the gcd of the previous node's weight and this node's weight
		a := gcd
		b := node.Weight
		for b != 0 {
			a, b = b, a%b
		}
		gcd = a
	}
	for _, node := range regular_nodes {
		for i := node.Weight / gcd; i > 0; i-- {
			weight_array = append(weight_array, node)
		}
	}

	// for load balancing during detecting crashed servers
	coe := (len(regular_nodes) * 6) / len(weight_array)
	if coe > 1 {
		new_weight_array := make([]*ClusterNode, len(weight_array)*coe)
		for i := 0; i < coe; i++ {
			copy(new_weight_array[i*len(weight_array):], weight_array)
		}
		weight_array = new_weight_array
	}

	// randomize the weight array
	for i := range weight_array {
		j := rand.Intn(i + 1)
		weight_array[i], weight_array[j] = weight_array[j], weight_array[i]
	}

	f.weighted_nodes = weight_array
}

func (f *ClusterSenderImpl) _onHeartbeat(addr *net.UDPAddr, msg []byte) {
	for _, node := range f.nodes {
		if node.resolved_ip.Equal(addr.IP) && node.resolved_port == addr.Port {
			if node.Heartbeat(true) {
				f.rebuildWeightArray()
			}
			break
		}
	}
}

func (f *ClusterSenderImpl) _reallySendData(node *ClusterNode, data []byte) error {
	conn, connErr := net.DialTimeout("tcp", node.Address, f.Timeouts["connect"])
	if connErr != nil {
		return connErr
	}
	defer conn.Close()

	deadlineErr := conn.SetWriteDeadline(time.Now().Add(f.Timeouts["write"]))
	if deadlineErr != nil {
		return deadlineErr
	}

	size := uint64(len(data))
	if writeErr := binary.Write(conn, binary.LittleEndian, size); writeErr != nil {
		return writeErr
	}
	for size > 0 {
		n, err := conn.Write(data)
		if err != nil {
			return err
		}
		size -= uint64(n)
	}

	node.Heartbeat(false)
	return nil
}

//////////////////////////////////////////////////////////////////////////////
// THE END
