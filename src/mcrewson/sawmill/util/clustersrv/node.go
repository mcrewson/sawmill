// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package clustersrv

import (
	"fmt"
	"math"
	"net"
	"net/url"
	"strconv"
	"time"
)

//////////////////////////////////////////////////////////////////////////////

type ClusterNode struct {
	Name   string
	Weight uint32

	available     bool
	Address       string
	resolved_ip   net.IP
	resolved_port int
	failure       *FailureDetector
	sender        *ClusterSenderImpl
}

var default_forwardernode_scheme string = "tcp"
var default_forwardernode_weight uint32 = 60

func NewClusterNode(address string, failure *FailureDetector, out *ClusterSenderImpl) (*ClusterNode, error) {
	parsed_uri, err := url.Parse(address)
	if err != nil {
		return nil, err
	}

	if parsed_uri.Scheme != "" && parsed_uri.Scheme != default_forwardernode_scheme {
		return nil, fmt.Errorf("invalid protocol: %s", parsed_uri.Scheme)
	}

	if parsed_uri.Host == "" {
		return nil, fmt.Errorf("missing address <host>[:<port>]")
	}

	query := parsed_uri.Query()

	var weight uint32
	if query.Get("weight") == "" {
		weight = default_forwardernode_weight
	} else {
		i, weightErr := strconv.ParseUint(query.Get("weight"), 10, 32)
		if weightErr != nil {
			return nil, fmt.Errorf("invalid weight parameter: %s", weightErr)
		}
		weight = uint32(i)
	}

	name := query.Get("name")
	if name == "" {
		name = address
	}

	addr, tcpErr := net.ResolveTCPAddr("tcp", parsed_uri.Host)
	if tcpErr != nil {
		return nil, tcpErr
	}

	n := &ClusterNode{
		Name:          name,
		Weight:        weight,
		Address:       parsed_uri.Host,
		resolved_ip:   addr.IP,
		resolved_port: addr.Port,
		available:     true,
		failure:       failure,
		sender:        out,
	}

	return n, nil
}

func (n *ClusterNode) String() string {
	return fmt.Sprintf("ClusterNode(name=%s, address=%s, weight=%d, available=%t)", n.Name, n.Address, n.Weight, n.available)
}

func (n *ClusterNode) IsAvailable() bool {
	return n.available
}

func (n *ClusterNode) Disable() {
	n.available = false
}

func (n *ClusterNode) IsStandby() bool {
	// TODO
	return false
}

func (n *ClusterNode) Check() bool {
	now := time.Now()
	if !n.available {
		if n.failure.IsHardTimeout(now) {
			n.failure.Clear()
		}
		return false
	}

	if n.failure.IsHardTimeout(now) {
		n.sender.Logger.Warningf("detaching cluster node '%s' (address=%s). hardtimeout=true\n", n.Name, n.Address)
		n.available = false
		n.failure.Clear()
		return true
	}

	phi := n.failure.Phi(now)
	if phi > n.sender.PhiThreshold {
		n.sender.Logger.Warningf("detaching cluster node '%s' (address=%s). phi=%0.3f\n", n.Name, n.Address, phi)
		n.available = false
		n.failure.Clear()
		return true
	}

	return false
}

func (n *ClusterNode) Heartbeat(detect bool) bool {
	now := time.Now()
	n.failure.Add(now)
	if detect && !n.available && n.failure.SampleSize() > n.sender.RecoverSampleSize {
		n.available = true
		n.sender.Logger.Warningf("recovered cluster node '%s' (address=%s)\n", n.Name, n.Address)
		return true
	} else {
		return false
	}
}

//////////////////////////////////////////////////////////////////////////////

type FailureDetector struct {
	heartbeatInterval time.Duration
	hardTimeout       time.Duration
	last              time.Time
	initGap           int64
	window            []int64
}

const FAILUREDETECTOR_SAMPLE_SIZE = 1000

var FAILUREDETECTOR_PHI_FACTOR = 1.0 / math.Log(10.0)

func NewFailureDetector(interval, hard time.Duration, initLast time.Time) *FailureDetector {
	fd := &FailureDetector{
		heartbeatInterval: interval,
		hardTimeout:       hard,
		last:              initLast,
		initGap:           interval.Nanoseconds(),
		window:            make([]int64, 0),
	}

	fd.window = append(fd.window, fd.initGap)
	return fd
}

func (fd *FailureDetector) IsHardTimeout(now time.Time) bool {
	if now.Sub(fd.last) > fd.hardTimeout {
		return true
	}
	return false
}

func (fd *FailureDetector) Add(now time.Time) {
	if len(fd.window) == 0 {
		fd.window = append(fd.window, fd.initGap)
		fd.last = now
	} else {
		gap := now.Sub(fd.last)
		fd.window = append(fd.window, gap.Nanoseconds())
		if len(fd.window) > FAILUREDETECTOR_SAMPLE_SIZE {
			copy(fd.window, fd.window[1:])
		}
		fd.last = now
	}
}

func (fd *FailureDetector) Phi(now time.Time) float64 {
	size := len(fd.window)
	if size == 0 {
		return 0.0
	}

	// calculate a crude weighted moving average
	var mean_nsec int64 = 0
	var fact int64 = 0
	for i, gap := range fd.window {
		mean_nsec += gap * int64(i+1)
		fact += int64(1 + i)
	}
	mean_nsec = mean_nsec / fact

	// normalize arrive intervals into 1sec
	mean := float64(mean_nsec)/1e9 - fd.heartbeatInterval.Seconds() + 1

	// calculate phi of the phi accural failure detector
	t := now.Sub(fd.last) - fd.heartbeatInterval + 1
	phi := FAILUREDETECTOR_PHI_FACTOR * t.Seconds() / mean

	return phi
}

func (fd *FailureDetector) SampleSize() uint32 {
	return uint32(len(fd.window))
}

func (fd *FailureDetector) Clear() {
	fd.window = make([]int64, 0)
	fd.last = time.Now()
}

//////////////////////////////////////////////////////////////////////////////
// THE END
