// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package clustersrv

import (
	"encoding/binary"
	"fmt"
	"net"
	"strconv"
	"time"

	"mcrewson/sawmill/util"
	"mcrewson/sawmill/util/logging"
)

//////////////////////////////////////////////////////////////////////////////

type ClusterReceiver interface {
	Start() error
	Stop() error
}

type ClusterReceiverImpl struct {
	listenAddress  string
	maxConnections uint32
	timeouts       map[string]time.Duration
	channel        chan []byte
	logger         *logging.Logger
}

func NewClusterReceiver(args map[string]string, channel chan []byte, logger *logging.Logger) (ClusterReceiver, error) {
	listen_address := default_listen_address
	val, ok := args["listenaddress"]
	if ok {
		listen_address = val
	}
	if _, err := net.ResolveTCPAddr("tcp", listen_address); err != nil {
		return nil, fmt.Errorf("invalid listen address: %s\n", listen_address)
	}

	max_connections := default_max_connections
	val, ok = args["maxconnections"]
	if ok {
		i, err := strconv.ParseUint(val, 10, 32)
		if err != nil {
			return nil, fmt.Errorf("maxconnections must be a positive integer")
		}
		max_connections = uint32(i)
	}

	timeouts, err := util.ParseNetworkTimeouts(args, default_timeouts)
	if err != nil {
		return nil, err
	}

	receiver := &ClusterReceiverImpl{
		listenAddress:  listen_address,
		maxConnections: max_connections,
		timeouts:       timeouts,
		channel:        channel,
		logger:         logger,
	}

	return receiver, nil
}

func (f *ClusterReceiverImpl) String() string {
	return ""
}

func (f *ClusterReceiverImpl) Start() error {

	// Heartbeat Request responder
	if err := f._heartbeat(); err != nil {
		return err
	}

	// Data Receiver
	if err := f._dataReader(); err != nil {
		return err
	}
	return nil
}

func (f *ClusterReceiverImpl) Stop() error {
	return nil
}

func (f *ClusterReceiverImpl) _heartbeat() error {
	addr, resolveErr := net.ResolveUDPAddr("udp4", f.listenAddress)
	if resolveErr != nil {
		return resolveErr
	}
	conn, heartbeatErr := net.ListenUDP("udp4", addr)
	if heartbeatErr != nil {
		return heartbeatErr
	}
	go func() {
		var buf [16]byte
		for {
			if _, remote, err := conn.ReadFromUDP(buf[:]); err == nil {
				f.logger.Debugf("Received heartbeat request from %s (err=%s)\n", remote, err)
				if _, wErr := conn.WriteTo([]byte("\x00"), remote); wErr != nil {
					f.logger.Debugf("failed to write! err = %s\n", wErr)
				}
			}
		}
	}()
	return nil
}

func (f *ClusterReceiverImpl) _dataReader() error {
	addr, resolveErr := net.ResolveTCPAddr("tcp", f.listenAddress)
	if resolveErr != nil {
		return resolveErr
	}
	conn, dataErr := net.ListenTCP("tcp", addr)
	if dataErr != nil {
		return dataErr
	}

	go func(sock *net.TCPListener) {
		var sem = make(chan bool, f.maxConnections)

		for {
			con, err := sock.AcceptTCP()
			if err != nil {
				f.logger.Warningf("failed to accept connection: %s\n", err)
				continue
			}

			// push a value onto the sem channel. Block until a slot is available
			// in the channel. This effectively limits the number of the following
			// goroutines to the buffered capacity of the channel
			sem <- true

			go func(c *net.TCPConn) {
				f.logger.Debugf("Accepted conncetion from %s\n", con.RemoteAddr())
				c.SetReadDeadline(time.Now().Add(f.timeouts["read"]))

				var size uint64
				if err := binary.Read(c, binary.LittleEndian, &size); err != nil {
					f.logger.Warningf("failed to read size of the frame: %s\n", err)
					c.Close()
					<-sem
					return
				}

				buffer := make([]byte, size)
				pos := 0
				for size > 0 {
					n, err := c.Read(buffer[pos:])
					if err != nil {
						f.logger.Warningf("failed to read frame: %s\n", err)
						c.Close()
						<-sem
						return
					}
					pos += n
					size -= uint64(n)
				}

				c.Close()

				timeout := time.After(time.Duration(60) * time.Second)
				select {
				case f.channel <- buffer:
				case <-timeout:
					f.logger.Warning("dropped the frame. channel has blocked for too long\n")
				}

				<-sem
			}(con)
		}
	}(conn)

	return nil
}

//////////////////////////////////////////////////////////////////////////////
// THE END
