// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package clustersrv

import (
	"fmt"
	"time"
)

//////////////////////////////////////////////////////////////////////////////

var default_listen_port int = 7891

var default_listen_address string = fmt.Sprintf("0.0.0.0:%d", default_listen_port)

var default_max_connections uint32 = 4096

var default_timeouts = map[string]time.Duration{
	"connecttimeout": time.Duration(2) * time.Second,
	"readtimeout":    time.Duration(10) * time.Second,
	"writetimeout":   time.Duration(60) * time.Second,
}

var default_heartbeatinterval time.Duration = time.Duration(1) * time.Second

var default_recover_wait time.Duration = time.Duration(10) * time.Second

var default_phithreshold float64 = 8.0

//////////////////////////////////////////////////////////////////////////////
// THE END
