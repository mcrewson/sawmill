// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package cmdline

import (
	"mcrewson/sawmill/engine"
	"strings"
)

//////////////////////////////////////////////////////////////////////////////

type InputOperation struct {
	SubOperation
}

func InputOperationInit(op string, args ...string) (Operation, error) {
	oper := &InputOperation{}
	oper.InitSubOperation(op, args)
	oper.AddSubOperation("list", oper.InputOp_List)
	oper.AddSubOperation("create", oper.InputOp_Create)
	oper.AddSubOperation("delete", oper.InputOp_Delete)
	oper.AddSubOperation("addfield", oper.InputOp_Addfield)
	oper.AddSubOperation("addfields", oper.InputOp_Addfield)
	oper.AddSubOperation("removefield", oper.InputOp_Removefield)
	oper.AddSubOperation("removefields", oper.InputOp_Removefield)
	return oper, nil
}

func (i *InputOperation) InputOp_List(eng *engine.Engine) (string, error) {
	i.Output("Inputs:\n-------\n")
	for _, ch := range eng.GetAllInputs() {
		if ch.IsRunning() {
			i.Output(" RUNNING: " + ch.String() + "\n")
		} else {
			i.Output(" STOPPED: " + ch.String() + "\n")
		}
	}
	i.Output("\n")
	return i.Success()
}

func (i *InputOperation) InputOp_Create(eng *engine.Engine) (string, error) {
	name, ok := i.argumentsMap["name"]
	if !ok {
		return i.Failuref("name not specified")
	}

	_, exists := eng.GetInput(name)
	if exists {
		return i.Failuref("input already exists: %s", name)
	}

	_, err := eng.NewInput(i.argumentsMap)
	if err != nil {
		return i.Failure(err)
	}

	return i.Success()
}

func (i *InputOperation) InputOp_Delete(eng *engine.Engine) (string, error) {
	name, ok := i.argumentsMap["name"]
	if !ok {
		return i.Failuref("name not specified")
	}

	ch, exists := eng.GetInput(name)
	if !exists {
		return i.Failuref("unknown input: %s", name)
	}

	if err := ch.Stop(); err != nil {
		return i.Failure(err)
	}
	if err := eng.DeleteInput(name); err != nil {
		return i.Failure(err)
	}
	return i.Success()
}

func (i *InputOperation) InputOp_Addfield(eng *engine.Engine) (string, error) {
	val, ok := i.argumentsMap["name"]
	if !ok {
		return i.Failuref("name not specified")
	}

	ch, exists := eng.GetInput(val)
	if !exists {
		return i.Failuref("unknown input: %s", val)
	}

	val, ok = i.argumentsMap["field"]
	if !ok {
		return i.Failuref("field not specified")
	}
	field := val

	val, ok = i.argumentsMap["value"]
	if !ok {
		return i.Failuref("value not specified")
	}
	value := val

	if err := ch.AddField(field, value); err != nil {
		return i.Failure(err)
	}
	return i.Success()
}

func (i *InputOperation) InputOp_Removefield(eng *engine.Engine) (string, error) {
	val, ok := i.argumentsMap["name"]
	if !ok {
		return i.Failuref("name not specified")
	}

	ch, exists := eng.GetInput(val)
	if !exists {
		return i.Failuref("unknown input: %s", val)
	}

	val, ok = i.argumentsMap["fields"]
	if !ok {
		val, ok = i.argumentsMap["field"]
		if !ok {
			return i.Failuref("field(s) not specified")
		}
	}

	for _, field := range strings.Split(val, ",") {
		field := strings.TrimSpace(field)
		err := ch.RemoveField(field)
		if err != nil {
			return "", err
		}
	}
	return i.Success()
}

func init() {
	RegisterCmdlineOperation(InputOperationInit, "inp", "inpu", "input")
}

//////////////////////////////////////////////////////////////////////////////
// THE END
