// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package cmdline

import (
	"mcrewson/sawmill/engine"
	"mcrewson/sawmill/util"
	"fmt"
	"strings"
)

func init() {
	RegisterCmdlineOperation(GetOperationInit, "get")
	RegisterCmdlineOperation(SetOperationInit, "set")
}

//////////////////////////////////////////////////////////////////////////////

type GetOperation struct {
	BaseOperation
}

func GetOperationInit(op string, args ...string) (Operation, error) {
	oper := &GetOperation{}
	err := oper.InitBaseOperation(op, args)
	if err != nil {
		return nil, err
	}
	return oper, err
}

func (s *GetOperation) Execute(eng *engine.Engine) (result string, err error) {
	if len(s.arguments) < 1 {
		return "", fmt.Errorf("invalid syntax (should be \"get <var>...\")")
	}

	vars := make([]string, 0)
	for i := range s.arguments {
		value, ok := eng.GetVariable(s.arguments[i])
		if !ok {
			vars = append(vars, fmt.Sprintf("unknown variable: %s", s.arguments[i]))
		} else {
			vars = append(vars, fmt.Sprintf("%s=%s", s.arguments[i], value))
		}
	}
	return strings.Join(vars, " ") + "\n", nil
}

//////////////////////////////////////////////////////////////////////////////

type SetOperation struct {
	BaseOperation
}

func SetOperationInit(op string, args ...string) (Operation, error) {
	oper := &SetOperation{}
	err := oper.InitBaseOperation(op, args)
	if err != nil {
		return nil, err
	}
	return oper, err
}

func (s *SetOperation) Execute(eng *engine.Engine) (result string, err error) {
	if len(s.arguments) < 1 {
		return "", fmt.Errorf("invalid syntax (should be \"set <var>=<value>\")")
	}

	for i := range s.arguments {
		parts := strings.SplitN(s.arguments[i], "=", 2)
		if len(parts) != 2 {
			return "", fmt.Errorf("invalid syntax (should be \"set <var>=<value>\" (len=%d))", len(parts))
		}
		key := util.TrimQuotes(parts[0])
		val := util.TrimQuotes(parts[1])
		eng.SetVariable(key, val)
	}
	return "", nil
}

//////////////////////////////////////////////////////////////////////////////
// THE END
