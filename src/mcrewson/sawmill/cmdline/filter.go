// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package cmdline

import (
	"mcrewson/sawmill/engine"
	"strings"
)

//////////////////////////////////////////////////////////////////////////////

type FilterOperation struct {
	SubOperation
}

func FilterOperationInit(op string, args ...string) (Operation, error) {
	oper := &FilterOperation{}
	oper.InitSubOperation(op, args)
	oper.AddSubOperation("list", oper.FilterOp_List)
	oper.AddSubOperation("create", oper.FilterOp_Create)
	oper.AddSubOperation("delete", oper.FilterOp_Delete)
	oper.AddSubOperation("requirefield", oper.FilterOp_AddRequireField)
	oper.AddSubOperation("addrequirefield", oper.FilterOp_AddRequireField)
	oper.AddSubOperation("removerequirefield", oper.FilterOp_RemoveRequireField)
	oper.AddSubOperation("excludefield", oper.FilterOp_AddExcludeField)
	oper.AddSubOperation("addexcludefield", oper.FilterOp_AddExcludeField)
	oper.AddSubOperation("removeexcludefield", oper.FilterOp_RemoveExcludeField)
	return oper, nil
}

func (f *FilterOperation) FilterOp_List(eng *engine.Engine) (string, error) {
	f.Output("Filters:\n--------\n")
	for _, ch := range eng.GetAllFilters() {
		if ch.IsRunning() {
			f.Output(" RUNNING: " + ch.String() + "\n")
		} else {
			f.Output(" STOPPED: " + ch.String() + "\n")
		}
	}
	f.Output("\n")
	return f.Success()
}

func (f *FilterOperation) FilterOp_Create(eng *engine.Engine) (string, error) {
	name, ok := f.argumentsMap["name"]
	if !ok {
		return f.Failuref("name not specified")
	}

	if _, exists := eng.GetFilter(name); exists {
		return f.Failuref("filter already exists: %s", name)
	}

	if _, err := eng.NewFilter(f.argumentsMap); err != nil {
		return f.Failure(err)
	}
	return f.Success()
}

func (f *FilterOperation) FilterOp_Delete(eng *engine.Engine) (string, error) {
	name, ok := f.argumentsMap["name"]
	if !ok {
		return f.Failuref("name not specified")
	}

	_, exists := eng.GetFilter(name)
	if !exists {
		return f.Failuref("unknown filter: %s", name)
	}

	if err := eng.DeleteFilter(name); err != nil {
		return f.Failure(err)
	}
	return f.Success()
}

func (f *FilterOperation) FilterOp_AddRequireField(eng *engine.Engine) (string, error) {
	val, ok := f.argumentsMap["name"]
	if !ok {
		return f.Failuref("name not specified")
	}

	ch, exists := eng.GetFilter(val)
	if !exists {
		return f.Failuref("unknown filter: %s", val)
	}

	val, ok = f.argumentsMap["field"]
	if !ok {
		return f.Failuref("field not specified")
	}
	field := val

	val, ok = f.argumentsMap["value"]
	if !ok {
		return f.Failuref("value not specified")
	}
	value := val

	if err := ch.AddRequiredField(field, value); err != nil {
		return f.Failure(err)
	}
	return f.Success()
}

func (f *FilterOperation) FilterOp_RemoveRequireField(eng *engine.Engine) (string, error) {
	val, ok := f.argumentsMap["name"]
	if !ok {
		return f.Failuref("name not specified")
	}

	ch, exists := eng.GetFilter(val)
	if !exists {
		return f.Failuref("unknown filter: %s", val)
	}

	val, ok = f.argumentsMap["fields"]
	if !ok {
		val, ok = f.argumentsMap["field"]
		if !ok {
			return f.Failuref("field(s) not specified")
		}
	}

	for _, field := range strings.Split(val, ",") {
		field := strings.TrimSpace(field)
		err := ch.RemoveRequiredField(field)
		if err != nil {
			return f.Failure(err)
		}
	}
	return f.Success()
}

func (f *FilterOperation) FilterOp_AddExcludeField(eng *engine.Engine) (string, error) {
	val, ok := f.argumentsMap["name"]
	if !ok {
		return f.Failuref("name not specified")
	}

	ch, exists := eng.GetFilter(val)
	if !exists {
		return f.Failuref("unknown filter: %s", val)
	}

	val, ok = f.argumentsMap["field"]
	if !ok {
		return f.Failuref("field not specified")
	}
	field := val

	val, ok = f.argumentsMap["value"]
	if !ok {
		return f.Failuref("value not specified")
	}
	value := val

	if err := ch.AddExcludedField(field, value); err != nil {
		return f.Failure(err)
	}
	return f.Success()
}

func (f *FilterOperation) FilterOp_RemoveExcludeField(eng *engine.Engine) (string, error) {
	val, ok := f.argumentsMap["name"]
	if !ok {
		return f.Failuref("name not specified")
	}

	ch, exists := eng.GetFilter(val)
	if !exists {
		return f.Failuref("unknown filter: %s", val)
	}

	val, ok = f.argumentsMap["fields"]
	if !ok {
		val, ok = f.argumentsMap["field"]
		if !ok {
			return f.Failuref("field(s) not specified")
		}
	}

	for _, field := range strings.Split(val, ",") {
		field := strings.TrimSpace(field)
		err := ch.RemoveExcludedField(field)
		if err != nil {
			return f.Failure(err)
		}
	}
	return f.Success()
}

func init() {
	RegisterCmdlineOperation(FilterOperationInit, "fil", "filt", "filte", "filter")
}

//////////////////////////////////////////////////////////////////////////////
// THE END
