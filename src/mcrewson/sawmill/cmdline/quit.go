// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package cmdline

import (
	"mcrewson/sawmill/engine"
	"fmt"
)

func init() {
	RegisterCmdlineOperation(QuitOperationInit, "quit", ":q")
	RegisterCmdlineOperation(ShutdownOperationInit, "shutdown")
}

//////////////////////////////////////////////////////////////////////////////

type QuitOperation struct {
	BaseOperation
}

func QuitOperationInit(op string, args ...string) (Operation, error) {
	oper := &QuitOperation{}
	err := oper.InitBaseOperation(op, args)
	if err != nil {
		return nil, err
	}
	return oper, err
}

func (q *QuitOperation) Execute(eng *engine.Engine) (result string, err error) {
	return ">>>QUIT<<<", nil
}

//////////////////////////////////////////////////////////////////////////////

type ShutdownOperation struct {
	BaseOperation
}

func ShutdownOperationInit(op string, args ...string) (Operation, error) {
	oper := &ShutdownOperation{}
	err := oper.InitBaseOperation(op, args)
	if err != nil {
		return nil, err
	}
	return oper, err
}

func (s *ShutdownOperation) Execute(eng *engine.Engine) (result string, err error) {
	if len(s.arguments) < 1 {
		return "", fmt.Errorf("Really?  Prove it by typing 'shutdown NOW'")
	}
	if s.arguments[0] != "NOW" {
		return "", fmt.Errorf("Really?  Prove it by typing 'shutdown NOW'")
	}
	eng.Shutdown()
	return ">>>QUIT<<<", nil
}

//////////////////////////////////////////////////////////////////////////////
// THE END
