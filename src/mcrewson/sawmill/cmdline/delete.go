// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package cmdline

import (
	"mcrewson/sawmill/engine"
	"fmt"
)

func init() {
	RegisterCmdlineOperation(DeleteOperationInit, "delete")
}

//////////////////////////////////////////////////////////////////////////////

type DeleteOperation struct {
	BaseOperation
}

func DeleteOperationInit(op string, args ...string) (Operation, error) {
	oper := &DeleteOperation{}
	err := oper.InitBaseOperation(op, args)
	if err != nil {
		return nil, err
	}
	return oper, err
}

func (d *DeleteOperation) Execute(eng *engine.Engine) (result string, err error) {
	if len(d.arguments) < 1 {
		return "", fmt.Errorf("invalid syntax (should be \"delete <obj>...\")")
	}
	for i := range d.arguments {
		switch d.arguments[i] {
		case "all":
			for _, ch := range eng.GetAllChainables() {
				err := ch.Stop()
				if err != nil {
					return d.Failure(err)
				}
			}

			// delete chains, outputs, filters, and inputs (in that order)
			for _, ch := range eng.GetAllChains() {
				if err = eng.DeleteChain(ch.Name()); err != nil {
					return d.Failure(err)
				}
			}
			for _, ch := range eng.GetAllOutputs() {
				if err = eng.DeleteOutput(ch.Name()); err != nil {
					return d.Failure(err)
				}
			}
			for _, ch := range eng.GetAllFilters() {
				if err = eng.DeleteFilter(ch.Name()); err != nil {
					return d.Failure(err)
				}
			}
			for _, ch := range eng.GetAllInputs() {
				if err = eng.DeleteInput(ch.Name()); err != nil {
					return d.Failure(err)
				}
			}

		case "allinputs":
			for _, ch := range eng.GetAllInputs() {
				err := ch.Stop()
				if err != nil {
					return d.Failure(err)
				}
				if err = eng.DeleteInput(ch.Name()); err != nil {
					return d.Failure(err)
				}
			}

		case "allfilters":
			for _, ch := range eng.GetAllFilters() {
				err := ch.Stop()
				if err != nil {
					return d.Failure(err)
				}
				if err = eng.DeleteFilter(ch.Name()); err != nil {
					return d.Failure(err)
				}
			}

		case "alloutputs":
			for _, ch := range eng.GetAllOutputs() {
				err := ch.Stop()
				if err != nil {
					return d.Failure(err)
				}
				if err = eng.DeleteOutput(ch.Name()); err != nil {
					return d.Failure(err)
				}
			}

		case "allchains":
			for _, ch := range eng.GetAllChains() {
				err := ch.Stop()
				if err != nil {
					return d.Failure(err)
				}
				if err = eng.DeleteChain(ch.Name()); err != nil {
					return d.Failure(err)
				}
			}

		default:
			ch, ok := eng.GetChainable(d.arguments[i])
			if !ok {
				return d.Failuref("unknown chainable: %s", d.arguments[i])
			}
			err := ch.Stop()
			if err != nil {
				return d.Failure(err)
			}
			switch ch.Type() {
			case engine.INPUT:
				if err = eng.DeleteInput(ch.Name()); err != nil {
					return d.Failure(err)
				}
			case engine.FILTER:
				if err = eng.DeleteFilter(ch.Name()); err != nil {
					return d.Failure(err)
				}
			case engine.OUTPUT:
				if err = eng.DeleteOutput(ch.Name()); err != nil {
					return d.Failure(err)
				}
			case engine.CHAIN:
				if err = eng.DeleteChain(ch.Name()); err != nil {
					return d.Failure(err)
				}
			default:
				return d.Failuref("wtf? unknown chainable type: %s", ch.Type())
			}
		}
	}
	return "", nil
}

//////////////////////////////////////////////////////////////////////////////
// THE END
