// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package cmdline

import (
	"mcrewson/sawmill/engine"
)

//////////////////////////////////////////////////////////////////////////////

type ChainOperation struct {
	SubOperation
}

func ChainOperationInit(op string, args ...string) (Operation, error) {
	oper := &ChainOperation{}
	oper.InitSubOperation(op, args)
	oper.AddSubOperation("list", oper.ChainOp_List)
	oper.AddSubOperation("create", oper.ChainOp_Create)
	oper.AddSubOperation("delete", oper.ChainOp_Delete)
	return oper, nil
}

func (c *ChainOperation) ChainOp_List(eng *engine.Engine) (string, error) {
	c.Output("Chains:\n-------\n")
	for _, ch := range eng.GetAllChains() {
		if ch.IsRunning() {
			c.Output(" RUNNING: " + ch.String() + "\n")
		} else {
			c.Output(" STOPPED: " + ch.String() + "\n")
		}
	}
	c.Output("\n")
	return c.Success()
}

func (c *ChainOperation) ChainOp_Create(eng *engine.Engine) (string, error) {
	val, ok := c.argumentsMap["name"]
	if !ok {
		return c.Failuref("name not specified")
	}
	name := val

	_, exists := eng.GetChain(name)
	if exists {
		return c.Failuref("chain already exists: %s", name)
	}

	c.argumentsMap["type"] = "chain"
	_, err := eng.NewChain(c.argumentsMap)
	if err != nil {
		return c.Failure(err)
	}

	return c.Success()
}

func (c *ChainOperation) ChainOp_Delete(eng *engine.Engine) (string, error) {
	name, ok := c.argumentsMap["name"]
	if !ok {
		return c.Failuref("name not specified")
	}
	_, exists := eng.GetChain(name)
	if !exists {
		return c.Failuref("unknown chain: %s", name)
	}

	if err := eng.DeleteChain(name); err != nil {
		return c.Failure(err)
	}
	return c.Success()
}

func init() {
	RegisterCmdlineOperation(ChainOperationInit, "cha", "chai", "chain")
}

//////////////////////////////////////////////////////////////////////////////
// THE END
