// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package cmdline

import (
	"mcrewson/sawmill/engine"
	"fmt"
)

func init() {
	RegisterCmdlineOperation(StartOperationInit, "start")
	RegisterCmdlineOperation(StopOperationInit, "stop")
}

//////////////////////////////////////////////////////////////////////////////

type StartOperation struct {
	BaseOperation
}

func StartOperationInit(op string, args ...string) (Operation, error) {
	oper := &StartOperation{}
	err := oper.InitBaseOperation(op, args)
	if err != nil {
		return nil, err
	}
	return oper, err
}

func (s *StartOperation) Execute(eng *engine.Engine) (result string, err error) {
	if len(s.arguments) < 1 {
		return "", fmt.Errorf("invalid syntax (should be \"start <obj>...\")")
	}

	for i := range s.arguments {
		switch s.arguments[i] {
		case "all":
			for _, chainable := range eng.GetAllChainables() {
				err := chainable.Start()
				if err != nil {
					return "", err
				}
			}

		case "allinputs":
			for _, chainable := range eng.GetAllInputs() {
				err := chainable.Start()
				if err != nil {
					return "", err
				}
			}

		case "allfilters":
			for _, chainable := range eng.GetAllFilters() {
				err := chainable.Start()
				if err != nil {
					return "", err
				}
			}

		case "alloutputs":
			for _, chainable := range eng.GetAllOutputs() {
				err := chainable.Start()
				if err != nil {
					return "", err
				}
			}

		case "allchains":
			for _, chainable := range eng.GetAllChains() {
				err := chainable.Start()
				if err != nil {
					return "", err
				}
			}

		default:
			chainable, ok := eng.GetChainable(s.arguments[i])
			if !ok {
				return "", fmt.Errorf("unknown chainable: %s", s.arguments[i])
			}
			err := chainable.Start()
			if err != nil {
				return "", err
			}
		}
	}
	return "", nil
}

//////////////////////////////////////////////////////////////////////////////

type StopOperation struct {
	BaseOperation
}

func StopOperationInit(op string, args ...string) (Operation, error) {
	oper := &StopOperation{}
	err := oper.InitBaseOperation(op, args)
	if err != nil {
		return nil, err
	}
	return oper, err
}

func (s *StopOperation) Execute(eng *engine.Engine) (result string, err error) {
	if len(s.arguments) < 1 {
		return "", fmt.Errorf("invalid syntax (should be \"start <obj>...\")")
	}

	for i := range s.arguments {
		switch s.arguments[i] {
		case "all":
			for _, chainable := range eng.GetAllChainables() {
				err := chainable.Stop()
				if err != nil {
					return "", err
				}
			}

		case "allinputs":
			for _, chainable := range eng.GetAllInputs() {
				err := chainable.Stop()
				if err != nil {
					return "", err
				}
			}

		case "allfilters":
			for _, chainable := range eng.GetAllFilters() {
				err := chainable.Stop()
				if err != nil {
					return "", err
				}
			}

		case "alloutputs":
			for _, chainable := range eng.GetAllOutputs() {
				err := chainable.Stop()
				if err != nil {
					return "", err
				}
			}

		case "allchains":
			for _, chainable := range eng.GetAllChains() {
				err := chainable.Stop()
				if err != nil {
					return "", err
				}
			}

		default:
			chainable, ok := eng.GetChainable(s.arguments[i])
			if !ok {
				return "", fmt.Errorf("unknown chainable: %s", s.arguments[i])
			}
			err := chainable.Stop()
			if err != nil {
				return "", err
			}
		}
	}
	return "", nil
}

//////////////////////////////////////////////////////////////////////////////
// THE END
