// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package cmdline

import (
	"mcrewson/sawmill/engine"
	"mcrewson/sawmill/version"
	"io"
	"net"
	"strings"
)

//////////////////////////////////////////////////////////////////////////////

func NetReader(addr string, eng *engine.Engine) error {
	psock, err := net.Listen("tcp", addr)
	if err != nil {
		return err
	}

	serverfunc := func() {
		for {
			conn, err := psock.Accept()
			if err != nil {
				return
			}

			eng.Logger.Infof("Connection from %v\n", conn.RemoteAddr())
			go tcphandler(conn, eng)
		}
	}
	go serverfunc()
	return nil
}

func tcphandler(conn net.Conn, eng *engine.Engine) {
	defer conn.Close()

	cmd := NewCmdlineProcessor(eng)

	conn.Write([]byte(version.Banner() + "\n"))
	more := false
	buf := make([]byte, 1024)
promptloop:
	for {
		prompt := "sawmill> "
		if more {
			prompt = ".......> "
		}
		conn.Write([]byte(prompt))

		n, err := conn.Read(buf)
		if err != nil {
			if err != io.EOF {
				eng.Logger.Errorf("tcphandler read problem: %v\n", err)
			}
			return
		}

		content := string(buf[:n])
		for _, line := range strings.Split(content, "\n") {
			result, m, cerr := cmd.ProcessLine(line)
			if cerr != nil {
				conn.Write([]byte(cerr.Error() + "\n"))
			} else if result == ">>>QUIT<<<" {
				break promptloop
			} else if result != "" {
				conn.Write([]byte(result))
			}
			more = m
		}
	}
}

//////////////////////////////////////////////////////////////////////////////
// THE END
