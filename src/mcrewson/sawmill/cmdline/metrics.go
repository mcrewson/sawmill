// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package cmdline

import (
	"mcrewson/sawmill/engine"
)

//////////////////////////////////////////////////////////////////////////////

type MetricOperation struct {
	SubOperation
}

func MetricOperationInit(op string, args ...string) (Operation, error) {
	oper := &MetricOperation{}
	oper.InitSubOperation(op, args)
	oper.AddSubOperation("dump", oper.MetricOp_Dump)
	return oper, nil
}

func (m *MetricOperation) MetricOp_Dump(eng *engine.Engine) (string, error) {
	m.Output("Metrics Dump:\n------------\n")
	m.Output(eng.DumpMetrics())
	return m.Success()
}

func init() {
	RegisterCmdlineOperation(MetricOperationInit, "metrics", "metric")
}

//////////////////////////////////////////////////////////////////////////////
// THE END
