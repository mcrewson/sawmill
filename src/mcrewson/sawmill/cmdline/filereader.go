// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package cmdline

import (
	"mcrewson/sawmill/engine"
	"bufio"
	"bytes"
	"fmt"
	"io"
	"os"
)

//////////////////////////////////////////////////////////////////////////////

func FileReader(filename string, eng *engine.Engine) (string, error) {
	f, err := os.Open(filename)
	if err != nil {
		return "", err
	}
	defer f.Close()
	reader := bufio.NewReader(f)

	cmd := NewCmdlineProcessor(eng)
	buf := &bytes.Buffer{}
	for {
		line, e := reader.ReadString('\n')
		if e == nil {
			result, _, cerr := cmd.ProcessLine(line)
			if cerr != nil {
				fmt.Fprintf(buf, "%s\n", cerr)
			} else if result == ">>>QUIT<<<" {
				break
			} else if result != "" {
				fmt.Fprintf(buf, "%s\n", result)
			}
		} else if e == io.EOF {
			break
		} else {
			return "", e
		}
	}

	return buf.String(), nil
}

//////////////////////////////////////////////////////////////////////////////
// THE END
