// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package cmdline

import (
	"mcrewson/sawmill/engine"
	"mcrewson/sawmill/util"
	"bytes"
	"fmt"
	"strings"
)

//////////////////////////////////////////////////////////////////////////////

type CmdlineProcessor struct {
	buffer []string
	engine *engine.Engine
}

func NewCmdlineProcessor(eng *engine.Engine) *CmdlineProcessor {
	proc := &CmdlineProcessor{engine: eng}
	proc._reset_buffer()

	return proc
}

func (cmd *CmdlineProcessor) ProcessLine(line string) (result string, more bool, err error) {
	line = strings.TrimSpace(line)
	if line == "" || line[0] == '#' {
		return
	}

	cmd.buffer = append(cmd.buffer, line)

	source := strings.Join(cmd.buffer, "\n")
	if source == "" {
		return
	}

	result, more, err = cmd.interpret(source)
	if err != nil {
		cmd.engine.Logger.Error(err)
	}
	if !more {
		cmd._reset_buffer()
	}
	return
}

func (cmd *CmdlineProcessor) interpret(source string) (result string, more bool, err error) {
	if strings.HasSuffix(source, "\\") {
		// line continuation marker
		return "", true, nil
	}

	// remove line continuations from the source
	source = strings.Replace(source, "\\\n", "", -1)

	parts := util.TokenizeString(source)
	op := parts[0]
	args := parts[1:]

	op = strings.ToLower(op)

	oper, err := NewOperation(op, args...)
	if err != nil {
		return "", false, err
	}

	result, err = oper.Execute(cmd.engine)
	if err != nil {
		return "", false, err
	}
	return result, false, nil
}

func (cmd *CmdlineProcessor) _reset_buffer() {
	cmd.buffer = make([]string, 0)
}

//////////////////////////////////////////////////////////////////////////////

type OperationInitFunc func(op string, args ...string) (Operation, error)

type Operation interface {
	Execute(eng *engine.Engine) (string, error)
}

var _operations map[string]OperationInitFunc

func RegisterCmdlineOperation(initFunc OperationInitFunc, opnames ...string) error {
	if _operations == nil {
		_operations = make(map[string]OperationInitFunc)
	}

	for _, opname := range opnames {
		_, exists := _operations[opname]
		if exists {
			return fmt.Errorf("Already registered an operation of this type: %s", opname)
		}
		_operations[opname] = initFunc
	}
	return nil
}

func NewOperation(op string, args ...string) (oper Operation, err error) {
	initFunc, ok := _operations[op]
	if !ok {
		err = fmt.Errorf("unknown operation: %s", op)
		return
	}

	oper, err = initFunc(op, args...)
	return
}

//////////////////////////////////////////////////////////////////////////////

type BaseOperation struct {
	operation string
	arguments []string
}

func (bo *BaseOperation) InitBaseOperation(op string, args []string) error {
	bo.operation = op
	bo.arguments = args
	return nil
}

func (bo *BaseOperation) Failure(err error) (string, error) {
	return "", err
}

func (bo *BaseOperation) Failuref(format string, a ...interface{}) (string, error) {
	return "", fmt.Errorf(format, a...)
}

//////////////////////////////////////////////////////////////////////////////

type SubOperation struct {
	BaseOperation
	argumentsMap map[string]string
	subops       map[string]func(*engine.Engine) (string, error)
	outBuffer    bytes.Buffer
	hasOutput    bool
}

func (so *SubOperation) InitSubOperation(op string, args []string) error {
	so.InitBaseOperation(op, args)
	so.subops = make(map[string]func(*engine.Engine) (string, error))
	return nil
}

func (so *SubOperation) AddSubOperation(name string, subop func(*engine.Engine) (string, error)) error {
	so.subops[name] = subop
	return nil
}

func (so *SubOperation) Execute(eng *engine.Engine) (string, error) {
	if len(so.arguments) < 1 {
		return "", fmt.Errorf("'%s' operation requires one or more arguments", so.operation)
	}
	command := so.arguments[0]
	subop, ok := so.subops[command]
	if !ok {
		return "", fmt.Errorf("Unknown %s operation: '%s %s ...'", so.operation, so.operation, command)
	}

	so.operation = so.operation + " " + command
	so.arguments = so.arguments[1:]

	cmdargs, err := util.ArgumentsToMap(so.arguments)
	if err != nil {
		return so.Failure(err)
	}
	so.argumentsMap = cmdargs

	return subop(eng)
}

func (so *SubOperation) Output(data string) (err error) {
	_, err = so.outBuffer.WriteString(data)
	so.hasOutput = true
	return
}

func (so *SubOperation) Success() (result string, err error) {
	if so.hasOutput {
		result = so.outBuffer.String()
		so.outBuffer.Reset()
	}
	return
}

//////////////////////////////////////////////////////////////////////////////

type DebugOperation struct {
	BaseOperation
}

func DebugOperationInit(op string, args ...string) (Operation, error) {
	oper := &DebugOperation{}
	err := oper.InitBaseOperation(op, args)
	if err != nil {
		return nil, err
	}
	return oper, err
}

func (d *DebugOperation) Execute(eng *engine.Engine) (result string, err error) {
	result = fmt.Sprintf("DEBUGOPER: args = \"%s\"\n", d.arguments)
	return
}

func init() {
	RegisterCmdlineOperation(DebugOperationInit, "debug")
}

//////////////////////////////////////////////////////////////////////////////
// THE END
