// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package cmdline

import (
	"mcrewson/sawmill/engine"
	"mcrewson/sawmill/util/logging"
	"bytes"
)

func init() {
	RegisterCmdlineOperation(LogOperationInit, "log")
}

//////////////////////////////////////////////////////////////////////////////

type LogOperation struct {
	SubOperation
}

func LogOperationInit(op string, args ...string) (Operation, error) {
	oper := &LogOperation{}
	oper.InitSubOperation(op, args)
	oper.AddSubOperation("dump", oper.LogOp_Dump)
	oper.AddSubOperation("setlevel", oper.LogOp_SetLevel)
	oper.AddSubOperation("clear", oper.LogOp_Clear)
	return oper, nil
}

func (s *LogOperation) LogOp_Dump(eng *engine.Engine) (string, error) {
	buf := &bytes.Buffer{}
	buf.Write(eng.Logger.DumpMemory())
	return buf.String(), nil
}

func (s *LogOperation) LogOp_SetLevel(eng *engine.Engine) (string, error) {
	levelname, ok := s.argumentsMap["level"]
	if !ok {
		levelname, ok = s.argumentsMap["lvl"]
		if !ok {
			return s.Failuref("level not specified")
		}
	}

	level, err := logging.LevelFromName(levelname)
	if err != nil {
		return s.Failuref("invalid log level: %s\n", levelname)
	}

	eng.Logger.Noticef("Log level now set to %s\n", level.String())
	eng.Logger.SetLevel(level)
	return s.Success()
}

func (s *LogOperation) LogOp_Clear(eng *engine.Engine) (string, error) {
	eng.Logger.ClearMemory()
	return s.Success()
}

//////////////////////////////////////////////////////////////////////////////
// THE END
