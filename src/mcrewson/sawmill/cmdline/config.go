// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package cmdline

import (
	"mcrewson/sawmill/engine"
	"mcrewson/sawmill/util/logging"
	"mcrewson/sawmill/version"
	"bytes"
	"os"
)

func init() {
	RegisterCmdlineOperation(ConfigOperationInit, "cnf", "conf", "confi", "config")
}

//////////////////////////////////////////////////////////////////////////////

type ConfigOperation struct {
	SubOperation
}

func ConfigOperationInit(op string, args ...string) (Operation, error) {
	oper := &ConfigOperation{}
	oper.InitSubOperation(op, args)
	oper.AddSubOperation("dump", oper.ConfigOp_Dump)
	oper.AddSubOperation("save", oper.ConfigOp_Save)
	oper.AddSubOperation("load", oper.ConfigOp_Load)
	oper.AddSubOperation("flush", oper.ConfigOp_Flush)
	return oper, nil
}

func (s *ConfigOperation) ConfigOp_Dump(eng *engine.Engine) (string, error) {
	cnf, err := generate_config(eng)
	if err != nil {
		return s.Failure(err)
	}
	return cnf, nil
}

func (s *ConfigOperation) ConfigOp_Save(eng *engine.Engine) (string, error) {
	filename, ok := s.argumentsMap["file"]
	if !ok {
		filename, ok = s.argumentsMap["filename"]
		if !ok {
			return s.Failuref("file name not specified")
		}
	}

	cnf, err := generate_config(eng)
	if err != nil {
		return s.Failure(err)
	}

	f, ferr := os.Create(filename)
	defer f.Close()
	if ferr != nil {
		return s.Failure(ferr)
	}
	_, ferr = f.WriteString(cnf)
	if ferr != nil {
		return s.Failure(ferr)
	}
	return s.Success()
}

func (s *ConfigOperation) ConfigOp_Load(eng *engine.Engine) (string, error) {
	filename, ok := s.argumentsMap["file"]
	if !ok {
		filename, ok = s.argumentsMap["filename"]
		if !ok {
			return s.Failuref("file name not specified")
		}
	}

	return FileReader(filename, eng)
}

func (s *ConfigOperation) ConfigOp_Flush(eng *engine.Engine) (string, error) {
	err := eng.FlushAll()
	if err != nil {
		return s.Failure(err)
	}
	return s.Success()
}

//////////////////////////////////////////////////////////////////////////////

func generate_config(eng *engine.Engine) (string, error) {
	buffer := new(bytes.Buffer)
	buffer.WriteString("# " + version.Banner() + "\n#\n\n")

	lvl := eng.Logger.Level()
	if lvl != logging.NOTICE {
		buffer.WriteString("log setlevel level=" + lvl.String() + "\n\n")
	}

	varnames := eng.GetVariableNames()
	if len(varnames) > 0 {
		for _, k := range varnames {
			v, _ := eng.GetVariable(k)
			buffer.WriteString("set " + k + "=" + v + "\n")
		}
		buffer.WriteString("\n")
	}

	inputs := eng.GetAllInputs()
	if len(inputs) > 0 {
		for _, ch := range inputs {
			buffer.WriteString("input create " + ch.String() + "\n")
		}
		buffer.WriteString("\n")
	}

	filters := eng.GetAllFilters()
	if len(filters) > 0 {
		for _, ch := range filters {
			buffer.WriteString("filter create " + ch.String() + "\n")
		}
		buffer.WriteString("\n")
	}

	outputs := eng.GetAllOutputs()
	if len(outputs) > 0 {
		for _, ch := range outputs {
			buffer.WriteString("output create " + ch.String() + "\n")
		}
		buffer.WriteString("\n")
	}

	chains := eng.GetAllChains()
	if len(chains) > 0 {
		for _, ch := range chains {
			buffer.WriteString("chain create " + ch.String() + "\n")
		}
		buffer.WriteString("\n")
	}

	// output the appropriate "start ..." commands to restart the
	// current running state of the engine
	chainables := eng.GetAllChainables()
	if len(chainables) > 0 {
		all := true
		for _, ch := range chainables {
			if !ch.IsRunning() {
				all = false
				break
			}
		}
		if all {
			buffer.WriteString("start all\n")
		} else {
			// inputs
			all = true
			for _, ch := range eng.GetAllInputs() {
				if !ch.IsRunning() {
					all = false
					break
				}
			}
			if all {
				buffer.WriteString("start allinputs\n")
			} else {
				for _, ch := range eng.GetAllInputs() {
					if ch.IsRunning() {
						buffer.WriteString("start " + ch.Name() + "\n")
					}
				}
			}

			// filters
			all = true
			for _, ch := range eng.GetAllFilters() {
				if !ch.IsRunning() {
					all = false
					break
				}
			}
			if all {
				buffer.WriteString("start allfilters\n")
			} else {
				for _, ch := range eng.GetAllFilters() {
					if ch.IsRunning() {
						buffer.WriteString("start " + ch.Name() + "\n")
					}
				}
			}

			// outputs
			all = true
			for _, ch := range eng.GetAllOutputs() {
				if !ch.IsRunning() {
					all = false
					break
				}
			}
			if all {
				buffer.WriteString("start alloutputs\n")
			} else {
				for _, ch := range eng.GetAllOutputs() {
					if ch.IsRunning() {
						buffer.WriteString("start " + ch.Name() + "\n")
					}
				}
			}

			// chains
			all = true
			for _, ch := range eng.GetAllChains() {
				if !ch.IsRunning() {
					all = false
					break
				}
			}
			if all {
				buffer.WriteString("start allchains\n")
			} else {
				for _, ch := range eng.GetAllChains() {
					if ch.IsRunning() {
						buffer.WriteString("start " + ch.Name() + "\n")
					}
				}
			}
		}
	}

	return buffer.String(), nil
}

//////////////////////////////////////////////////////////////////////////////
// THE END
