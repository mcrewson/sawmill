// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package cmdline

import (
	"mcrewson/sawmill/engine"
	"strings"
)

//////////////////////////////////////////////////////////////////////////////

type OutputOperation struct {
	SubOperation
}

func OutputOperationInit(op string, args ...string) (Operation, error) {
	oper := &OutputOperation{}
	oper.InitSubOperation(op, args)
	oper.AddSubOperation("list", oper.OutputOp_List)
	oper.AddSubOperation("create", oper.OutputOp_Create)
	oper.AddSubOperation("delete", oper.OutputOp_Delete)
	oper.AddSubOperation("requirefield", oper.OutputOp_AddRequireField)
	oper.AddSubOperation("addrequirefield", oper.OutputOp_AddRequireField)
	oper.AddSubOperation("removerequirefield", oper.OutputOp_RemoveRequireField)
	oper.AddSubOperation("excludefield", oper.OutputOp_AddExcludeField)
	oper.AddSubOperation("addexcludefield", oper.OutputOp_AddExcludeField)
	oper.AddSubOperation("removeexcludefield", oper.OutputOp_RemoveExcludeField)
	return oper, nil
}

func (o *OutputOperation) OutputOp_List(eng *engine.Engine) (string, error) {
	o.Output("Outputs:\n--------\n")
	for _, ch := range eng.GetAllOutputs() {
		if ch.IsRunning() {
			o.Output(" RUNNING: " + ch.String() + "\n")
		} else {
			o.Output(" STOPPED: " + ch.String() + "\n")
		}
	}
	o.Output("\n")
	return o.Success()
}

func (o *OutputOperation) OutputOp_Create(eng *engine.Engine) (string, error) {
	name, ok := o.argumentsMap["name"]
	if !ok {
		return o.Failuref("name not specified")
	}

	_, exists := eng.GetOutput(name)
	if exists {
		return o.Failuref("output already exists: %s", name)
	}

	if _, err := eng.NewOutput(o.argumentsMap); err != nil {
		return o.Failure(err)
	}
	return o.Success()
}

func (o *OutputOperation) OutputOp_Delete(eng *engine.Engine) (string, error) {
	name, ok := o.argumentsMap["name"]
	if !ok {
		return o.Failuref("name not specified")
	}

	_, exists := eng.GetOutput(name)
	if !exists {
		return o.Failuref("unknown output: %s", name)
	}

	if err := eng.DeleteOutput(name); err != nil {
		return o.Failure(err)
	}
	return o.Success()
}

func (o *OutputOperation) OutputOp_AddRequireField(eng *engine.Engine) (string, error) {
	val, ok := o.argumentsMap["name"]
	if !ok {
		return o.Failuref("name not specified")
	}

	ch, exists := eng.GetOutput(val)
	if !exists {
		return o.Failuref("unknown output: %s", val)
	}

	val, ok = o.argumentsMap["field"]
	if !ok {
		return o.Failuref("field not specified")
	}
	field := val

	val, ok = o.argumentsMap["value"]
	if !ok {
		return o.Failuref("value not specified")
	}
	value := val

	if err := ch.AddRequiredField(field, value); err != nil {
		return o.Failure(err)
	}
	return o.Success()
}

func (o *OutputOperation) OutputOp_RemoveRequireField(eng *engine.Engine) (string, error) {
	val, ok := o.argumentsMap["name"]
	if !ok {
		return o.Failuref("name not specified")
	}

	ch, exists := eng.GetOutput(val)
	if !exists {
		return o.Failuref("unknown output: %s", val)
	}

	val, ok = o.argumentsMap["fields"]
	if !ok {
		val, ok = o.argumentsMap["field"]
		if !ok {
			return o.Failuref("field(s) not specified")
		}
	}

	for _, field := range strings.Split(val, ",") {
		field := strings.TrimSpace(field)
		err := ch.RemoveRequiredField(field)
		if err != nil {
			return o.Failure(err)
		}
	}
	return o.Success()
}

func (o *OutputOperation) OutputOp_AddExcludeField(eng *engine.Engine) (string, error) {
	val, ok := o.argumentsMap["name"]
	if !ok {
		return o.Failuref("name not specified")
	}

	ch, exists := eng.GetOutput(val)
	if !exists {
		return o.Failuref("unknown output: %s", val)
	}

	val, ok = o.argumentsMap["field"]
	if !ok {
		return o.Failuref("field not specified")
	}
	field := val

	val, ok = o.argumentsMap["value"]
	if !ok {
		return o.Failuref("value not specified")
	}
	value := val

	if err := ch.AddExcludedField(field, value); err != nil {
		return o.Failure(err)
	}
	return o.Success()
}

func (o *OutputOperation) OutputOp_RemoveExcludeField(eng *engine.Engine) (string, error) {
	val, ok := o.argumentsMap["name"]
	if !ok {
		return o.Failuref("name not specified")
	}

	ch, exists := eng.GetOutput(val)
	if !exists {
		return o.Failuref("unknown output: %s", val)
	}

	val, ok = o.argumentsMap["fields"]
	if !ok {
		val, ok = o.argumentsMap["field"]
		if !ok {
			return o.Failuref("field(s) not specified")
		}
	}

	for _, field := range strings.Split(val, ",") {
		field := strings.TrimSpace(field)
		err := ch.RemoveExcludedField(field)
		if err != nil {
			return o.Failure(err)
		}
	}
	return o.Success()
}
func init() {
	RegisterCmdlineOperation(OutputOperationInit, "out", "outp", "outpu", "output")
}

//////////////////////////////////////////////////////////////////////////////
// THE END
