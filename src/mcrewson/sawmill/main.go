// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package main

import (
	"flag"
	"fmt"
	stdlog "log"
	"os"
	"os/signal"
	"runtime/pprof"
	"syscall"

	"github.com/VividCortex/godaemon"

	"mcrewson/sawmill/cmdline"
	"mcrewson/sawmill/engine"
	"mcrewson/sawmill/engine/filters"
	"mcrewson/sawmill/engine/inputs"
	"mcrewson/sawmill/engine/outputs"
	"mcrewson/sawmill/util/logging"
	"mcrewson/sawmill/version"
)

// silly no-op variables to force the chainable sub-packages to be included at compile time
var _ = inputs.NOOP
var _ = filters.NOOP
var _ = outputs.NOOP

var console_off = "off"
var console_default = ":7890"

var config = flag.String("config", "", "Config to read at startup")
var daemonize = flag.Bool("daemonize", false, "Daemonize sawmill, running it in the background")
var pidfile = flag.String("pidfile", "", "Write daemonized process id to file")
var logfile = flag.String("logfile", "", "Write sawmill's internal log messages to file")
var console = flag.String("console", console_default, "TCP address to listen on for commands")
var profile = flag.String("cpuprofile", "", "Enable the golang cpuprofiler")

const logformat string = "%{time} %{level:-8.8s} %{message}"

func main() {
	flag.Parse()

	if *profile != "" {
		f, err := os.Create(*profile)
		if err != nil {
			stdlog.Fatal(err)
		}
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}

	var logger *logging.Logger
	var lerr error
	if *logfile != "" {
		logger, lerr = logging.FileLogger("", logging.NOTICE, logformat, *logfile)
	} else {
		logger, lerr = logging.NullLogger("", logging.NOTICE, logformat)
	}
	if lerr != nil {
		stdlog.Fatalf("Cannot create internal loggers: %s\n", lerr)
	}
	logger.Noticef("%s started.\n", version.Banner())

	if *daemonize {
		_, _, err := godaemon.MakeDaemon(&godaemon.DaemonAttr{})
		if err != nil {
			stdlog.Fatalf("cannot daemonize: %s\n", err)
		}

		if *pidfile != "" {
			pf, err := os.Create(*pidfile)
			if err != nil {
				stdlog.Fatalf("cannot write pid file: %s\n", err)
			}
			_, err = pf.Write([]byte(fmt.Sprintf("%d\n", os.Getpid())))
			if err != nil {
				stdlog.Fatalf("cannot write pid file: %s\n", err)
			}
			pf.Close()
		}
	} else {
		if *pidfile != "" {
			stdlog.Println("Not writing pid file. Sawmill is not daemonized.")
			*pidfile = ""
		}
	}

	eng, err := engine.GetTheEngine()
	if err != nil {
		logger.Fatalf("failed to create the sawmill engine: %s\n", err)
	}
	eng.Logger = logger

	err = setupConsole(*console, eng)
	if err != nil {
		logger.Fatalf("failed to start tcpserver: %s\n", err)
	}

	if *config != "" {
		result, cerr := cmdline.FileReader(*config, eng)
		if cerr != nil {
			logger.Fatalf("failed to reader config file (%s): %s\n", *config, cerr)
		}
		if result != "" {
			logger.Info(result)
		}
	}

	// Setup signal handlers
	termChannel := make(chan os.Signal, 2)
	go func() {
		<-termChannel
		eng.Shutdown()
	}()
	signal.Notify(termChannel, syscall.SIGTERM, syscall.SIGINT)

	// wait for the engine
	eng.WaitForShutdown()

	// cleanup
	logger.Destroy()
	if *pidfile != "" {
		err := os.Remove(*pidfile)
		if err != nil {
			stdlog.Fatalf("cannot remove pid file: %s\n", err)
		}
	}
}

func setupConsole(console string, eng *engine.Engine) error {
	if console == console_off {
		return nil
	}

	err := cmdline.NetReader(console, eng)
	return err
}

//////////////////////////////////////////////////////////////////////////////
// THE END
