// vim:set ts=4 sw=4 sts=4 et nowrap syntax=go:
//

package version

func Version() string {
	return version
}

func Banner() string {
	return banner
}

var name = "Sawmill"
var version = "1.0.1"
var release = "Water"

var banner = name + " v" + version + " (\"" + release + "\")"

//var release = "33"
//var release = "Water"
//var release = "Bastille Day"
//var release = "Act of Contrition"
//var release = "You Can't Go Home Again"
//var release = "Litmus"
//var release = "Six Degrees of Separation"
//var release = "Flesh and Bone"
//var release = "Tigh Me Up, Tigh Me Down"
//var release = "The Hand of God"
//var release = "Colonial Day"
//var release = "Kobol's Last Gleaming"

//var release = "Scattered"
//var release = "Valley of Darkness"
//var release = "Fragged"
//var release = "Resistance"
//var release = "The Farm"
//var release = "Home"
//var release = "Final Cut"
//var release = "Flight of the Phoenix"
//var release = "Pegasus"
//var release = "Resurrection Ship"
//var release = "Epiphanies"
//var release = "Black Market"
//var release = "Scar"
//var release = "Sacrifice"
//var release = "The Captain's Hand"
//var release = "Downloaded"
//var release = "Lay Down Your Burdens"

//var release = "Occupation"
//var release = "Precipice"
//var release = "Exodus"
//var release = "Collaborators"
//var release = "Torn"
//var release = "A Measure of Salvation"
//var release = "Hero"
//var release = "Unfinished Business"
//var release = "The Passage"
//var release = "The Eye of Jupiter"
//var release = "Rapture"
//var release = "Taking a Break from All Your Worries"
//var release = "The Woman King"
//var release = "A Day in the Life"
//var release = "Dirty Hands"
//var release = "Maelstrom"
//var release = "The Son Also Rises"
//var release = "Crossroads"

//var release = "He That Believeth in Me"
//var release = "Six of One"
//var release = "The Ties That Bind"
//var release = "Escape Velocity"
//var release = "The Road Less Traveled"
//var release = "Faith"
//var release = "Guess What's Coming to Dinner?"
//var release = "Sine Qua Non"
//var release = "The Hub"
//var release = "Revelations"
//var release = "Sometimes a Great Nation"
//var release = "A Disquiet Follows My Soul"
//var release = "The Oath"
//var release = "Blood on the Scales"
//var release = "No Exit"
//var release = "Deadlock"
//var release = "Someone to Watch Over Me"
//var release = "Islanded in a Stream of Stars"
//var release = "Daybreak"

//////////////////////////////////////////////////////////////////////////////
// THE END
