# sawmill configuration

# globals
set conn_timeout=2 read_timeout=10 write_timeout=20

# inputs
input create   name=syslog1 type=syslog socket="/run/systemd/journal/syslog" cleanup=true ignored_priorities="authpriv.ALL"
input addfield name=syslog1 field=type value=syslog

input create   name=syslog2 type=syslog sockets="0.0.0.0:514, :::514" ignored_priorities="authpriv.ALL"
input addfield name=syslog2 field=type value=syslog

input removefields name=syslog2 fields=type,@timestamp
input destroy name=syslog2

# filters
filter create name=hostn1 type=hostname
filter create name=debug1 type=debug

filter create name=grok1  type=grok field=message match="<%{POSINT:syslog_pri}>%{SYSLOGTIMESTAMP:timestamp} (?:%{SYSLOGFACILITY} )?(?:%{SYSLOGHOST:hostname} )?(?:%{SYSLOGPROG:syslog_tag}: )?%{GREEDYDATA:message}"
filter requirefield name=grok1 field=type value=syslog
filter excludefield name=grok1 field=hostname regexp=curcl-g0?

# outputs
output create name=redisout type=redis servers="127.0.0.1:6379,archlab01.dev.mcrewson:6379" listname=logevents conn_timeout=1 write_timeout=5

# chains
chain create name=syslogfilterchain pipe=hostn1,grok1,debug1
chain create name="syslogout1" pipe="syslog1,syslogfilterchain,redisout"
chain create name="syslogout2" pipe="syslog2,syslogfilterchain,redisout"

start allchains

