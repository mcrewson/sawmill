#!/bin/bash

PROGRAM=sawmill

TOPLEVEL_PKG=mcrewson/${PROGRAM}

export GOPATH=${HOME}/work/${PROGRAM}
GOCMD=go
GOBUILD="${GOCMD} build"
STRIPCMD="strip --strip-unneeded"
SHRINKCMD="upx -q"

(
  set -e
  cd ${GOPATH} 
  echo "compiling ..."
  ${GOBUILD} --ldflags '-extldflags "-static"' ${TOPLEVEL_PKG}

  echo "striping ..."
  ${STRIPCMD} ${PROGRAM}

  echo "shrinking ..."
  ${SHRINKCMD} ${PROGRAM}

  ls -l ${PROGRAM}
)

if [ $? -ne 0 ]; then
    echo -e "\n\nERROR!!\n"
else
    echo -e "\nDONE."
fi
